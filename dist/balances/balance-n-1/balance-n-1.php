<?php
/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 15/12/2021
 * Time: 10:31
 */

include('../../db.php');

if(!isset($_SESSION['id_user']))
{
    header("location:../../connexion.php");
    exit();
}

$query = "SELECT cpte FROM balance_n_1";
$statement = $db->prepare($query);
$statement->execute();
$count = $statement->rowCount();

echo $count;

if($count > 0)
{
    header("location:balance-n-1-plein.php");
} else {
    header("location:balance-n-1-vide.php");
}

?>