
<?php

include('../../db.php');
include('../../fonctions.php');
include('../../fonctions-sql.php');
include('../../lib-php/PhpExcel/Classes/PHPExcel.php');

$message = '';

// Importation des données du fichier dans la base données

/*
$uploadPathExcel = "C:/xampp/htdocs/mistral/dist/balances/balance-n/excel";
$uploadPathCsv = "C:/xampp/htdocs/mistral/dist/balances/balance-n/csv";
*/


$uploadPathExcel = __DIR__ . "/excel";
$uploadPathCsv = __DIR__ . "/csv";



$tableImport = "balance_n_1";
$tableOrigin = "compte";

    $fullPathExcel = $uploadPathExcel . '/' . $_FILES['fichier']['name'];

    $infoPath = pathinfo($_FILES['fichier']['name']);

    if (in_array($infoPath['extension'], array('xlsx'))) {

        //Permet d'uploader même quand le fichier est ouvert

        // if (is_executable($_FILES['fichier']['tmp_name'])) {
            move_uploaded_file($_FILES['fichier']['tmp_name'], $fullPathExcel);
        // } else {
        //     $message = 'erreur importation';
        // }


        // Convertir le fichier excel en csv

        $inputFileType = 'Excel2007';
        $inputFileName = $fullPathExcel;
        $sheetIndexChoice = 0;

        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcelReader = $objReader->load($inputFileName);

        $loadedSheetNames = $objPHPExcelReader->getSheetNames();

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcelReader, 'CSV');

        foreach ($loadedSheetNames as $sheetIndex => $loadedSheetName) {

            if ($sheetIndex == $sheetIndexChoice) {

                $objWriter->setSheetIndex($sheetIndex);
                $objWriter->setEnclosure('');
                $objWriter->setDelimiter(';');
                $fullPathCsv = $uploadPathCsv . '/' . $loadedSheetName . '.csv';
                $objWriter->save($fullPathCsv);
            }
        }

        // Récupération des données dans le fichier csv

        $ligneSheet = [
            'cpte' => 0,
            'inti' => '',
            'sid' => 0,
            'sic' => 0,
            'md' => 0,
            'mc' => 0,
            'sfd' => 0,
            'sfc' => 0,
        ];
        $sheetExcel = [];

        $query = "TRUNCATE TABLE $tableImport";
        $statement = $db->prepare($query);
        $statement->execute();

        $sheet = file_get_contents($fullPathCsv);
        $lignes = explode(PHP_EOL, $sheet);
        array_shift($lignes);

        //Effacer les lignes vides du fichier csv récupérer


        foreach ($lignes as $i => $ligne) {
            if ($ligne == ',,,,,,,' || $ligne == '') {
                unset($lignes[$i]);
            }
        }


        foreach ($lignes as $i => $ligne) {
            $ligne = explode(';', $ligne);
            $col = 0;
            foreach ($ligne as $cell) {
                switch ($col) {
                    case 0:
                        $ligneSheet['cpte'] = addslashes($cell);
                        break;
                    case 1:
                        $ligneSheet['inti'] = addslashes($cell);
                        break;
                    case 2:
                        $ligneSheet['sid'] = str_replace(',', '', $cell);
                        break;
                    case 3:
                        $ligneSheet['sic'] = str_replace(',', '', $cell);
                        break;
                    case 4:
                        $ligneSheet['md'] = str_replace(',', '', $cell);
                        break;
                    case 5:
                        $ligneSheet['mc'] = str_replace(',', '', $cell);
                        break;
                    case 6:
                        $ligneSheet['sfd'] = str_replace(',', '', $cell);
                        break;
                    case 7:
                        $ligneSheet['sfc'] = str_replace(',', '', $cell);
                        break;
                }
                $col++;
            }
            if (insert($tableImport, $ligneSheet, $db)) {
                //do something
            } else {
                $message = 'erreur importation';
                break;
            }
            $sheetExcel[] = $ligneSheet;
        }

        $message = 'importation reussi';

        // Comparaison de infos et infosoriginales
        /*
        $query = "SELECT * FROM $tableOrigin";
        $statement = $db->prepare($query);
        $statement->execute();

        $sheetDatabase = $statement->fetchAll();
        
        if (!empty($sheetExcel) && empty(compareTable($sheetExcel, $sheetDatabase))) {
            $message = 'importation reussi';
        } else {
            $message = 'importation echoue';
            $_SESSION['failledInfos'] = compareTable($sheetExcel, $sheetDatabase);
        }
        */


    } else {
        $message = 'extension incorrecte';
    }



echo json_encode($message);
?>