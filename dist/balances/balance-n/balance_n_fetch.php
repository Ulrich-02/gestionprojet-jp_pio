<?php
/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 15/12/2021
 * Time: 12:31
 */

include('../../db.php');

//$colonne = array("cpte", "inti", "sid", "sic", "md", "mc", "sfd", "sfc");

$query = '';

$output = array();

$query .= "SELECT * FROM balance_n ";

if(isset($_POST["search"]["value"]))
{


    $query .= 'WHERE cpte LIKE "%'.$_POST["search"]["value"].'%" ';
    $query .= 'OR inti LIKE "%'.$_POST["search"]["value"].'%" ';
    $query .= 'OR sid LIKE "%'.$_POST["search"]["value"].'%" ';
    $query .= 'OR sic LIKE "%'.$_POST["search"]["value"].'%" ';
    $query .= 'OR md LIKE "%'.$_POST["search"]["value"].'%" ';
    $query .= 'OR mc LIKE "%'.$_POST["search"]["value"].'%" ';
    $query .= 'OR sfd LIKE "%'.$_POST["search"]["value"].'%" ';
    $query .= 'OR sfc LIKE "%'.$_POST["search"]["value"].'%" ';


}

// Filtrage dans le tableau
if(isset($_POST['order']))
{
    $query .= 'ORDER BY '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].' ';
}
else
{
    $query .= 'ORDER BY cpte ASC ';
}

if($_POST['length'] != -1)
{
    $query .= 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}

$statement = $connect->prepare($query);

$statement->execute();

$result = $statement->fetchAll();

$data = array();

$filtered_rows = $statement->rowCount();

foreach($result as $row)
{

    $sub_array = array();
    $sub_array[] = $row['cpte'];
    $sub_array[] = $row['inti'];
    $sub_array[] = $row['sid'];
    $sub_array[] = $row['sic'];
    $sub_array[] = $row['md'];
    $sub_array[] = $row['mc'];
    $sub_array[] = $row['sfd'];
    $sub_array[] = $row['sfc'];
    $data[] = $sub_array;

    /*
    $sub_array = array();
    $sub_array[] = 1;
    $sub_array[] = 2;
    $sub_array[] = 3;
    $sub_array[] = 4;
    $sub_array[] = 5;
    $sub_array[] = 6;
    $sub_array[] = 7;
    $sub_array[] = 8;
    $data[] = $sub_array;
    */
}

$output = array(
    "draw"			=>	intval($_POST["draw"]),
    "recordsTotal"  	=>  $filtered_rows,
    "recordsFiltered" 	=> 	get_total_all_records($connect),
    "data"				=>	$data
);

function get_total_all_records($connect)
{
    $statement = $connect->prepare("SELECT * FROM balance_n ");
    $statement->execute();
    return $statement->rowCount();
}

echo json_encode($output);

?>