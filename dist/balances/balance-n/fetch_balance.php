<?php
/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 15/12/2021
 * Time: 14:14
 */

include('../../db.php');

// On met tiret quand une valeur est 0;
function jp($a) {
    $b = ($a == 0) ? "-" : number_format($a, 0, ',', ' ');
    return $b;
}

function balance_data($db)
{
    $query = "
    SELECT * FROM balance_n 
    ";
    $statement = $db->prepare($query);
    $statement->execute();

    $result = $statement->fetchAll();
    $output = '';

    foreach($result as $row)
    {
        $output .= '
            <tr>
                <td>'.$row["cpte"].'</td>
                <td>'.$row["inti"].'</td>
                <td>'.jp($row["sid"]).'</td>
                <td>'.jp($row["sic"]).'</td>
                <td>'.jp($row["md"]).'</td>
                <td>'.jp($row["mc"]).'</td>
                <td>'.jp($row["sfd"]).'</td>
                <td>'.jp($row["sfc"]).'</td>
            </tr>
        ';
    }
    return $output;
}

function lien_data($db)
{
    $query = "
    SELECT * FROM lien 
    ";
    $statement = $db->prepare($query);
    $statement->execute();

    $result = $statement->fetchAll();
    $output = '';

    foreach($result as $row)
    {
        $output .= '
            <tr>
                <td>'.$row["url_lien"].'</td>
                <td>'.$row["etat_lien"].'</td>
                <td>'.jp($row["tarif_lien"]).'</td>
                <td>'.jp($row["date_lien"]).'</td>
                <td>'.jp($row["description_lien"]).'</td>
                <td>'.jp($row["intitule_lien"]).'</td>
            </tr>
        ';
    }
    return $output;
}

?>