<?php
/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 21/12/2021
 * Time: 21:33
 */



//include 'vendor/autoload.php';

include('../../db.php');
include('../../vendor/autoload.php');

//$db = new PDO("mysql:host=localhost;dbname=testing", "root", "");


    $allowed_extension = array('xls', 'csv', 'xlsx');
    $file_array = explode(".", $_FILES["fichier"]["name"]);
    $file_extension = end($file_array);

    if(in_array($file_extension, $allowed_extension))
    {
        $file_name = time() . '.' . $file_extension;
        move_uploaded_file($_FILES['fichier']['tmp_name'], $file_name);
        $file_type = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file_name);
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($file_type);

        $spreadsheet = $reader->load($file_name);

        unlink($file_name);

        $data = $spreadsheet->getActiveSheet()->toArray();

        foreach($data as $row)
        {
            $insert_data = array(
                ':cpte'  => $row[0],
                ':inti'  => $row[1],
                ':sid'  => $row[2],
                ':sic'  => $row[3],
                ':md'  => $row[4],
                ':mc'  => $row[5],
                ':sfd'  => $row[6],
                ':sfc'  => $row[7]
            );

            $query = "INSERT INTO balance_n (cpte, inti, sid, sic, md, mc, sfd, sfc) VALUES (:cpte, :inti, :sid, :sic, :md, :mc, :sfd, :sfc) ";

            $statement = $db->prepare($query);
            $statement->execute($insert_data);
        }
        $message = 'success';

    }
    else
    {
        $message = 'bad extension';
    }


echo json_encode($message);

?>