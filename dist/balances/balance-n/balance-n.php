<?php
/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 15/12/2021
 * Time: 10:31
 */

include('../../db.php');

if(!isset($_SESSION['id_user']))
{
    header("location:../../connexion.php");
    exit();
}

$query = "SELECT cpte FROM balance_n ";
$statement = $db->prepare($query);
$statement->execute();
$count = $statement->rowCount();

if($count > 0)
{
    header("location:balance-n-plein.php");
} else {
    header("location:balance-n-vide.php");
}

?>