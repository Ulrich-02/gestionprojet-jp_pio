<?php require_once(__DIR__ . '/../../db.php') ?>
<?php require_once(__DIR__ . '/../../fonctions.php') ?>
<?php require_once(__DIR__ . '/../../fonctions-sql.php') ?>
<?php require_once(__DIR__ . '/../../lib-php/PhpExcel/Classes/PHPExcel.php') ?>
<?php
$message = '';

// Importation des données du fichier dans la base données

$uploadPathExcel = __DIR__ . "/excel";
$uploadPathCsv = __DIR__ . "/csv";
$tableImport = "balance_n";
$tableOrigin = "compte";
if ($_FILES) {
    $fullPathExcel = $uploadPathExcel . '/' . $_FILES['fichier']['name'];

    $infoPath = pathinfo($_FILES['fichier']['name']);

    if (in_array($infoPath['extension'], array('xlsx'))) {

        //Permet d'uploader même quand le fichier est ouvert

        // if (is_executable($_FILES['fichier']['tmp_name'])) {
            move_uploaded_file($_FILES['fichier']['tmp_name'], $fullPathExcel);
        // } else {
        //     $message = 'erreur importation';
        // }


        // Convertir le fichier excel en csv

        $inputFileType = 'Excel2007';
        $inputFileName = $fullPathExcel;
        $sheetIndexChoice = 0;

        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcelReader = $objReader->load($inputFileName);

        $loadedSheetNames = $objPHPExcelReader->getSheetNames();

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcelReader, 'CSV');

        foreach ($loadedSheetNames as $sheetIndex => $loadedSheetName) {

            if ($sheetIndex == $sheetIndexChoice) {

                $objWriter->setSheetIndex($sheetIndex);
                $objWriter->setEnclosure('');
                $fullPathCsv = $uploadPathCsv . '/' . $loadedSheetName . '.csv';
                $objWriter->save($fullPathCsv);
            }
        }

        // Récupération des données dans le fichier csv

        $ligneSheet = [
            'cpte' => 0,
            'inti' => '',
            'sid' => 0,
            'sic' => 0,
            'md' => 0,
            'mc' => 0,
            'sfd' => 0,
            'sfc' => 0,
        ];
        $sheetExcel = [];

        $query = "TRUNCATE TABLE $tableImport";
        $statement = $connect->prepare($query);
        $statement->execute();

        $sheet = file_get_contents($fullPathCsv);
        $lignes = explode(PHP_EOL, $sheet);
        array_shift($lignes);

        //Effacer les lignes vides du fichier csv récupérer

        foreach ($lignes as $i => $ligne) {
            if ($ligne == ',,,,,,,' || $ligne == '') {
                unset($lignes[$i]);
            }
        }

        foreach ($lignes as $i => $ligne) {
            $ligne = explode(',', $ligne);
            $col = 0;
            foreach ($ligne as $cell) {
                switch ($col) {
                    case 0:
                        $ligneSheet['cpte'] = $cell;
                        break;
                    case 1:
                        $ligneSheet['inti'] = $cell;
                        break;
                    case 2:
                        $ligneSheet['sid'] = $cell;
                        break;
                    case 3:
                        $ligneSheet['sic'] = $cell;
                        break;
                    case 4:
                        $ligneSheet['md'] = $cell;
                        break;
                    case 5:
                        $ligneSheet['mc'] = $cell;
                        break;
                    case 6:
                        $ligneSheet['sfd'] = $cell;
                        break;
                    case 7:
                        $ligneSheet['sfc'] = $cell;
                        break;
                }
                $col++;
            }
            if (insert($tableImport, $ligneSheet, $connect)) {
                //do something
            } else {
                $message = 'erreur importation';
                break;
            }
            $sheetExcel[] = $ligneSheet;
        }

        // Comparaison de infos et infosoriginales

        $query = "SELECT * FROM $tableOrigin";
        $statement = $connect->prepare($query);
        $statement->execute();

        $sheetDatabase = $statement->fetchAll();
        
        if (!empty($sheetExcel) && empty(compareTable($sheetExcel, $sheetDatabase))) {
            $message = 'importation reussi';
        } else {
            $message = 'importation echoue';
            $_SESSION['failledInfos'] = compareTable($sheetExcel, $sheetDatabase);
        }
    } else {
        $message = 'extension incorrecte';
    }
}



// Affichage des données récupérées de la base de données

if (isset($_POST['table'])) {

    $table = $_POST['table'];
    $query = "SELECT * FROM $table";
    $statement = $connect->prepare($query);
    $statement->execute();

    $result = $statement->fetchAll();
    $message = $result;
}

echo json_encode($message);
?>