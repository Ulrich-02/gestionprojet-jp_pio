<?php
  $title = 'Références';
include('db.php');
include('fonctions.php');
include('fonctions-sql.php');
?>

<html>
    <head>
        <title>REF</title>
    </head>

<body>

<section class="container">

    <h1>Listes des références du bilan</h1>

    <div class="alert alert-primary text-center w-75 h2 p-2" role="alert">Actif</div>

    <ul class="list">
        <li class="list-group mt-2"><a href="ref-print.php?ref=ad">Référence <b><?= strtoupper("ad") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=ae">Référence <b><?= strtoupper("ae") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=af">Référence <b><?= strtoupper("af") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=ag">Référence <b><?= strtoupper("ag") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=ah">Référence <b><?= strtoupper("ah") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=ai">Référence <b><?= strtoupper("ai") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=aj">Référence <b><?= strtoupper("aj") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=ak">Référence <b><?= strtoupper("ak") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=al">Référence <b><?= strtoupper("al") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=am">Référence <b><?= strtoupper("am") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=an">Référence <b><?= strtoupper("an") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=ap">Référence <b><?= strtoupper("ap") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=aq">Référence <b><?= strtoupper("aq") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=ar">Référence <b><?= strtoupper("ar") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=as">Référence <b><?= strtoupper("as") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=az">Référence <b><?= strtoupper("az") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=ba">Référence <b><?= strtoupper("ba") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=bb">Référence <b><?= strtoupper("bb") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=bg">Référence <b><?= strtoupper("bg") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=bh">Référence <b><?= strtoupper("bh") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=bi">Référence <b><?= strtoupper("bi") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=bj">Référence <b><?= strtoupper("bj") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=bk">Référence <b><?= strtoupper("bk") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=bq">Référence <b><?= strtoupper("bq") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=br">Référence <b><?= strtoupper("br") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=bs">Référence <b><?= strtoupper("bs") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=bt">Référence <b><?= strtoupper("bt") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=bu">Référence <b><?= strtoupper("bu") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=bz">Référence <b><?= strtoupper("bz") ?></b></a></li>
    </ul>

    <div class="alert alert-primary text-center w-75 h2 p-2" role="alert">Passif</div>
    <ul class="list">
        <li class="list-group mt-2"><a href="ref-print.php?ref=ca">Référence <b><?= strtoupper("ca") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=cb">Référence <b><?= strtoupper("cb") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=cd">Référence <b><?= strtoupper("cd") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=ce">Référence <b><?= strtoupper("ce") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=cf">Référence <b><?= strtoupper("cf") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=cg">Référence <b><?= strtoupper("cg") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=ch">Référence <b><?= strtoupper("ch") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=cj">Référence <b><?= strtoupper("cj") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=cl">Référence <b><?= strtoupper("cl") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=cm">Référence <b><?= strtoupper("cm") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=cp">Référence <b><?= strtoupper("cp") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=da">Référence <b><?= strtoupper("da") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=db">Référence <b><?= strtoupper("db") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=dc">Référence <b><?= strtoupper("dc") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=dd">Référence <b><?= strtoupper("dd") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=df">Référence <b><?= strtoupper("df") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=dh">Référence <b><?= strtoupper("dh") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=di">Référence <b><?= strtoupper("di") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=dj">Référence <b><?= strtoupper("dj") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=dk">Référence <b><?= strtoupper("dk") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=dm">Référence <b><?= strtoupper("dm") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=dn">Référence <b><?= strtoupper("dn") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=dp">Référence <b><?= strtoupper("dp") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=dq">Référence <b><?= strtoupper("dq") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=dr">Référence <b><?= strtoupper("dr") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=dt">Référence <b><?= strtoupper("dt") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=dv">Référence <b><?= strtoupper("dv") ?></b></a></li>
        <li class="list-group mt-2"><a href="ref-print.php?ref=dz">Référence <b><?= strtoupper("dz") ?></b></a></li>
    </ul>

</section>

</body>

</html>