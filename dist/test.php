<?php
//Insertion dans la base
update('bilan', [
    'id_bilan' => 1,
    'ad_brut' => $ad_brut,
    'ad_dep' => $ad_dep,
    'ad_net' => $ad_net,
    'ad_net_1' => $ad_net_n_1,

    'ae_brut' => $ae_brut,
    'ae_dep' => $ae_dep,
    'ae_net' => $ae_net,
    'ae_net_1' => $ae_net_n_1,

    'af_brut' => $af_brut,
    'af_dep' => $af_dep,
    'af_net' => $af_net,
    'af_net_1' => $af_net_n_1,

    'ag_brut' => $ag_brut,
    'ag_dep' => $ag_dep,
    'ag_net' => $ag_net,
    'ag_net_1' => $ag_net_n_1,

    'ah_brut' => $ah_brut,
    'ah_dep' => $ah_dep,
    'ah_net' => $ah_net,
    'ah_net_1' => $ah_net_n_1,

    'ai_brut' => $ai_brut,
    'ai_dep' => $ai_dep,
    'ai_net' => $ai_net,
    'ai_net_1' => $ai_net_n_1,

    'aj_brut' => $aj_brut,
    'aj_dep' => $aj_dep,
    'aj_net' => $aj_net,
    'aj_net_1' => $aj_net_n_1,

    'ak_brut' => $ak_brut,
    'ak_dep' => $ak_dep,
    'ak_net' => $ak_net,
    'ak_net_1' => $ak_net_n_1,

    'al_brut' => $al_brut,
    'al_dep' => $al_dep,
    'al_net' => $al_net,
    'al_net_1' => $al_net_n_1,

    'am_brut' => $am_brut,
    'am_dep' => $am_dep,
    'am_net' => $am_net,
    'am_net_1' => $am_net_n_1,

    'an_brut' => $an_brut,
    'an_dep' => $an_dep,
    'an_net' => $an_net,
    'an_net_1' => $an_net_n_1,

    'ap_brut' => $ap_brut,
    'ap_dep' => $ap_dep,
    'ap_net' => $ap_net,
    'ap_net_1' => $ap_net_n_1,

    'aq_brut' => $aq_brut,
    'aq_dep' => $aq_dep,
    'aq_net' => $aq_net,
    'aq_net_1' => $aq_net_n_1,

    'ar_brut' => $ar_brut,
    'ar_dep' => $ar_dep,
    'ar_net' => $ar_net,
    'ar_net_1' => $ar_net_n_1,

    'as_brut' => $as_brut,
    'as_dep' => $as_dep,
    'as_net' => $as_net,
    'as_net_1' => $as_net_n_1,

    'az_brut' => $az_brut,
    'az_dep' => $az_dep,
    'az_net' => $az_net,
    'az_net_1' => $az_net_n_1,

    'ba_brut' => $ba_brut,
    'ba_dep' => $ba_dep,
    'ba_net' => $ba_net,
    'ba_net_1' => $ba_net_n_1,

    'bb_brut' => $bb_brut,
    'bb_dep' => $bb_dep,
    'bb_net' => $bb_net,
    'bb_net_1' => $bb_net_n_1,

    'bg_brut' => $bg_brut,
    'bg_dep' => $bg_dep,
    'bg_net' => $bg_net,
    'bg_net_1' => $bg_net_n_1,

    'bh_brut' => $bh_brut,
    'bh_dep' => $bh_dep,
    'bh_net' => $bh_net,
    'bh_net_1' => $bh_net_n_1,

    'bi_brut' => $bi_brut,
    'bi_dep' => $bi_dep,
    'bi_net' => $bi_net,
    'bi_net_1' => $bi_net_n_1,

    'bj_brut' => $bj_brut,
    'bj_dep' => $bj_dep,
    'bj_net' => $bj_net,
    'bj_net_1' => $bj_net_n_1,

    'bk_brut' => $bk_brut,
    'bk_dep' => $bk_dep,
    'bk_net' => $bk_net,
    'bk_net_1' => $bk_net_n_1,

    'bq_brut' => $bq_brut,
    'bq_dep' => $bq_dep,
    'bq_net' => $bq_net,
    'bq_net_1' => $bq_net_n_1,

    'br_brut' => $br_brut,
    'br_dep' => $br_dep,
    'br_net' => $br_net,
    'br_net_1' => $br_net_n_1,

    'bs_brut' => $bs_brut,
    'bs_dep' => $bs_dep,
    'bs_net' => $bs_net,
    'bs_net_1' => $bs_net_n_1,

    'bt_brut' => $bt_brut,
    'bt_dep' => $bt_dep,
    'bt_net' => $bt_net,
    'bt_net_1' => $bt_net_n_1,

    'bu_brut' => $bu_brut,
    'bu_dep' => $bu_dep,
    'bu_net' => $bu_net,
    'bu_net_1' => $bu_net_n_1,

    'bz_brut' => $bz_brut,
    'bz_dep' => $bz_dep,
    'bz_net' => $bz_net,
    'bz_net_1' => $bz_net_n_1,

    'ca' => $ca,
    'ca_1' => $ca_n_1,

    'cb' => $cb,
    'cb_1' => $cb_n_1,

    'cd' => $cd,
    'cd_1' => $cd_n_1,

    'ce' => $ce,
    'ce_1' => $ce_n_1,

    'cf' => $cf,
    'cf_1' => $cf_n_1,

    'cg' => $cg,
    'cg_1' => $cg_n_1,

    'ch' => $ch,
    'ch_1' => $ch_n_1,

    'cj' => $cj,
    'cj_1' => $cj_n_1,

    'cl' => $cl,
    'cl_1' => $cl_n_1,

    'cm' => $cm,
    'cm_1' => $cm_n_1,

    'cp' => $cp,
    'cp_1' => $cp_n_1,

    'da' => $da,
    'da_1' => $da_n_1,

    'db' => $db,
    'db_1' => $db_n_1,

    'dc' => $dc,
    'dc_1' => $dc_n_1,

    'dd' => $dd,
    'dd_1' => $dd_n_1,

    'df' => $df,
    'df_1' => $df_n_1,

    'dh' => $dh,
    'dh_1' => $dh_n_1,

    'di' => $di,
    'di_1' => $di_n_1,

    'dj' => $dj,
    'dj_1' => $dj_n_1,

    'dk' => $dk,
    'dk_1' => $dk_n_1,

    'dm' => $dm,
    'dm_1' => $dm_n_1,

    'dn' => $dn,
    'dn_1' => $dn_n_1,

    'dp' => $dp,
    'dp_1' => $dp_n_1,

    'dq' => $dq,
    'dq_1' => $dq_n_1,

    'dr' => $dr,
    'dr_1' => $dr_n_1,

    'dt' => $dt,
    'dt_1' => $dt_n_1,

    'dv' => $dv,
    'dv_1' => $dv_n_1,

    'dz' => $dz,
    'dz_1' => $dz_n_1

],'id_bilan = 1', $db);

echo 'Terminé';
?>