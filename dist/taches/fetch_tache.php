<?php
/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 15/12/2021
 * Time: 14:14
 */

include('../db.php');

// On met tiret quand une valeur est 0;
function jp($a) {
    $b = ($a == 0) ? "-" : number_format($a, 0, ',', ' ');
    return $b;
}

function balance_data($db)
{
    $query = "
    SELECT * FROM balance_n 
    ";
    $statement = $db->prepare($query);
    $statement->execute();

    $result = $statement->fetchAll();
    $output = '';

    foreach($result as $row)
    {
        $output .= '
            <tr>
                <td>'.$row["cpte"].'</td>
                <td>'.$row["inti"].'</td>
                <td>'.jp($row["sid"]).'</td>
                <td>'.jp($row["sic"]).'</td>
                <td>'.jp($row["md"]).'</td>
                <td>'.jp($row["mc"]).'</td>
                <td>'.jp($row["sfd"]).'</td>
                <td>'.jp($row["sfc"]).'</td>
            </tr>
        ';
    }
    return $output;
}

function tache_data($db)
{
    $query = "
    SELECT * FROM taches T,projet J,participants P
    WHERE T.id_projet_tache_ft_projet = J.id_projet AND T.id_participant_tache_ft_pp = P.id_participants
    ";
    // INNER JOIN projet tc on taches.id_projet_tache_ft_projet = tc.id_projet and 
    // INNER JOIN participants tk on taches.id_projet_tache_ft_projet = tk.id_participants 
    $statement = $db->prepare($query);
    $statement->execute();

    $result = $statement->fetchAll();
    $output = '';

    foreach($result as $row)
    {
        $status = '';
        if($row['statut_tache'] == 'Actif')
        {
            $status = '<span class="badge badge-primary">'. "Actif" .'</span>';
        }
        else
        {
            $status = '<span class="badge badge-danger">'. "Inactif" .'</span>';
        }

        $output .= '
            <tr>
                <td>'.$row["nom_tache"].'</td>
                <td>'.$row["date_debut_tache"].'</td>
                <td>'.$row["date_fin_tache"].'</td>
                <td>'.$row["nom_projet"].'</td>
                <td>'.$row["nom_pp"].'</td>
                <td>'.$status.'</td>
                <td class="text-end">
                    <a class="btn btn-sm btn-light btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
                    <span class="svg-icon svg-icon-5 m-0">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="black" />
                        </svg>
                    </span>
                    <!--end::Svg Icon--></a>
                    <!--begin::Menu-->
                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">
                        <!--begin::Menu item-->
                        <div class="menu-item px-3">
                            <a  class="menu-link px-3">Modifier</a>
                        </div>
                        <!--end::Menu item-->
                        <!--begin::Menu item-->
                        <div class="menu-item px-3">
                            <a class="menu-link px-3" data-kt-customer-table-filter="delete_row">Désactiver</a>
                        </div>
                        <!--end::Menu item-->
                    </div>
                    <!--end::Menu-->
                </td>
            </tr>
        ';
    }
    return $output;
}

?>