<?php
/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 23/12/2021
 * Time: 08:15
 */

include('vendor/autoload.php');
include('db.php');

$query = "SELECT * FROM bilan ";
$statement = $db->prepare($query);
$statement->execute();
$result = $statement->fetchAll();



$query1 = "SELECT * FROM infos ";
$statement1 = $db->prepare($query1);
$statement1->execute();
$result1 = $statement1->fetchAll();

$nom = '';
$numero = '';
$an_n = '';
$an_n_1 = '';
$duree = '';
foreach($result1 as $row1) {
    $nom = $row1["nom_infos"];
    $numero = $row1["numero_infos"];
    $an_n = $row1["an_n_infos"];
    $an_n_1 = $row1["an_n_1_infos"];
    $duree = $row1["duree_infos"];
}


$date = gmdate("d-m-Y");
$hour = gmdate("H:i");
$hour2 = gmdate("H-i");

// On met tiret quand une valeur est 0;
function jp($a) {
    $b = ($a == 0) ? "-" : number_format($a, 0, ',', ' ');
    return $b;
}

// reference the Dompdf namespace
use Dompdf\Dompdf;

$output = '
        <html>
        <head>
            <style>
                .gras {font-weight: bold;}
                .pl-5 {padding-left: 5px;}
                .pr-5 {padding-right: 5px;}
                .bg-col-blue {background-color: #95b3d7;}
                .bg-col-gris {background-color: #D3D3D3;}
                .txt-center {text-align: center;}
                .txt-right {text-align: right;}
                .bg-color-blue-fonce {background-color: #95b3d7;}
                .bg-color-green-fonce {background-color: #33ff00;}
                .col-white {color: white;}
            </style>
        </head>
        <body>
		<div class="table-responsive" style="font-size: 8.5px !important;">
		    <h1 style="font-size: 13px !important;">
		        <span>Désignation entité: '.$nom.'</span>
		        <span style="float: right;">Exercice clos le 31-12-'.$an_n.'</span>
		    </h1>
		    <h1 style="font-size: 13px !important;">
		        <span>Numéro d\'identification: '.$numero.'</span>
		        <span style="float: right;">Durée (en mois): '.$duree.'</span>
		    </h1>
		    <h1 style="font-size: 17px !important; text-align: center; color: #0070c0;">BILAN AU 31 DECEMBRE '.$an_n.'</h1>
		    <!--<h1>Bilan du '.$date.' à '.$hour.' (Heure GMT)</h1>-->
            <table border="1" style="border-collapse:collapse;" >
                <tr class="bg-col-blue">
                    <th rowspan="2" class="pl-5 pr-5">REF</th>
                    <th rowspan="2" colspan="8">ACTIF</th>
                    
                    
                    <th rowspan="2" class="pl-5 pr-5">Note</th>
                    <th colspan="9">EXERCICE <br>AU 31/12/'.$an_n.'</th>
                    
                    <th colspan="3" class="pl-5 pr-5">EXERCICE <br>AU 31/12/'.$an_n_1.'</th>
                    
                    <th rowspan="2" class="pl-5 pr-5">REF</th>
                    <th rowspan="2" colspan="6">PASSIF</th>
                    
                    <th rowspan="2" class="pl-5 pr-5">Note</th>
                    <th colspan="3" class="pl-5 pr-5">EXERCICE <br>AU 31/12/'.$an_n.'</th>
                    
                    <th colspan="3" class="pl-5 pr-5">EXERCICE <br>AU 31/12/'.$an_n_1.'</th>
                    
                    
                    
                    
                </tr>
';



$output .= '
    			<tr class="bg-col-blue">
    			    <th colspan="3">BRUT</th>
    			    
    				<th colspan="3">AMORT/DEPREC</th>
    			    
    				<th colspan="3">NET</th>
    				
    			    <th colspan="3">NET</th>
    			    
    				<th colspan="3">NET</th>
    				
    			    <th colspan="3">NET</th>
    			</tr>
';



foreach($result as $row) {
    $output .= '
    			<tr>
    			    <td class="gras txt-center">AD</td>
    			    <td colspan="8" class="pl-5 pr-5 gras bg-col-gris" style="width: 220px;">IMMOBILISATIONS INCORPORELLES</td>
    			    
    				
    				<td class="txt-center gras bg-col-gris">3</td>
    			    <td colspan="3" class="txt-right gras pr-5 bg-col-gris">'.jp($row["ad_brut"]).'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5 bg-col-gris">'.jp($row["ad_dep"]).'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5 bg-col-gris">'.number_format($row["ad_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5 bg-col-gris">'.number_format($row["ad_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">CA</td>
    			    <td colspan="6" class="pl-5 pr-5 gras" style="width: 280px;">Capital</td>
    			    
    			    <td class="txt-center gras">13</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["ca"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["ca_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="txt-center gras">AE</td>
    			    <td colspan="8" class="pl-5 pr-5 gras">Frais de développement et de prospection</td>
    			    
    				
    				<td class="txt-center gras"></td>
    			    <td colspan="3" class="txt-right gras pr-5">'.jp($row["ae_brut"]).'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["ae_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["ae_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["ae_net_1"], 0, ',', ' ').'</td></td>
    			    
    				<td class="txt-center gras">CB</td>
    			    <td colspan="6" class="pl-5 pr-5 gras">Apporteurs capital non appelé (-)</td>
    			    
    			    <td class="txt-center gras">13</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["cb"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["cb_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="txt-center gras">AF</td>
    			    <td colspan="8" class="pl-5 pr-5 gras">Brevets, licences, logiciels et droits similaires</td>
    			    
    				
    				<td class="txt-center gras"></td>
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["af_brut"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["af_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["af_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["af_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">CD</td>
    			    <td colspan="6" class="pl-5 pr-5 gras">Primes liées au capital social</td>
    			    
    			    <td class="txt-center gras">14</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["cd"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["cd_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="txt-center gras">AG</td>
    			    <td colspan="8" class="pl-5 pr-5 gras">Fonds commercial et droit au bail</td>
    			    
    				
    				<td class="txt-center gras"></td>
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["ag_brut"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["ag_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["ag_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["ag_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">CE</td>
    			    <td colspan="6" class="pl-5 pr-5 gras">Ecarts de réévaluation</td>
    			    
    			    <td class="txt-center gras">3e</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["ce"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["ce_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="txt-center gras">AH</td>
    			    <td colspan="8" class="pl-5 pr-5 gras">Autres immobilisations incorporelles</td>
    			    
    				
    				<td class="txt-center gras"></td>
    			    <td colspan="3" class="txt-right gras pr-5">'.jp($row["ah_brut"]).'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["ah_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["ah_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["ah_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">CF</td>
    			    <td colspan="6" class="pl-5 pr-5 gras">Réserves indisponibles</td>
    			    
    			    <td class="txt-center gras">14</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["cf"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["cf_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="gras txt-center">AI</td>
    			    <td colspan="8" class="pl-5 pr-5 gras bg-col-gris">IMMOBILISATIONS CORPORELLES</td>
    			    
    				
    				<td class="txt-center gras bg-col-gris">3</td>
    			    <td colspan="3" class="txt-right gras pr-5 bg-col-gris">'.number_format($row["ai_brut"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5 bg-col-gris">'.number_format($row["ai_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5 bg-col-gris">'.number_format($row["ai_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5 bg-col-gris">'.number_format($row["ai_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">CG</td>
    			    <td colspan="6" class="pl-5 pr-5 gras">Réserves libres</td>
    			    
    			    <td class="txt-center gras">14</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["cg"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["cg_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="txt-center gras">AJ</td>
    			    <td colspan="8" class="pl-5 pr-5 gras">Terrains (1)<br>
    			    (1) dont Placement en Net ..................../.....................
    			    </td>
    			    
    				
    				<td class="txt-center gras"></td>
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["aj_brut"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["aj_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["aj_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["aj_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">CH</td>
    			    <td colspan="6" class="pl-5 pr-5 gras">Report à nouveau (+ ou -)</td>
    			    
    			    <td class="txt-center gras">14</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["ch"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["ch_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="txt-center gras">AK</td>
    			    <td colspan="8" class="pl-5 pr-5 gras">Bâtiments<br>
    			    (1) dont Placement en Net ..................../.....................
    			    </td>
    			    
    				
    				<td class="txt-center gras"></td>
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["ak_brut"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["ak_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["ak_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["ak_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">CJ</td>
    			    <td colspan="6" class="pl-5 pr-5 gras">Résultat net de l\'exercice (bénéfice + ou perte -)</td>
    			    
    			    <td class="txt-center gras"></td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["cj"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["cj_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="txt-center gras">AL</td>
    			    <td colspan="8" class="pl-5 pr-5 gras">Aménagement, agencements et Installations</td>
    			    
    				
    				<td class="txt-center gras"></td>
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["al_brut"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["al_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["al_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["al_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">CL</td>
    			    <td colspan="6" class="pl-5 pr-5 gras">Subventions d\'investissement</td>
    			    
    			    <td class="txt-center gras">15</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["cl"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["cl_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="txt-center gras">AM</td>
    			    <td colspan="8" class="pl-5 pr-5 gras">Matériel, mobilier et actifs biologiques</td>
    			    
    				
    				<td class="txt-center gras"></td>
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["am_brut"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["am_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["am_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["am_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">CM</td>
    			    <td colspan="6" class="pl-5 pr-5 gras">Provisions réglementées</td>
    			    
    			    <td class="txt-center gras">15</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["cm"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["cm_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="txt-center gras">AN</td>
    			    <td colspan="8" class="pl-5 pr-5 gras">Matériel de transport</td>
    			    
    				
    				<td class="txt-center gras"></td>
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["an_brut"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["an_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["an_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["an_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">CP</td>
    			    <td colspan="6" class="pl-5 pr-5 gras bg-col-gris">TOTAL CAPITAUX PROPRES ET RESSOURCES ASSIMILEES</td>
    			    
    			    <td class="txt-center gras bg-col-gris"></td>
    			    
    				<td colspan="3" class="txt-right gras pr-5 bg-col-gris">'.number_format($row["cp"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5 bg-col-gris">'.number_format($row["cp_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="txt-center gras">AP</td>
    			    <td colspan="8" class="pl-5 pr-5 gras">Avances et acomptes versés sur immobilisations</td>
    			    
    				
    				<td class="txt-center gras">3</td>
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["ap_brut"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["ap_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["ap_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["ap_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">DA</td>
    			    <td colspan="6" class="pl-5 pr-5 gras">Emprunts et dettes financières diverses</td>
    			    
    			    <td class="txt-center gras">16</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["da"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["da_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="gras txt-center">AQ</td>
    			    <td colspan="8" class="pl-5 pr-5 gras bg-col-gris">IMMOBILISATIONS FINANCIERES</td>
    			    
    				
    				<td class="txt-center gras bg-col-gris">4</td>
    			    <td colspan="3" class="txt-right gras pr-5 bg-col-gris">'.number_format($row["aq_brut"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5 bg-col-gris">'.number_format($row["aq_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5 bg-col-gris">'.number_format($row["aq_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5 bg-col-gris">'.number_format($row["aq_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">DB</td>
    			    <td colspan="6" class="pl-5 pr-5 gras">Dettes de location acquisition</td>
    			    
    			    <td class="txt-center gras">16</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["db"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["db_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="txt-center gras">AR</td>
    			    <td colspan="8" class="pl-5 pr-5 gras">Titres de participation</td>
    			    
    				
    				<td class="txt-center gras"></td>
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["ar_brut"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["ar_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["ar_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["ar_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">DC</td>
    			    <td colspan="6" class="pl-5 pr-5 gras">Provisions pour risques et charges</td>
    			    
    			    <td class="txt-center gras">16</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["dc"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["dc_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="txt-center gras">AS</td>
    			    <td colspan="8" class="pl-5 pr-5 gras">Autres immobilisations financières</td>
    			    
    				
    				<td class="txt-center gras"></td>
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["as_brut"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["as_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["as_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["as_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">DD</td>
    			    <td colspan="6" class="pl-5 pr-5 gras bg-col-gris">TOTAL DETTES FINANCIERES ET RESSOURCES ASSIMILEES</td>
    			    
    			    <td class="txt-center gras bg-col-gris"></td>
    			    
    				<td colspan="3" class="txt-right gras pr-5 bg-col-gris">'.number_format($row["dd"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5 bg-col-gris">'.number_format($row["dd_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="gras txt-center">AZ</td>
    			    <td colspan="8" class="pl-5 pr-5 gras bg-color-blue-fonce">TOTAL ACTIF IMMOBILISE</td>
    			    
    				
    				<td class="txt-center gras bg-color-blue-fonce"></td>
    			    <td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">'.number_format($row["az_brut"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">'.number_format($row["az_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">'.number_format($row["az_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">'.number_format($row["az_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">DF</td>
    			    <td colspan="6" class="pl-5 pr-5 gras bg-color-blue-fonce">TOTAL RESSOURCES STABLES</td>
    			    
    			    <td class="txt-center gras bg-color-blue-fonce"></td>
    			    
    				<td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">'.number_format($row["df"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">'.number_format($row["df_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="txt-center gras">BA</td>
    			    <td colspan="8" class="pl-5 pr-5 gras">ACTIF CIRCULANT H.A.O</td>
    			    
    				
    				<td class="txt-center gras">5</td>
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["ba_brut"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["ba_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["ba_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["ba_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">DH</td>
    			    <td colspan="6" class="pl-5 pr-5 gras">DETTES CIRCULANTES H.A.O</td>
    			    
    			    <td class="txt-center gras">5</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["dh"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["dh_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="txt-center gras">BB</td>
    			    <td colspan="8" class="pl-5 pr-5 gras">STOCKS ET ENCOURS</td>
    			    
    				
    				<td class="txt-center gras">6</td>
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["bb_brut"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["bb_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["bb_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["bb_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">DI</td>
    			    <td colspan="6" class="pl-5 pr-5 gras">Clients, avances reçues</td>
    			    
    			    <td class="txt-center gras">7</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["di"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["di_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="txt-center gras">BG</td>
    			    <td colspan="8" class="pl-5 pr-5 gras">CREANCES ET EMPLOIS ASSIMILES</td>
    			    
    				
    				<td class="txt-center gras"></td>
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["bg_brut"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["bg_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["bg_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["bg_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">DJ</td>
    			    <td colspan="6" class="pl-5 pr-5 gras">Fournisseurs d\'exploitation</td>
    			    
    			    <td class="txt-center gras">17</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["dj"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["dj_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="txt-center gras">BH</td>
    			    <td colspan="8" class="pl-5 pr-5 gras">Fournisseurs, avances versées</td>
    			    
    				
    				<td class="txt-center gras">17</td>
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["bh_brut"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["bh_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["bh_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["bh_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">DK</td>
    			    <td colspan="6" class="pl-5 pr-5 gras">Dettes fiscales et sociales</td>
    			    
    			    <td class="txt-center gras">18</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["dk"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["dk_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="txt-center gras">BI</td>
    			    <td colspan="8" class="pl-5 pr-5 gras">Clients</td>
    			    
    				
    				<td class="txt-center gras">7</td>
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["bi_brut"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["bi_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["bi_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["bi_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">DM</td>
    			    <td colspan="6" class="pl-5 pr-5 gras">Autres Dettes</td>
    			    
    			    <td class="txt-center gras">19</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["dm"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["dm_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="txt-center gras">BJ</td>
    			    <td colspan="8" class="pl-5 pr-5 gras">Autres créances</td>
    			    
    				
    				<td class="txt-center gras">8</td>
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["bj_brut"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["bj_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["bj_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["bj_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">DN</td>
    			    <td colspan="6" class="pl-5 pr-5 gras">Provisions pour risques à court terme Dettes</td>
    			    
    			    <td class="txt-center gras">19</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["dn"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["dn_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="gras txt-center">BK</td>
    			    <td colspan="8" class="pl-5 pr-5 gras bg-color-blue-fonce">TOTAL ACTIF CIRCULANT</td>
    			    
    				
    				<td class="txt-center gras bg-color-blue-fonce"></td>
    			    <td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">'.number_format($row["bk_brut"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">'.number_format($row["bk_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">'.number_format($row["bk_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">'.number_format($row["bk_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">DP</td>
    			    <td colspan="6" class="pl-5 pr-5 gras bg-color-blue-fonce">TOTAL PASSIF CIRCULANT</td>
    			    
    			    <td class="txt-center gras bg-color-blue-fonce"></td>
    			    
    				<td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">'.number_format($row["dp"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">'.number_format($row["dp_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="txt-center gras">BQ</td>
    			    <td colspan="8" class="pl-5 pr-5 gras">Titres de placement</td>
    			    
    				
    				<td class="txt-center gras">9</td>
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["bq_brut"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["bq_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["bq_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["bq_net_1"], 0, ',', ' ').'</td>
    			    
    			    
    				<td class="txt-center gras"></td>
    			    <td colspan="6" class="pl-5 pr-5 gras"></td>
    			    
    			    <td class="txt-center gras"></td>
    			    
    				<td colspan="3" class="txt-right gras pr-5"></td>
    				
    			    <td colspan="3" class="txt-right gras pr-5"></td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="txt-center gras">BR</td>
    			    <td colspan="8" class="pl-5 pr-5 gras">Valeurs à encaisser</td>
    			    
    				
    				<td class="txt-center gras">10</td>
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["br_brut"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["br_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["br_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["br_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">DQ</td>
    			    <td colspan="6" class="pl-5 pr-5 gras">Banques, crédits d\'escompte</td>
    			    
    			    <td class="txt-center gras">20</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["dq"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["dq_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="txt-center gras">BS</td>
    			    <td colspan="8" class="pl-5 pr-5 gras">Banques, chèques postaux, caisse et assimilés</td>
    			    
    				
    				<td class="txt-center gras">11</td>
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["bs_brut"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["bs_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["bs_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["bs_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">DR</td>
    			    <td colspan="6" class="pl-5 pr-5 gras">Banques, établissements financiers et crédits de trésorerie</td>
    			    
    			    <td class="txt-center gras">20</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["dr"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["dr_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="gras txt-center">BT</td>
    			    <td colspan="8" class="pl-5 pr-5 gras bg-color-blue-fonce">TOTAL TRESORERIE-ACTIF</td>
    			    
    				
    				<td class="txt-center gras bg-color-blue-fonce"></td>
    			    <td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">'.number_format($row["bt_brut"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">'.number_format($row["bt_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">'.number_format($row["bt_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">'.number_format($row["bt_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">DT</td>
    			    <td colspan="6" class="pl-5 pr-5 gras bg-color-blue-fonce">TOTAL TRESORERIE-PASSIF</td>
    			    
    			    <td class="txt-center gras bg-color-blue-fonce"></td>
    			    
    				<td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">'.number_format($row["dt"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">'.number_format($row["dt_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="txt-center gras">BU</td>
    			    <td colspan="8" class="pl-5 pr-5 gras">Ecarts de conversion-Actif</td>
    			    
    				
    				<td class="txt-center gras">12</td>
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["bu_brut"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["bu_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["bu_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["bu_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">DV</td>
    			    <td colspan="6" class="pl-5 pr-5 gras">Ecarts de conversion-Passif</td>
    			    
    			    <td class="txt-center gras">12</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5">'.number_format($row["dv"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5">'.number_format($row["dv_1"], 0, ',', ' ').'</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td class="gras txt-center">BZ</td>
    			    <td colspan="8" class="pl-5 pr-5 gras bg-color-green-fonce">TOTAL GENERAL</td>
    			    
    				
    				<td class="txt-center gras bg-color-green-fonce"></td>
    			    <td colspan="3" class="txt-right gras pr-5 bg-color-green-fonce">'.number_format($row["bz_brut"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5 bg-color-green-fonce">'.number_format($row["bz_dep"], 0, ',', ' ').'</td>
    			    
    				<td colspan="3" class="txt-right gras pr-5 bg-color-green-fonce">'.number_format($row["bz_net"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5 bg-color-green-fonce">'.number_format($row["bz_net_1"], 0, ',', ' ').'</td>
    			    
    				<td class="txt-center gras">DZ</td>
    			    <td colspan="6" class="pl-5 pr-5 gras bg-color-green-fonce">TOTAL GENERAL</td>
    			    
    			    <td class="txt-center gras bg-color-green-fonce"></td>
    			    
    				<td colspan="3" class="txt-right gras pr-5 bg-color-green-fonce">'.number_format($row["dz"], 0, ',', ' ').'</td>
    				
    			    <td colspan="3" class="txt-right gras pr-5 bg-color-green-fonce">'.number_format($row["dz_1"], 0, ',', ' ').'</td>
    			</tr>
';
}



$output .= '
            </table>
        </div>
        </body>
        </html>
';





// instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->loadHtml($output);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'landscape');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
//$dompdf->stream(); // Télécharge directement le document
$file_name = 'Bilan_'.$date.'_'.$hour2.'.pdf';
$dompdf->stream($file_name, array("Attachment" => false));