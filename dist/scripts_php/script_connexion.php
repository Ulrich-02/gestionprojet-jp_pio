<?php

include('../db.php');

//On vérifie si l'email renvoyé existe dans la base
$query = "
  SELECT * FROM user 
  WHERE email_user = :email_user
";
$statement = $db->prepare($query);
$statement->execute(
    array(
        ':email_user'	=>	$_POST["email_user"]
    )
);

$message = '';
$count = $statement->rowCount();
if($count > 0)
{
    $result = $statement->fetchAll();
    foreach($result as $row)
    {
        if($row['statut_user'] == 'Actif')
        {
            if(password_verify($_POST["mdp_user"], $row["mdp_user"]))
            {
                $_SESSION['id_user'] = $row['id_user'];
                $_SESSION['nom_user'] = $row['nom_user'];
                $_SESSION['prenom_user'] = $row['prenom_user'];
                $_SESSION['fonction_user'] = $row['fonction_user'];
                $_SESSION['email_user'] = $row['email_user'];

                $message = "Compte Ok";
            }
            else
            {
                $message = "Mot de passe erroné";
            }
        }
        else
        {
            $message = "Compte désactivé";
        }
    }
}
else
{
    $message = "Email invalide";
}

echo json_encode($message);

?>