<?php

function fill_projet_list($db)
{
    $query = "
	SELECT * FROM projet 
	WHERE statut_projet = 'Actif' 
	ORDER BY nom_projet ASC 
	";
    $statement = $db->prepare($query);
    $statement->execute();
    $result = $statement->fetchAll();
    $output = '';
    foreach($result as $row)
    {
        $output .= '<option value="'.$row["id_projet"].'">'.$row["nom_projet"].'</option>';
    }
    return $output;
}

function fill_participant_list($db)
{
    $query = "
	SELECT * FROM participants 
	WHERE statut_pp = 'Actif' 
	ORDER BY nom_pp ASC 
	";
    $statement = $db->prepare($query);
    $statement->execute();
    $result = $statement->fetchAll();
    $output = '';
    foreach($result as $row)
    {
        $output .= '<option value="'.$row["id_participants"].'">'.$row["nom_pp"].'</option>';
    }
    return $output;
}

function fill_tache_list($db)
{
    $query = "
	SELECT * FROM taches 
	WHERE statut_tache = 'Actif' 
	ORDER BY nom_tache ASC 
	";
    $statement = $db->prepare($query);
    $statement->execute();
    $result = $statement->fetchAll();
    $output = '';
    foreach($result as $row)
    {
        $output .= '<option value="'.$row["id_tache"].'">'.$row["nom_tache"].'</option>';
    }
    return $output;
}