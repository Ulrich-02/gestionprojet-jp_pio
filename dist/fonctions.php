<?php

    //include('db.php');


    function connected() {
        if(isset($_SESSION['id_compte'])) {
            return true;
        } else {
            return false;
        }
    }


    function dump($variable){
        echo '<pre>';
        var_dump($variable);
        echo '</pre>';
    }

    
    function si_funct($a, $b, $c, $d){
        if($a == $b)
            return $c;
        else
            return $d;
    }

    function si_funct1($a, $b, $c){
        if($a)
            return $b;
        else
            return $c;
    }


    function select($propriete, $table, $db, $selected = '', $condition = '') : string {

        $query = "SELECT DISTINCT $propriete FROM $table $condition";
        $statement = $db->prepare($query);
        $statement->execute();

        $result = $statement->fetchAll();

        

        $output = '';

        foreach ($result as $row) {
            $attributes = ($selected == $row[$propriete]) ? 'selected' : '';
            $output .= '<option value="'.$row[''.$propriete.''].'" '.$attributes.'>'.$row[''.$propriete.''].'</option>'; 
        }

        return $output;
        
    }

    function select1($propriete, $table, $value, $db, $selected = '', $condition = '') : string {

        $query = "SELECT DISTINCT * FROM $table $condition";
        $statement = $db->prepare($query);
        $statement->execute();

        $result = $statement->fetchAll();

        

        $output = '';

        foreach ($result as $row) {
            $attributes = ($selected == $row[$propriete]) ? 'selected' : '';
            $output .= '<option value="'.$row[''.$value.''].'" '.$attributes.'>'.$row[''.$propriete.''].'</option>'; 
        }

        return $output;
        
    }

    function is_in_database($propriete, $table, $value, $db){
        

        $query = "SELECT $propriete FROM $table WHERE $propriete = :value";
        $statement = $db->prepare($query);
        $statement->bindParam(':value', $value);
        $statement->execute();

        $result = $statement->rowCount();
        if($result > 0){
            return true;
        }else{
            return false;
        }
    }

/*
function compareTable($table1, $table2){

    $failledInfos = [];
    foreach ($table1 as $ligne1) {

        $findInfos = false;
        foreach ($table2 as $ligne2) {

            if($ligne1['cpte'] == $ligne2['cpte']){
                $findInfos = true;
            }

        }
        if($findInfos == false){
            $failledInfos[] = $ligne1;
        }

    }
    return $failledInfos;

}
*/



    function compareTable($table1, $table2){

        $failledInfos = [];
        foreach ($table1 as $ligne1) {

            $findInfos = false;
            foreach ($table2 as $ligne2) {

                if(substr($ligne1['cpte'], 0, 4) == substr($ligne2['cpte'], 0, 4)){
                    $findInfos = true;
                }

            }
            if($findInfos == false){
                $failledInfos[] = $ligne1;
            }

        }
        return $failledInfos;

    }


    function somme($values){
        $som = 0;
        foreach ($values as $value) {
            $som += $value;
        }
        return $som;
    }
