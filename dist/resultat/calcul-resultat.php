
<?php require_once(__DIR__ . '/db.php'); ?>
<?php require_once(__DIR__ . '/includes/header.php'); ?>
<?php require_once(__DIR__ . '/fonctions.php'); ?>
<?php require_once(__DIR__ . '/fonctions-sql.php'); ?>


<?php
function val_cpte_debit($cpte, $exp_cpte = [])
{

    global $an, $db;
    $implode_head = '';
    $implode_body = '';
    $implode_foot = '';
    if (!empty($exp_cpte)) {
        $implode_head = "and cpte NOT LIKE '";
        $implode_body = implode("%' and cpte NOT LIKE '", $exp_cpte);
        $implode_foot = "%'";
    }

    $query = "SELECT SUM(sfd) FROM balance_n WHERE cpte LIKE '$cpte%' $implode_head$implode_body$implode_foot";
    $statement = $db->prepare($query);
    $statement->execute();
    $result = $statement->fetch();

    $compte_val = $result['SUM(sfd)'];

    return $compte_val;
}

function val_cpte_credit($cpte, $exp_cpte = [])
{

    global $an, $db;
    $implode_head = '';
    $implode_body = '';
    $implode_foot = '';
    if (!empty($exp_cpte)) {
        $implode_head = "and cpte NOT LIKE '";
        $implode_body = implode("%' and cpte NOT LIKE '", $exp_cpte);
        $implode_foot = "%'";
    }

    $query = "SELECT SUM(sfc) FROM balance_n WHERE cpte LIKE '$cpte%' $implode_head$implode_body$implode_foot";
    $statement = $db->prepare($query);
    $statement->execute();
    $result = $statement->fetch();

    $compte_val = $result['SUM(sfc)'];

    return $compte_val;
}


function debit_cpte($cpte, $exp_cpte = [])
{
    $val_cpte = 0;
    foreach ($cpte as $value) {
        $val_cpte += val_cpte_debit($value, $exp_cpte);
    }
    return $val_cpte;
}

function credit_cpte($cpte, $exp_cpte = [])
{
    $val_cpte = 0;
    foreach ($cpte as $value) {
        $val_cpte += val_cpte_credit($value, $exp_cpte);
    }
    return $val_cpte;
}

function debitcredit_cpte($cpte, $exp_cpte = [])
{
    $valdebit_cpte = 0;
    $valcredit_cpte = 0;
    foreach ($cpte as $value) {
        $valcredit_cpte += val_cpte_credit($value, $exp_cpte);
        $valdebit_cpte += val_cpte_debit($value, $exp_cpte);
    }
    if ($valdebit_cpte > 0 && $valcredit_cpte == 0) {
        return $valdebit_cpte;
    }
    if ($valcredit_cpte > 0 && $valdebit_cpte == 0) {
        return $valcredit_cpte;
    }
}

?>
<?php


//Marge commerciale somme (TA à RB) XA

$ta = credit_cpte([701]);
$ra = 0 - (debit_cpte([601])); // on rend $ra négatif (voir tableau de correspondance)


// La Variation de stock de marchandises peut être débiteur ou créditeur
$rb_debiteur = debit_cpte([6031]);
$rb_crediteur = credit_cpte([6031]);
$rb = 0;
if ($rb_debiteur > 0 && $rb_crediteur == 0) {
    $rb = 0 - $rb_debiteur; //on rend négatif la valeur de rb
}
if ($rb_crediteur > 0 && $rb_debiteur == 0) {
    $rb = $rb_crediteur;
}
if ($rb_crediteur == 0 && $rb_debiteur == 0) {
    $rb = 0;
}

$xa = $ta + $ra + $rb;



//Chiffre d'affaire (A + B + C + D)

$tb = credit_cpte([702, 703, 704]);
$tc = credit_cpte([705, 706]);
$td = credit_cpte([707]);

$xb = $ta + $tb + $tc + $td;



//Valeur ajoutée (XB + RA + RB + somme(TE à RJ))

$te = credit_cpte([73]);
$tf = credit_cpte([72]);
$tg = credit_cpte([71]);
$th = credit_cpte([75]);
$ti = credit_cpte([781]);

$rc = 0 - (debit_cpte([602])); // on rend $rc négatif (voir tableau de correspondance)


// La Variation de stock de matières premières peut être débiteur ou créditeur
$rd_debiteur = debit_cpte([6032]);
$rd_crediteur = credit_cpte([6032]);
$rd = 0;
if ($rd_debiteur > 0 && $rd_crediteur == 0) {
    $rd = 0 - $rd_debiteur; //on rend négatif la valeur de rb
}
if ($rd_crediteur > 0 && $rd_debiteur == 0) {
    $rd = $rd_crediteur;
}
if ($rd_crediteur == 0 && $rd_debiteur == 0) {
    $rd = 0;
}


$re = 0 - (debit_cpte([604, 605, 608])); // on rend $re négatif (voir tableau de correspondance)


// La Variation de stock d'autres approvisionnements peut être débiteur ou créditeur
$rf_debiteur = debit_cpte([6033]);
$rf_crediteur = credit_cpte([6033]);
$rf = 0;
if ($rf_debiteur > 0 && $rf_crediteur == 0) {
    $rf = 0 - $rf_debiteur; //on rend négatif la valeur de rb
}
if ($rf_crediteur > 0 && $rf_debiteur == 0) {
    $rf = $rf_crediteur;
}
if ($rf_crediteur == 0 && $rf_debiteur == 0) {
    $rf = 0;
}



// on rend $rg à $rj négatifs (voir tableau de correspondance)
$rg = 0 - (debit_cpte([61]));
$rh = 0 - (debit_cpte([62, 63]));
$ri = 0 - (debit_cpte([64]));
$rj = 0 - (debit_cpte([65]));


$xc = $xb + $ra + $rb + somme([
    $te,
    $tf,
    $tg,
    $th,
    $ti,
    $rc,
    $rd,
    $re,
    $rf,
    $rg,
    $rh,
    $ri,
    $rj
]);

//Excedent brut d'exploitation (XC + RK)

$rk = 0 - (debit_cpte([66]));

$xd = $xc + $rk;



//Résultat d'exploitation (XD + TJ + RL)

$tj = credit_cpte([791, 798, 799]);
$rl = 0 - (debit_cpte([681, 691]));

$xe = $xd + $tj + $rl;


//Résultat financier somme(TK à RN)

$tk = credit_cpte([77]);
$tl = credit_cpte([797]);
$tm = credit_cpte([787]);
$rm = 0 - (debit_cpte([67]));
$rn = 0 - (debit_cpte([697]));

$xf = somme([
    $tk,
    $tl,
    $tm,
    $rm,
    $rn
]);

//Résultat des activités ordinaire (XE + XF)

$xg = $xe + $xf;

//Résultat hors activités ordinaires somme(TN à RP)

$tn = credit_cpte([82]);
$to = credit_cpte([84, 86, 88]);
$ro = 0 - (debit_cpte([81]));
$rp = 0 - (debit_cpte([83, 85]));

$xh = somme([
    $tn,
    $to,
    $ro,
    $rp
]);

//Résultat net (XG + XH + RQ + RS)

$rq = 0 - (debit_cpte([87]));
$rs = 0 - (debit_cpte([89]));

$xi = $xg + $xh + $rq + $rs;


?>