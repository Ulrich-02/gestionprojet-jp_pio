<?php

function insert($table, $column_value, PDO $db)
{
    $columns = [];
    $values = [];
    foreach ($column_value as $column => $value) {
        $columns[] = $column;
        $values[] = "'" . $value . "'";
    }

    $query = "INSERT INTO " . $table . " (" . implode(',', $columns) . ") VALUES (" . implode(',', $values) . ")";
    if ($db->query($query))
        return true;
    else
        return false;
}
function insert1($table, $column_value, PDO $db)
{
    $columns = [];
    $values = [];
    foreach ($column_value as $column => $value) {
        $columns[] = $column;
        $values[] = "'" . $value . "'";
    }

    $query = "INSERT INTO " . $table . " (" . implode(',', $columns) . ") VALUES (" . implode(',', $values) . ")";
    // if($db->query($query))
    //     return true;
    // else
    //     return false;
    return $query;
}

function update($table, $column_value, $condition, $db)
{
    $db = new PDO('mysql:host=localhost;dbname=mist', 'root', ''/*,[PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]*/);
    $column_value_normal_for_requete = [];
    foreach ($column_value as $column => $value) {
        $column_value_normal_for_requete[] = $column . " = '" . $value . "'";
    }
    $query = "UPDATE " . $table . " SET " . implode(' , ', $column_value_normal_for_requete) . " WHERE " . $condition;
    if ($db->query($query))
        return true;
    else
        return false;
}

function update1($table, $column_value, $condition, $db)
{
    $column_value_normal_for_requete = [];
    foreach ($column_value as $column => $value) {
        $column_value_normal_for_requete[] = $column . " = '" . $value . "'";
    }
    $query = "UPDATE " . $table . " SET " . implode(' , ', $column_value_normal_for_requete) . " WHERE " . $condition;

    return $query;
}

function delete($table, $condition, $db)
{
    $query = "DELETE FROM " . $table . " WHERE " . $condition;
    if ($db->query($query))
        return true;
    else
        return false;
}

function returnSelect($propriete, $table, $condition, PDO $db)
{

    $query = "SELECT $propriete FROM $table WHERE $condition";
    $statement = $db->prepare($query);
    $statement->execute();

    $result = $statement->fetch();
    if ($result["$propriete"] != NULL)
        return $result["$propriete"];
    else
        return 0;
}
function returnSelect1($propriete, $table, $condition, PDO $db)
{

    $query = "SELECT $propriete FROM $table WHERE $condition";
    $statement = $db->prepare($query);
    $statement->execute();

    $result = $statement->fetch();
    return $query;
}
function emptyTable($table, PDO $db)
{
    $query = "SELECT * FROM $table";
    $statement = $db->prepare($query);
    $statement->execute();

    $result = $statement->fetch();
    if (empty($result)) {
        return true;
    } else {
        return false;
    }
}
function tableLineBalance($table, PDO $db)
{
    $query = "SELECT * FROM $table";
    $statement = $db->prepare($query);
    $statement->execute();

    $result = $statement->fetchAll();
    $tableLine = '';
    foreach ($result as $line) {
        $tableLine .= <<<HTML
            <tr>
                <td>{$line['cpte']}</td>
                <td>{$line['inti']}</td>
                <td>{$line['sid']}</td>
                <td>{$line['sic']}</td>
                <td>{$line['md']}</td>
                <td>{$line['mc']}</td>
                <td>{$line['sfd']}</td>
                <td>{$line['sfc']}</td>
            </tr>
HTML;
    }
    return $tableLine;
}
function deleteTable($table, $db)
{
    $query = "TRUNCATE TABLE $table";
    $statement = $db->prepare($query);
    $statement->execute();
}
