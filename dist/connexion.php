<?php

include('db.php');

if(isset($_SESSION['id_user']))
{
    header("location:index.php");
}

?>

<!DOCTYPE html>
<!--
Author: Keenthemes
Product Name: Metronic - Bootstrap 5 HTML, VueJS, React, Angular & Laravel Admin Dashboard Theme
Purchase: https://1.envato.market/EA4JP
Website: http://www.keenthemes.com
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
License: For each use you must have a valid license purchased only from above link in order to legally use the theme for your project.
-->
<html lang="en">
	<!--begin::Head-->
	<head><!--<base href="../../../">-->
		<title>
            Bienvenue sur Mistral
        </title>
		<meta charset="utf-8" />
		<meta name="description" content="The most advanced Bootstrap Admin Theme on Themeforest trusted by 94,000 beginners and professionals. Multi-demo, Dark Mode, RTL support and complete React, Angular, Vue &amp; Laravel versions. Grab your copy now and get life-time updates for free." />
		<meta name="keywords" content="Metronic, bootstrap, bootstrap 5, Angular, VueJs, React, Laravel, admin themes, web design, figma, web development, free templates, free admin themes, bootstrap theme, bootstrap template, bootstrap dashboard, bootstrap dak mode, bootstrap button, bootstrap datepicker, bootstrap timepicker, fullcalendar, datatables, flaticon" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta property="og:locale" content="en_US" />
		<meta property="og:type" content="article" />
		<meta property="og:title" content="Metronic - Bootstrap 5 HTML, VueJS, React, Angular &amp; Laravel Admin Dashboard Theme" />
		<meta property="og:url" content="https://keenthemes.com/metronic" />
		<meta property="og:site_name" content="Keenthemes | Metronic" />
		<link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
		<link rel="shortcut icon" href="assets/fav.PNG" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Global Stylesheets Bundle(used by all pages)-->
		<link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Global Stylesheets Bundle-->
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="bg-body">
		<!--begin::Main-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Authentication - Sign-in -->
			<div class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed" style="background-image: url(assets/media/illustrations/sigma-1/14.png">
				<!--begin::Content-->
				<div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
					<!--begin::Logo-->
					<a href="connexion.php" class="mb-12">
						<img alt="Logo" src="assets/logo-mistral-200.jpg" class="h-40px" />
					</a>
					<!--end::Logo-->
					<!--begin::Wrapper-->
					<div class="w-lg-500px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
						<!--begin::Form-->
						<form class="form w-100" novalidate="novalidate" id="loginForm" method="post" enctype="multipart/form-data">
							<!--begin::Heading-->
							<div class="text-center" style="margin-bottom: 50px;">
								<!--begin::Title-->
								<h1 class="text-dark mb-3">Bienvenue sur Mistral</h1>
								<!--end::Title-->
								<!--begin::Link-->




                                    <!--end::Link-->
							</div>
							<!--begin::Heading-->
							<!--begin::Input group-->
							<div class="fv-row mb-10">
								<!--begin::Label-->
								<label class="form-label fs-6 fw-bolder text-dark">Email</label>
								<!--end::Label-->
								<!--begin::Input-->
								<input class="form-control form-control-lg form-control-solid" type="text" name="email_user" autocomplete="off" required />
								<!--end::Input-->
							</div>
							<!--end::Input group-->
							<!--begin::Input group-->
							<div class="fv-row mb-10">
								<!--begin::Wrapper-->
								<div class="d-flex flex-stack mb-2">
									<!--begin::Label-->
									<label class="form-label fw-bolder text-dark fs-6 mb-0">Mot de passe</label>
									<!--end::Label-->
									<!--begin::Link-->
									<!--end::Link-->
								</div>
								<!--end::Wrapper-->
								<!--begin::Input-->
								<input class="form-control form-control-lg form-control-solid" type="password" name="mdp_user" autocomplete="off" required />
								<!--end::Input-->
							</div>
							<!--end::Input group-->
							<!--begin::Actions-->
							<div class="text-center">
								<!--begin::Submit button-->
                                <!--
								<button type="submit" id="kt_sign_in_submit" class="btn btn-lg btn-primary w-100 mb-5">
									<span class="indicator-label">Connexion</span>
									<span class="indicator-progress">Please wait...
									<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
								</button>
								-->

                                <button type="submit" id="loginBouton" class="btn btn-lg btn-primary w-100 mb-5">

                                </button>

							</div>
							<!--end::Actions-->
						</form>
						<!--end::Form-->
					</div>
					<!--end::Wrapper-->
				</div>
				<!--end::Content-->
				<!--begin::Footer-->
				<div class="d-flex flex-center flex-column-auto p-10">
					<!--begin::Links-->
                    <!--
					<div class="d-flex align-items-center fw-bold fs-6">
						<a href="https://keenthemes.com" class="text-muted text-hover-primary px-2">About</a>
						<a href="mailto:support@keenthemes.com" class="text-muted text-hover-primary px-2">Contact</a>
						<a href="https://1.envato.market/EA4JP" class="text-muted text-hover-primary px-2">Contact Us</a>
					</div>
					-->
					<!--end::Links-->
				</div>
				<!--end::Footer-->
			</div>
			<!--end::Authentication - Sign-in-->
		</div>
		<!--end::Main-->
		<script>var hostUrl = "assets/";</script>
		<!--begin::Javascript-->
		<!--begin::Global Javascript Bundle(used by all pages)-->
		<script src="assets/plugins/global/plugins.bundle.js"></script>
		<script src="assets/js/scripts.bundle.js"></script>
		<!--end::Global Javascript Bundle-->
		<!--begin::Page Custom Javascript(used by this page)-->
		<script src="assets/js/custom/authentication/sign-in/general.js"></script>
		<!--end::Page Custom Javascript-->
		<!--end::Javascript-->


        <script type="text/javascript">

            $(document).ready(function(){


                $('#loginBouton').html("<span class='indicator-label'>Se connecter</span>");


                $(document).on('submit','#loginForm', function(event){
                    event.preventDefault();

                    //window.location = "balances/balance-n/balance-n-plein.php";

                    $('#loginBouton').html("<span>Vérification en cours...<span class='spinner-border spinner-border-sm align-middle ms-2'></span></span>");


                    $.ajax({
                        url:'scripts_php/script_connexion.php',
                        type: "POST",
                        data:  new FormData(this),
                        contentType: false,
                        cache: false,
                        processData:false,
                        dataType:"json",
                        success:function(data) {
                            //console.log(data);

                            if(data == "Compte Ok") {

                                //On replace le texte initial du bouton
                                $('#loginBouton').html("<span class='indicator-label'>Se connecter</span>");

                                Swal.fire({
                                    text: "Les paramètres de connexion sont corrects !",
                                    icon: "success",
                                    buttonsStyling: !1,
                                    confirmButtonText: "Continuer",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                }).then((value) => {
                                    window.location = "index.php";
                                });

                            } else if (data == 'Mot de passe erroné') {

                                //$('#loginForm')[0].reset();

                                //On replace le texte initial du bouton
                                $('#loginBouton').html("<span class='indicator-label'>Se connecter</span>");

                                Swal.fire({
                                    text: "Le mot de passe est incorrect !",
                                    icon: "error",
                                    buttonsStyling: !1,
                                    confirmButtonText: "Ok, c'est compris !",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                });

                            } else if (data == 'Compte désactivé') {

                                //$('#loginForm')[0].reset();

                                //On replace le texte initial du bouton
                                $('#loginBouton').html("<span class='indicator-label'>Se connecter</span>");

                                Swal.fire({
                                    text: "Votre compte a été désactivé par l'administrateur !",
                                    icon: "error",
                                    buttonsStyling: !1,
                                    confirmButtonText: "Ok, c'est compris !",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                });

                            } else {

                                //$('#loginForm')[0].reset();

                                //On replace le texte initial du bouton
                                $('#loginBouton').html("<span class='indicator-label'>Se connecter</span>");

                                Swal.fire({
                                    text: "L'email saisi est incorrect.",
                                    icon: "error",
                                    buttonsStyling: !1,
                                    confirmButtonText: "Ok, c'est compris !",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                });

                            }

                        }
                    });



                });

            });
        </script>



	</body>
	<!--end::Body-->
</html>