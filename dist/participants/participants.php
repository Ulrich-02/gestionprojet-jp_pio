<?php

$participants = 'active';

include('fetch_participants.php'); // ce fichier a déjà le db.php qui a à son tour session_start
/*
if(!isset($_SESSION['id_user']))
{
    header("location:../../connexion.php");
    exit();
}

$query = "SELECT cpte FROM balance_n ";
$statement = $db->prepare($query);
$statement->execute();
$count = $statement->rowCount();

if($count == 0)
{
    header("location:balance-n-vide.php");
    exit();
}
*/
?>


<!DOCTYPE html>
<!--
Author: Keenthemes
Product Name: Metronic - Bootstrap 5 HTML, VueJS, React, Angular & Laravel Admin Dashboard Theme
Purchase: https://1.envato.market/EA4JP
Website: http://www.keenthemes.com
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
License: For each use you must have a valid license purchased only from above link in order to legally use the theme for your project.
-->
<html lang="en">
	<!--begin::Head-->
	<head><base href="../">
		<title>Mistral - Lien de Paiement</title>
		<meta charset="utf-8" />
		<meta name="description" content="The most advanced Bootstrap Admin Theme on Themeforest trusted by 94,000 beginners and professionals. Multi-demo, Dark Mode, RTL support and complete React, Angular, Vue &amp; Laravel versions. Grab your copy now and get life-time updates for free." />
		<meta name="keywords" content="Metronic, bootstrap, bootstrap 5, Angular, VueJs, React, Laravel, admin themes, web design, figma, web development, free templates, free admin themes, bootstrap theme, bootstrap template, bootstrap dashboard, bootstrap dak mode, bootstrap button, bootstrap datepicker, bootstrap timepicker, fullcalendar, datatables, flaticon" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta property="og:locale" content="en_US" />
		<meta property="og:type" content="article" />
		<meta property="og:title" content="Metronic - Bootstrap 5 HTML, VueJS, React, Angular &amp; Laravel Admin Dashboard Theme" />
		<meta property="og:url" content="https://keenthemes.com/metronic" />
		<meta property="og:site_name" content="Keenthemes | Metronic" />
		<link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
        <link rel="shortcut icon" href="assets/fav.PNG" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Page Vendor Stylesheets(used by this page)-->
		<link href="assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Page Vendor Stylesheets-->
		<!--begin::Global Stylesheets Bundle(used by all pages)-->
		<link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Global Stylesheets Bundle-->

        <style>
            .bg-gris-clair {
                background-color: #e9ebec;
            }
        </style>
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed aside-fixed aside-secondary-disabled">
		<!--begin::Main-->
		<!--begin::Root-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Page-->
			<div class="page d-flex flex-row flex-column-fluid">
				<!--begin::Aside-->
				<div id="kt_aside" class="aside" data-kt-drawer="true" data-kt-drawer-name="aside" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="auto" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_aside_toggle">
					<!--begin::Logo-->
					<div class="aside-logo flex-column-auto pt-10 pt-lg-20" id="kt_aside_logo">
                        <a href="index.php">
                            <img alt="Logo" src="assets/mascotte-89.jpg" class="h-40px" />
                        </a>
					</div>
					<!--end::Logo-->

                    <!--begin::Nav-->
                    <?php include '../parts/menu2.php';?>
                    <!--end::Nav-->

                    <!--begin::Footer-->
                    <?php include '../parts/footer.php';?>
                    <!--end::Footer-->
				</div>
				<!--end::Aside-->
				<!--begin::Wrapper-->
				<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
					<!--begin::Header tablet and mobile-->
					<div class="header-mobile py-3">
						<!--begin::Container-->
						<div class="container d-flex flex-stack">
							<!--begin::Mobile logo-->
							<div class="d-flex align-items-center flex-grow-1 flex-lg-grow-0">
                                <a href="index.php">
                                    <img alt="Logo" src="assets/mascotte-89.jpg" class="h-35px" />
                                </a>
							</div>
							<!--end::Mobile logo-->
							<!--begin::Aside toggle-->
							<button class="btn btn-icon btn-active-color-primary" id="kt_aside_toggle">
								<!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg-->
								<span class="svg-icon svg-icon-2x me-n1">
									<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
										<path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="black" />
										<path opacity="0.3" d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z" fill="black" />
									</svg>
								</span>
								<!--end::Svg Icon-->
							</button>
							<!--end::Aside toggle-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Header tablet and mobile-->
					<!--begin::Header-->
					<div id="kt_header" class="header py-6 py-lg-0" data-kt-sticky="true" data-kt-sticky-name="header" data-kt-sticky-offset="{lg: '300px'}">
						<!--begin::Container-->
						<div class="header-container container-xxl">
							<!--begin::Page title-->
							<div class="page-title d-flex flex-column align-items-start justify-content-center flex-wrap me-lg-20 py-3 py-lg-0 me-3">
								<!--begin::Heading-->
								<h1 class="d-flex flex-column text-dark fw-bolder my-1">
									<span class="text-white fs-1">Participants</span>
								</h1>
								<!--end::Heading-->
								<!--begin::Breadcrumb-->
                                <!--
								<ul class="breadcrumb breadcrumb-line fw-bold fs-7 my-1">
									<li class="breadcrumb-item text-gray-600">
										<a href="../../demo9/dist/index.html" class="text-gray-600">Home</a>
									</li>
									<li class="breadcrumb-item text-gray-600">Apps</li>
									<li class="breadcrumb-item text-gray-600">Customers</li>
									<li class="breadcrumb-item text-gray-400">Customer Listing</li>
								</ul>
								-->
								<!--end::Breadcrumb-->
							</div>
							<!--end::Page title=-->
							<!--begin::Wrapper-->
							<div class="d-flex align-items-center flex-wrap">

								<!--begin::Action-->
								<div class="d-flex align-items-center py-3 py-lg-0">

									<div class="me-3">
										<a href="#" class="btn btn-icon btn-custom btn-active-color-primary" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
											<!--begin::Svg Icon | path: icons/duotune/communication/com013.svg-->
											<span class="svg-icon svg-icon-1 svg-icon-white">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
													<path d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z" fill="black" />
													<rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="black" />
												</svg>
											</span>
											<!--end::Svg Icon-->
										</a>
										<!--begin::Menu-->
                                        <?php include '../parts/user-connect.php';?>
										<!--end::Menu-->
									</div>
                                    <!--a href="nouveau_client.php"-->
									<button type="button" class="btn btn-primary reset" data-bs-toggle="modal" data-bs-target="#kt_modal_add_customer">Nouveau Participant</button>
									<!--/a-->
								</div>
								<!--end::Action-->
							</div>
							<!--end::Wrapper-->
						</div>
						<!--end::Container-->
						<div class="header-offset"></div>
					</div>
					<!--end::Header-->
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Container-->
						<div class="container-xxl" id="kt_content_container">
							<!--begin::Card-->
							<div class="card">
								<!--begin::Card header-->
								<div class="card-header border-0 pt-6">
									<!--begin::Card title-->
									<div class="card-title">
										<!--begin::Search-->
										<div class="d-flex align-items-center position-relative my-1">
											<!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
											<span class="svg-icon svg-icon-1 position-absolute ms-6">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
													<rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black" />
													<path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black" />
												</svg>
											</span>
											<!--end::Svg Icon-->
											<input type="text" data-kt-customer-table-filter="search" class="form-control form-control-solid w-250px ps-15" placeholder="Recherche" />
										</div>
										<!--end::Search-->
									</div>
									<!--begin::Card title-->
									<!--begin::Card toolbar-->
									<div class="card-toolbar">
										<!--begin::Toolbar-->
										<div class="d-flex justify-content-end" data-kt-customer-table-toolbar="base">


											<!--begin::Export-->
                                            <!--a href="balances/balance-n-1/balance-n-1.php" class="btn btn-light-primary"--> <!--Aller vers la Balance N-1-->
											
											<!--begin::Svg Icon | path: icons/duotune/arrows/arr078.svg-->
											<!--span class="svg-icon svg-icon-2">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <path d="M10 4L18 12L10 20H14L21.3 12.7C21.7 12.3 21.7 11.7 21.3 11.3L14 4H10Z" fill="black"/>
                                                <path opacity="0.3" d="M3 4L11 12L3 20H7L14.3 12.7C14.7 12.3 14.7 11.7 14.3 11.3L7 4H3Z" fill="black"/>
                                                </svg>
											</span-->
											<!--end::Svg Icon-->
                                            <!--/a-->


											<!--end::Export-->
											<!--begin::Add customer-->
                                            <!--
											<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#kt_modal_add_customer">Add Customer</button>
											-->
											<!--end::Add customer-->
										</div>
										<!--end::Toolbar-->
										<!--begin::Group actions-->
										<div class="d-flex justify-content-end align-items-center d-none" data-kt-customer-table-toolbar="selected">
											<div class="fw-bolder me-5">
											<span class="me-2" data-kt-customer-table-select="selected_count"></span>Selected</div>
											<button type="button" class="btn btn-danger" data-kt-customer-table-select="delete_selected">Delete Selected</button>
										</div>
										<!--end::Group actions-->
									</div>
									<!--end::Card toolbar-->
								</div>
								<!--end::Card header-->
								<!--begin::Card body-->
								<div class="card-body pt-0">
									<!--begin::Table-->
									<!--<table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_customers_table">-->
									<table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_customers_table">

										<!--begin::Table head-->
										<thead>
											<!--begin::Table row-->
											<tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
												<th class="min-w-60px" style="text-transform: none;">Nom</th>
												<th class="min-w-60px" style="text-transform: none;">Prenom</th>
												<th class="min-w-60px" style="text-transform: none;">Sexe</th>
												<th class="min-w-60px" style="text-transform: none;">Fonction</th>
												<th class="min-w-60px" style="text-transform: none;">Telephone</th>
												<th class="min-w-60px" style="text-transform: none;">E-mail</th>
												<th class="min-w-60px" style="text-transform: none;">Statuts</th>
												<!-- <th class="min-w-75px" style="text-transform: none;">Participants</th> -->
												<!-- <th class="min-w-75px" style="text-transform: none;">% Finition</th> -->
												<!--th class="min-w-125px" style="text-transform: none;">Actions</th-->
												<th class="text-end min-w-70px">Actions</th>
											</tr>
											<!--end::Table row-->
										</thead>
										<!--end::Table head-->


                                        <tbody>

										<?php echo participants_data($db); ?>

                                        </tbody>



									</table>
									<!--end::Table-->


                                    <!--
                                    <table id="balance_data">
                                        <tr>
                                            <th>Compte</th>
                                            <th>Compte</th>
                                            <th>Compte</th>
                                            <th>Compte</th>
                                            <th>Compte</th>
                                            <th>Compte</th>
                                            <th>Compte</th>
                                            <th>Compte</th>
                                        </tr>
                                    </table>
                                    -->

								</div>
								<!--end::Card body-->
							</div>
							<!--end::Card-->
							<!--begin::Modals-->



							<!-- Modal D'ajout de Nouveau Projets -->
							<div class="modal fade form_modal" id="kt_modal_add_customer" tabindex="-1" aria-hidden="true">
									<!--begin::Modal dialog-->
									<div class="modal-dialog modal-dialog-centered mw-650px">
										<!--begin::Modal content-->
										<div class="modal-content">
											<!--begin::Form-->
											<form class="form "  method="post" id="kt_modal_add_customer_form" data-kt-redirect="#">
												<!--begin::Modal header-->
												<div class="modal-header form_modif" id="kt_modal_add_customer_header">
													<!--begin::Modal title-->
													<h2 class="fw-bolder" id="kt_modal_add_customer_header_title" >Ajout Participant</h2>
													<!--end::Modal title-->
													<!--begin::Close-->
													<div id="kt_modal_add_customer_close" class="btn btn-icon btn-sm btn-active-icon-primary">
														<!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
														<span class="svg-icon svg-icon-1">
															<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
																<rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
															</svg>
														</span>
														<!--end::Svg Icon-->
													</div>
													<!--end::Close-->
												</div>
												<!--end::Modal header-->
												<!--begin::Modal body-->
												<div class="modal-body py-10 px-lg-17">
													<!--begin::Scroll-->
													<div class="scroll-y me-n7 pe-7" id="kt_modal_add_customer_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_customer_header" data-kt-scroll-wrappers="#kt_modal_add_customer_scroll" data-kt-scroll-offset="300px">
														<!--begin::Input group-->
														
                                                        <div class="row g-9 mb-7">
																<!--begin::Col-->
																<div class="col-md-6 fv-row">
																	<!--begin::Label-->
																	<label class="required fs-6 fw-bold mb-2">Nom</label>
																	<!--end::Label-->
																	<!--begin::Input-->
																	<input class="form-control form-control-solid" placeholder="" name="nom_pp" id="nom_pp" value="" />
																	<!--end::Input-->
																</div>
																<!--end::Col-->
																<!--begin::Col-->
																<div class="col-md-6 fv-row">
																	<!--begin::Label-->
																	<label class="required fs-6 fw-bold mb-2">Prenom</label>
																	<!--end::Label-->
																	<!--begin::Input-->
																	<input class="form-control form-control-solid" placeholder="" name="prenom_pp" id="prenom_pp" value="" />
																	<!--end::Input-->
																</div>
																<!--end::Col-->
														</div>


														<!--end::Input group-->
														<!--begin::Input group-->
														<!-- <div class="fv-row mb-7"> -->
															<!--begin::Label-->
															<!-- <label class="fs-6 fw-bold mb-2">
																<span class="required">Email</span>
																<i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Email address must be active"></i>
															</label> -->
															<!--end::Label-->
															<!--begin::Input-->
															<!-- <input type="email" class="form-control form-control-solid" placeholder="sean@yahoo.fr" name="email_personnel" id="email_personnel" value="" /> -->
															<!--end::Input-->
														<!-- </div> -->
														<!--end::Input group-->
														<!--begin::Input group-->

                                                        <div class="row g-9 mb-7">
																<!--begin::Col-->
																<div class="col-md-6 fv-row">
																	<!--begin::Label-->
																	<label class="required fs-6 fw-bold mb-2">Sexe</label>
																	<!--end::Label-->
																	<!--begin::Input-->
																	<select class="form-control form-control-solid" placeholder="" name="sexe_pp" id="sexe_pp" >
																		<option value="Masculin">Masculin</option>
																		<option value="Feminin">Feminin</option>
																	</select>
																	<!--end::Input-->
																</div>
																<!--end::Col-->
																<!--begin::Col-->
																<div class="col-md-6 fv-row">
																	<!--begin::Label-->
																	<label class="required fs-6 fw-bold mb-2">Fonction</label>
																	<!--end::Label-->
																	<!--begin::Input-->
																	<input class="form-control form-control-solid" placeholder="" name="fonction_pp" id="fonction_pp" value="" />
																	<!--end::Input-->
																</div>
																<!--end::Col-->
														</div>

														<div class="row g-9 mb-7">
															<div class="col-md-6 fv-row">
																		<!--begin::Label-->
																		<label class="required fs-6 fw-bold mb-2">Telephone</label>
																		<!--end::Label-->
																		<!--begin::Input-->
																		<input class="form-control form-control-solid" type="number" placeholder="" name="tel_pp" id="tel_pp" value="" />
																		<!--end::Input-->
																	</div>
																	<!--end::Col-->
																	<!--begin::Col-->
																	<div class="col-md-6 fv-row">
																		<!--begin::Label-->
																		<label class="required fs-6 fw-bold mb-2">E-mail</label>
																		<!--end::Label-->
																		<!--begin::Input-->
																		<input class="form-control form-control-solid" type="email" placeholder="sean@yahoo.fr" name="mail_pp" id="mail_pp" value="" />
																		<!--end::Input-->
																	</div>
																	<!--end::Col-->
														</div>
													</div>
												</div>
												<!--end::Modal body-->
												<!--begin::Modal footer-->
												<div class="modal-footer flex-center">
													<!--begin::Button-->
													<button type="reset" id="kt_modal_add_customer_cancel" class="btn btn-light me-3">Annuler</button>
													<!--end::Button-->
													<!--begin::Button-->
													<!-- <button type="submit" id="kt_modal_add_customer_submit" class="btn btn-primary">
														<span class="indicator-label">Envoyer</span>
														<span class="indicator-progress">Please wait...
														<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
													</button> -->
                                                    <button type="submit"  id="add_personnel_bouton" class="btn btn-primary"></button>
                                                    <input type="text" class="form-control form-control-solid" placeholder="" id="btn_action" name="btn_action" value="" />
                                                    <!-- <input type="text" class="form-control form-control-solid" placeholder="" id="id_personnel_modif" name="id_personnel_modif"/> -->
                                                    <!-- <input type="text" class="form-control form-control-solid" placeholder="" id="btn_action_modif" name="btn_action_modif"/> -->
													<!--end::Button-->
												</div>
												<!--end::Modal footer-->
											</form>
											<!--end::Form-->
										</div>
									</div>
								</div>




                            <!-- <div class="modal fade" id="deleteModal" tabindex="-1" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered mw-650px">
                                    <div class="modal-content">

                                        <form class="form" method="post" id="deleteForm" enctype="multipart/form-data">
                                            <div class="modal-header" id="">
                                                <h2 class="fw-bolder">Supprimer la balance</h2>
                                                <div id="fermerModal" class="btn btn-icon btn-sm btn-active-icon-primary">
                                                    <span class="svg-icon svg-icon-1">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                                                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
                                                        </svg>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="modal-body py-10 px-lg-17">
                                                <div class="scroll-y me-n7 pe-7" id="" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_customer_header" data-kt-scroll-wrappers="#kt_modal_add_customer_scroll" data-kt-scroll-offset="300px">
                                                    <div class="fv-row mb-7">
                                                        <label class="fs-6 fw-bold mb-2">Êtes-vous sûr(e) de vouloir supprimer toutes les données de la balance ?</label>
                                                    </div>




                                                </div>
                                            </div>
                                            <div class="modal-footer flex-center">
                                                <button type="reset" id="annulerModal" class="btn btn-light me-3">Annuler</button>
                                                <button type="submit" id="deleteBouton" class="btn btn-primary">

                                                </button>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div> -->











							
					</div>
					<!--end::Content-->
					<!--begin::Footer-->
					<div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
						<!--begin::Container-->
						<div class="container-xxl d-flex flex-column flex-md-row flex-stack">
                            <?php include '../parts/copyright.php';?>
						</div>
						<!--end::Container-->
					</div>
					<!--end::Footer-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Page-->
		</div>
		<!--end::Root-->
		<!--begin::Drawers-->
		<!--begin::Activities drawer-->
		
		<!--end::Activities drawer-->
		<!--begin::Exolore drawer toggle-->
        <!--
		<button id="kt_explore_toggle" class="explore-toggle btn btn-sm bg-body btn-color-gray-700 btn-active-primary shadow-sm position-fixed px-5 fw-bolder zindex-2 top-50 mt-10 end-0 transform-90 fs-6 rounded-top-0" title="Explore Metronic" data-bs-toggle="tooltip" data-bs-placement="right" data-bs-trigger="hover">
			<span id="kt_explore_toggle_label">Explore</span>
		</button>
		-->
		<!--end::Exolore drawer toggle-->
		<!--begin::Exolore drawer-->
		
		<!--end::Exolore drawer-->
		<!--begin::Chat drawer-->
		
		<!--end::Chat drawer-->
		<!--end::Drawers-->
		<!--begin::Modals-->
		<!--begin::Modal - Invite Friends-->
		
		<!--end::Scrolltop-->
		<!--end::Main-->
		<script>var hostUrl = "assets/";</script>
		<!--begin::Javascript-->
		<!--begin::Global Javascript Bundle(used by all pages)-->
		<script src="assets/plugins/global/plugins.bundle.js"></script>
		<script src="assets/js/scripts.bundle.js"></script>
		<!--end::Global Javascript Bundle-->
		<!--begin::Page Vendors Javascript(used by this page)-->

		<script src="assets/plugins/custom/datatables/datatables.bundle.js"></script>

		<script src="assets/plugins/custom/jstree/jstree.bundle.js"></script>
		<!--end::Page Vendors Javascript-->
		<!--begin::Page Custom Javascript(used by this page)-->
		<script src="assets/js/custom/apps/customers/list/export.js"></script>
		<script src="assets/js/custom/apps/customers/list/list.js"></script>
		<script src="assets/js/custom/apps/customers/add.js"></script>
		<script src="assets/js/custom/widgets.js"></script>
		<script src="assets/js/custom/apps/chat/chat.js"></script>
		<script src="assets/js/custom/modals/create-app.js"></script>
		<script src="assets/js/custom/modals/upgrade-plan.js"></script>
		<!--end::Page Custom Javascript-->

        <!--<script src="assets/js/jquery.datatables.js"></script>-->


        <script type="text/javascript">



            $(document).ready(function(){

				// Add Personnel

                $('#btn_action').hide();
                // $('#btn_action_modif').hide();
                $('#id_personnel_modif').hide();

				// $(document).on('click', '.reset', function(){
				// 	$('#kt_modal_add_customer_form')[0].reset();
				// 	$('#kt_modal_add_customer_header_title').text("Ajouter Personnel");
				// 	$('#add_personnel_bouton').html("<span class='indicator-label'>Envoyer</span>");
				// 	$('#btn_action').val('insertion');
				// 	document.getElementByClassName("form_modal").id = "kt_modal_add_customer_form";
				// });

				$('#add_personnel_bouton').html("<span class='indicator-label'>Envoyer</span>");
				$('#btn_action').val('insertion');
				

				// if($('#btn_action').val() == 'insertion'){
						$(document).on('submit','#kt_modal_add_customer_form', function(event){
						event.preventDefault();

						$('#add_personnel_bouton').html("<span>Loading...<span class='spinner-border spinner-border-sm align-middle ms-2'></span></span>");
						
						$.ajax({
							url:'participants/participants_actions.php',
							type: "POST",
							data: new FormData(this),
							contentType: false,
							cache: false,
							processData:false,
							dataType:"json",
							success:function(data) {
								//console.log(data);

								//On replace le texte initial du bouton
								$('#add_personnel_bouton').html("<span class='indicator-label'>Envoyer</span>");

								if(data == "insertion ok") {
									// $('#kt_modal_add_customer_form')[0].reset();
									Swal.fire({
										text: "Le Personnel a été créé avec succès.",
										icon: "success",
										buttonsStyling: !1,
										confirmButtonText: "Ok, c'est compris !",
										customClass: {
											confirmButton: "btn btn-primary"
										}
									}).then((value) => {
										window.location = "participants/participants.php";
									});

								} else if (data == 'insertion pas ok') {

								//    $('#fichierForm')[0].reset();
								// $('#kt_modal_add_customer_form')[0].reset();

									//On replace le texte initial du bouton
									$('#importerBouton').html("<span class='indicator-label'>Reprener</span>");

									Swal.fire({
										text: "Une Erreur s'est produite lors de la création du personnel!",
										icon: "error",
										buttonsStyling: !1,
										confirmButtonText: "Ok, c'est compris !",
										customClass: {
											confirmButton: "btn btn-primary"
										}
									});

								} 
								else if (data == 'Champs vide') {

									// $('#kt_modal_add_customer_form')[0].reset();
								// $('#fichierForm')[0].reset();

									//On replace le texte initial du bouton
									$('#importerBouton').html("<span class='indicator-label'>Reprener</span>");

									$('#fichierModal').modal('hide');

									Swal.fire({
										text: "Remplisser tous les champs !",
										icon: "error",
										buttonsStyling: !1,
										confirmButtonText: "Ok, c'est compris !",
										customClass: {
											confirmButton: "btn btn-primary"
										}
									});

								}
								
								
								// Modification....>>
								else if(data == "modif ok") {
									$('#kt_modal_modif_customer_form').modal('hide');
									Swal.fire({
												text: "Le Personnel a été modifier avec succès.",
												icon: "success",
												buttonsStyling: !1,
												confirmButtonText: "Ok, c'est compris !",
												customClass: {
													confirmButton: "btn btn-primary"
												}
											}).then((value) => {
												window.location = "personnels.php";
											});
								}


								else if(data == "modif pas ok") {
									$('#kt_modal_modif_customer_form').modal('hide');
									$('#importerBouton').html("<span class='indicator-label'>Reprener</span>");

											Swal.fire({
												text: "Une Erreur s'est produite lors de la modification du personnel!",
												icon: "error",
												buttonsStyling: !1,
												confirmButtonText: "Ok, c'est compris !",
												customClass: {
													confirmButton: "btn btn-primary"
												}
											});
								}

							}
						});
					});
				// }
                

            });

			// Modifier Modal----fetch de la donne selectionnee

			/* fetch single */
			$(document).on('click', '.update', function(){
              var id_personnel_modif = $(this).attr("id");
              var btn_action = 'fetch_single';
			  
              $.ajax({
                  url:"script_add_personnel.php",
                  method:"POST",
                  data:{id_personnel_modif:id_personnel_modif, btn_action:btn_action},
                  dataType:"json",
                  success:function(data)
                  {
                      $('#kt_modal_add_customer').modal('show');
                      $('#nom_personnel').val(data.nom_personnel);
                      $('#prenom_personnel').val(data.prenom_personnel);
                      $('#email_personnel').val(data.email_personnel);
                      $('#poste_personnel').val(data.poste_personnel);
                      $('#salaire_personnel').val(data.salaire_personnel);
                      $('#kt_modal_add_customer_header_title').text("Modifier Personnel");
                      $('#add_personnel_bouton').text("Modifier");
                      $('#id_personnel_modif').val(id_personnel_modif);
                      $('#btn_action').val("modifier");
					  document.getElementByClassName("form_modal").id = "kt_modal_modif_customer_form";
                  }
              })
          });


      			/* Modifier Submit */

				// if($('#btn_action').val() == 'fetch_single'){
						$(document).on('submit','#kt_modal_modif_customer_form', function(event){
						event.preventDefault();

						$('#add_personnel_bouton').html("<span>Modif en cours...<span class='spinner-border spinner-border-sm align-middle ms-2'></span></span>");

						var form_data = $(this).serialize();
						$.ajax({
							url:"script_add_personnel.php",
							method:"POST",
							data:form_data,
							dataType:"json",
							success:function(data)
							{
								//console.log(data);
								if(data == "modif ok") {
									$('#kt_modal_modif_customer_form').modal('hide');
									Swal.fire({
												text: "Le Personnel a été modifier avec succès.",
												icon: "success",
												buttonsStyling: !1,
												confirmButtonText: "Ok, c'est compris !",
												customClass: {
													confirmButton: "btn btn-primary"
												}
											}).then((value) => {
												window.location = "personnels.php";
											});
								}


								else if(data == "modif pas ok") {
									$('#kt_modal_modif_customer_form').modal('hide');
									$('#importerBouton').html("<span class='indicator-label'>Reprener</span>");

											Swal.fire({
												text: "Une Erreur s'est produite lors de la modification du personnel!",
												icon: "error",
												buttonsStyling: !1,
												confirmButtonText: "Ok, c'est compris !",
												customClass: {
													confirmButton: "btn btn-primary"
												}
											});
								}

								else if (data == 'champs vide') {

									// $('#kt_modal_add_customer_form')[0].reset();
									// $('#fichierForm')[0].reset();

									//On replace le texte initial du bouton
									$('#importerBouton').html("<span class='indicator-label'>Reprener</span>");

									$('#fichierModal').modal('hide');

									Swal.fire({
										text: "Remplisser tous les champs !",
										icon: "error",
										buttonsStyling: !1,
										confirmButtonText: "Ok, c'est compris !",
										customClass: {
											confirmButton: "btn btn-primary"
										}
									});

									} 


								$('#kt_modal_add_customer_form')[0].reset();
								//   chambredataTable.ajax.reload();
							}
						})
					});
				// }
          

			


        </script>


		<!--end::Javascript-->
	</body>
	<!--end::Body-->
</html>