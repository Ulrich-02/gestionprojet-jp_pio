<!--begin::Copyright-->
<div class="text-dark order-2 order-md-1">
    <span class="text-gray-400 fw-bold me-1">Conception: </span>
    <a href="#" class="text-muted text-hover-primary fw-bold me-2 fs-6">Digital Professional Alliance</a>
</div>
<!--end::Copyright-->
<!--begin::Menu-->
<ul class="menu menu-gray-600 menu-hover-primary fw-bold order-1">
    <li class="menu-item">
        <a href="#" class="menu-link px-2">Mistral &copy; <?php echo date('Y'); ?> - Tous droits réservés</a>
    </li>
</ul>
<!--end::Menu-->