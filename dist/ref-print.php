<?php
$title = 'Affichage références';
require_once('fonctions.php');
require_once('fonctions-sql.php');
require_once('ref-action.php');
?>

<?php
if (isset($_GET['ref'])) {
    $ref = $_GET['ref'];
}
?>

<html>
<head>
    <title>REF</title>
</head>

<body>

<section class="container">

    <h1>Référence <?= $ref ?></h1>
    <?php

    function val_cpte_debit($cpte, $exp_cpte = [])
    {
        //global $an, $db;
        $db = new PDO('mysql:host=localhost;dbname=mist', 'root', ''/*,[PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]*/);
        $implode_head = '';
        $implode_body = '';
        $implode_foot = '';
        if (!empty($exp_cpte)) {
            $implode_head = "and cpte NOT LIKE '";
            $implode_body = implode("%' and cpte NOT LIKE '", $exp_cpte);
            $implode_foot = "%'";
        }

        $query = "SELECT SUM(sfd) FROM balance_n WHERE cpte LIKE '$cpte%' $implode_head$implode_body$implode_foot";
        $statement = $db->prepare($query);
        $statement->execute();
        $result = $statement->fetch();

        $compte_val = $result['SUM(sfd)'];

        return $compte_val;
    }

    function val_cpte_credit($cpte, $exp_cpte = [])
    {

        //global $an, $db;
        $db = new PDO('mysql:host=localhost;dbname=mist', 'root', ''/*,[PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]*/);
        $implode_head = '';
        $implode_body = '';
        $implode_foot = '';
        if (!empty($exp_cpte)) {
            $implode_head = "and cpte NOT LIKE '";
            $implode_body = implode("%' and cpte NOT LIKE '", $exp_cpte);
            $implode_foot = "%'";
        }

        $query = "SELECT SUM(sfc) FROM balance_n WHERE cpte LIKE '$cpte%' $implode_head$implode_body$implode_foot";
        $statement = $db->prepare($query);
        $statement->execute();
        $result = $statement->fetch();

        $compte_val = $result['SUM(sfc)'];

        return $compte_val;
    }

    function brut_cpte($cpte, $exp_cpte = [])
    {
        $val_cpte = 0;
        foreach ($cpte as $value) {
            $val_cpte += val_cpte_debit($value, $exp_cpte);
        }
        return $val_cpte;
    }

    function dep_cpte($cpte, $exp_cpte = [])
    {
        $val_cpte = 0;
        foreach ($cpte as $value) {
            $val_cpte += val_cpte_credit($value, $exp_cpte);
        }
        return $val_cpte;
    }

    /////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////// ACTIF ////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////
    $ae_brut = brut_cpte([211, 2181, 2191]);
    $ae_dep = dep_cpte([2811, 2818, 2911, 2918, 2919]);

    $af_brut = brut_cpte([212, 213, 214, 2193]);
    $af_dep = dep_cpte([2812, 2813, 2814, 2912, 2913, 2914, 2919]);

    $ag_brut = brut_cpte([215, 216]);
    $ag_dep = dep_cpte([2815, 2816, 2915, 2916]);

    $ah_brut = brut_cpte([217, 218, 2198], [2181]);
    $ah_dep = dep_cpte([2817, 2818, 2917, 2918, 2919]);

    $aj_brut = brut_cpte([22]);
    $aj_dep = dep_cpte([282, 292]);

    $ak_brut = brut_cpte([231, 232, 233, 237, 2391]);
    $ak_dep = dep_cpte([2831, 2832, 2833, 2837, 2931, 2932, 2933, 2937, 2939]);

    $al_brut = brut_cpte([234, 235, 238, 2392, 2393]);
    $al_dep = dep_cpte([2834, 2835, 2838, 2934, 2935, 2938, 2939]);

    $am_brut = brut_cpte([24], [245, 2495]);
    $am_dep = dep_cpte([284, 294, 2949], [2845, 2945, 2949]);

    $an_brut = brut_cpte([245, 2495]);
    $an_dep = dep_cpte([2845, 2945, 2949]);

    $ap_brut = brut_cpte([251, 252]);
    $ap_dep = dep_cpte([2951, 2952]);

    $ar_brut = brut_cpte([26]);
    $ar_dep = dep_cpte([296]);

    $as_brut = brut_cpte([27]);
    $as_dep = dep_cpte([297]);

    $ba_brut = brut_cpte([485, 488]);
    $ba_dep = dep_cpte([498]);

    $bb_brut = brut_cpte([31, 32, 33, 34, 35, 36, 37, 38]);
    $bb_dep = dep_cpte([39]);

    

    $bh_brut = brut_cpte([409]);
    $bh_dep = dep_cpte([490]);

    $bi_brut = brut_cpte([41], [419]);
    $bi_dep = dep_cpte([491]);

    $bj_brut = brut_cpte([185, 42, 43, 44, 45, 46, 47], [478]);
    $bj_dep = dep_cpte([492, 493, 494, 495, 496, 497]);
    
    $bg_brut = $bh_brut + $bi_brut + $bj_brut;
    $bg_dep = $bh_dep + $bi_dep + $bj_dep;

    $bq_brut = brut_cpte([50]);
    $bq_dep = dep_cpte([590]);

    $br_brut = brut_cpte([51]);
    $br_dep = dep_cpte([591]);

    $bs_brut = brut_cpte([52, 53, 54, 55, 57, 581, 582]);
    $bs_dep = dep_cpte([592, 593, 594]);

    $bu_brut = brut_cpte([478]);
    $bu_dep = dep_cpte([0]);

    ///////////////////////////////////////////////////////////////////////////


    $ad_brut = somme([$ae_brut, $af_brut, $ag_brut, $ah_brut]);
    $ad_dep = somme([$ae_dep, $af_dep, $ag_dep, $ah_dep]);

    $ai_brut = somme([$aj_brut, $ak_brut, $al_brut, $am_brut, $an_brut, $ap_brut]);
    $ai_dep = somme([$aj_dep, $ak_dep, $al_dep, $am_dep, $an_dep, $ap_dep]);

    $aq_brut = somme([$ar_brut, $as_brut]);
    $aq_dep = somme([$ar_dep, $as_dep]);

    $az_brut = somme([$ad_brut, $ai_brut, $aq_brut]);
    $az_dep = somme([$ad_dep, $ai_dep, $aq_dep]);

    $bk_brut = somme([$ba_brut, $bb_brut, $bg_brut]);
    $bk_dep = somme([$ba_dep, $bb_dep, $bg_dep]);

    $bt_brut = somme([$bq_brut, $br_brut, $bs_brut]);
    $bt_dep = somme([$bq_dep, $br_dep, $bs_dep]);

    $bz_brut = somme([$az_brut, $bk_brut, $bt_brut, $bu_brut]);
    $bz_dep = somme([$az_dep, $bk_dep, $bt_dep, $bu_dep]);


    //////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////// PASSIF /////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////

    $ca = dep_cpte([101, 102, 103, 104]);
    $cb = dep_cpte([109]);
    $cd = dep_cpte([105]);
    $ce = dep_cpte([106]);
    $cf = dep_cpte([111, 112, 113]);
    $cg = dep_cpte([118]);
    $ch = dep_cpte([12], [121, 129]);
    $cj = dep_cpte([13], [131, 139]);
    $cl = dep_cpte([14]);
    $cm = dep_cpte([15]);
    $da = dep_cpte([16, 181, 182, 183, 184]);
    $db = dep_cpte([17]);
    $dc = dep_cpte([19]);
    $dh = dep_cpte([481, 482, 484, 4998]);
    $di = dep_cpte([419]);
    $dj = dep_cpte([40], [409]);
    $dk = dep_cpte([42, 43, 44]);
    $dm = dep_cpte([185, 45, 46, 47], [479]);
    $dn = dep_cpte([499, 599], [4998]);
    $dq = dep_cpte([564, 565]);
    $dr = dep_cpte([52, 53, 561, 566]);
    $dv = dep_cpte([479]);

    ////////////////////////////////////////////////////////////////////////////////

    $cp = somme([$ca, $cb, $cd, $ce, $cf, $cg, $ch, $cj, $cl, $cm]);
    $dd = somme([$da, $db, $dc]);
    $df = somme([$cp, $dd]);
    $dp = somme([$dh, $di, $dj, $dk, $dm, $dn]);
    $dt = somme([$dq, $dr]);
    $dp = somme([$cp, $dd, $df, $dp, $dt]);






    switch ($ref) {

            //ACTIF

        case 'ae':
            echo affiche_tab_brut([211, 2181, 2191]);
            echo affiche_tab_dep([2811, 2818, 2911, 2918, 2919]);
            echo '<b>Valeur net de la référence = </b>';
            echo $ae = $ae_brut - $ae_dep;
            break;
        case 'af':
            echo affiche_tab_brut([212, 213, 214, 2193]);
            echo affiche_tab_dep([2812, 2813, 2814, 2912, 2913, 2914, 2919]);
            echo '<b>Valeur net de la référence = </b>';
            echo $af = $af_brut - $af_dep;
            break;
        case 'ag':
            echo affiche_tab_brut([215, 216]);
            echo affiche_tab_dep([2815, 2816, 2915, 2916]);
            echo '<b>Valeur net de la référence = </b>';
            echo $ag = $ag_brut - $ag_dep;
            break;
        case 'ah':
            echo affiche_tab_brut([217, 218, 2198], [2181]);
            echo affiche_tab_dep([2817, 2818, 2917, 2918, 2919]);
            echo '<b>Valeur net de la référence = </b>';
            echo $ah = $ah_brut - $ah_dep;
            break;
        case 'aj':
            echo affiche_tab_brut([22]);
            echo affiche_tab_dep([282, 292]);
            echo '<b>Valeur net de la référence = </b>';
            echo $aj = $aj_brut - $aj_dep;
            break;
        case 'ak':
            echo affiche_tab_brut([231, 232, 233, 237, 2391]);
            echo affiche_tab_dep([2831, 2832, 2833, 2837, 2931, 2932, 2933, 2937, 2939]);
            echo '<b>Valeur net de la référence = </b>';
            echo $ak = $ak_brut - $ak_dep;
            break;
        case 'al':
            echo affiche_tab_brut([234, 235, 238, 2392, 2393]);
            echo affiche_tab_dep([2834, 2835, 2838, 2934, 2935, 2938, 2939]);
            echo '<b>Valeur net de la référence = </b>';
            echo $al = $al_brut - $al_dep;
            break;
        case 'am':
            echo affiche_tab_brut([24], [245, 2495]);
            echo affiche_tab_dep([284, 294, 2949], [2845, 2945, 2949]);
            echo '<b>Valeur net de la référence = </b>';
            echo $am = $am_brut - $am_dep;
            break;
        case 'an':
            echo affiche_tab_brut([245, 2495]);
            echo affiche_tab_dep([2845, 2945, 2949]);
            echo '<b>Valeur net de la référence = </b>';
            echo $an = $an_brut - $an_dep;
            break;
        case 'ap':
            echo affiche_tab_brut([251, 252]);
            echo affiche_tab_dep([2951, 2952]);
            echo '<b>Valeur net de la référence = </b>';
            echo $ap = $ap_brut - $ap_dep;
            break;
        case 'ar':
            echo affiche_tab_brut([26]);
            echo affiche_tab_dep([296]);
            echo '<b>Valeur net de la référence = </b>';
            echo $ar = $ar_brut - $ar_dep;
            break;
        case 'as':
            echo affiche_tab_brut([27]);
            echo affiche_tab_dep([297]);
            echo '<b>Valeur net de la référence = </b>';
            echo $as = $as_brut - $as_dep;
            break;
        case 'ba':
            echo affiche_tab_brut([485, 488]);
            echo affiche_tab_dep([498]);
            echo '<b>Valeur net de la référence = </b>';
            echo $ba = $ba_brut - $ba_dep;
            break;
        case 'bb':
            echo affiche_tab_brut([31, 32, 33, 34, 35, 36, 37, 38]);
            echo affiche_tab_dep([39]);
            echo '<b>Valeur net de la référence = </b>';
            echo $bb = $bb_brut - $bb_dep;
            break;
        case 'bh':
            echo affiche_tab_brut([409]);
            echo affiche_tab_dep([490]);
            echo '<b>Valeur net de la référence = </b>';
            echo $bh = $bh_brut - $bh_dep;
            break;
        case 'bi':
            echo affiche_tab_brut([41], [419]);
            echo affiche_tab_dep([491]);
            echo '<b>Valeur net de la référence = </b>';
            echo $bi = $bi_brut - $bi_dep;
            break;
        case 'bj':
            echo affiche_tab_brut([185, 42, 43, 44, 45, 46, 47], [478]);
            echo affiche_tab_dep([492, 493, 494, 495, 496, 497]);
            echo '<b>Valeur net de la référence = </b>';
            echo $bj = $bj_brut - $bj_dep;
            break;
        case 'bq':
            echo affiche_tab_brut([50]);
            echo affiche_tab_dep([590]);
            echo '<b>Valeur net de la référence = </b>';
            echo $bq = $bq_brut - $bq_dep;
            break;
        case 'br':
            echo affiche_tab_brut([51]);
            echo affiche_tab_dep([591]);
            echo '<b>Valeur net de la référence = </b>';
            echo $br = $br_brut - $br_dep;
            break;
        case 'bs':
            echo affiche_tab_brut([52, 53, 54, 55, 57, 581, 582]);
            echo affiche_tab_dep([592, 593, 594]);
            echo '<b>Valeur net de la référence = </b>';
            echo $bs = $bs_brut - $bs_dep;
            break;
        case 'bu':
            echo affiche_tab_brut([478]);
            echo affiche_tab_dep([0]);
            echo '<b>Valeur net de la référence = </b>';
            echo $bu = $bu_brut - $bu_dep;
            break;




        case 'ad':
            echo '-----------------Brut------------------';
            echo affiche_tab_sref([
                'ae' => 'Frais de dévelloppement et de prospection',
                'af' => 'Brevet, licence, logiciels et droits similaire',
                'ag' => 'Fonds commercial et droit au bail',
                'ah' => 'Autres immobilisations incoporelles'
            ], [$ae_brut, $af_brut, $ag_brut, $ah_brut]);

            echo '-----------------Amort------------------';
            echo affiche_tab_sref([
                'ae' => 'Frais de dévelloppement et de prospection',
                'af' => 'Brevet, licence, logiciels et droits similaire',
                'ag' => 'Fonds commercial et droit au bail',
                'ah' => 'Autres immobilisations incoporelles'
            ], [$ae_dep, $af_dep, $ag_dep, $ah_dep]);
            echo '<b>Valeur net de la référence = </b>';
            echo $ad_brut - $ad_dep;
            break;
            
        

        case 'ai':
            echo '-----------------Brut------------------';
            echo affiche_tab_sref([
                'aj' => 'Terrains dont placement en net',
                'ak' => 'Bâtiments dont placement en net',
                'al' => 'Aménagement, agencement et installations',
                'am' => 'Matériel, mobilier et actifs biologiques',
                'an' => 'Matériels de transport',
                'ap' => 'Avances et acomptes versés sur immobilisation'
            ], [$aj_brut, $ak_brut, $al_brut, $am_brut, $an_brut, $ap_brut]);

            echo '-----------------Amort------------------';
            echo affiche_tab_sref([
                'aj' => 'Terrains dont placement en net',
                'ak' => 'Bâtiments dont placement en net',
                'al' => 'Aménagement, agencement et installations',
                'am' => 'Matériel, mobilier et actifs biologiques',
                'an' => 'Matériels de transport',
                'ap' => 'Avances et acomptes versés sur immobilisation'
            ], [$aj_dep, $ak_dep, $al_dep, $am_dep, $an_dep, $ap_dep]);
            echo '<b>Valeur net de la référence = </b>';
            echo $ai_brut - $ai_dep;
            break;

        case 'aq':
            echo '-----------------Brut------------------';
            echo affiche_tab_sref([
                'ar' => 'Titres de participation',
                'as' => 'Autres immobilisations incorporelles'
            ], [$ar_brut, $as_brut]);

            echo '-----------------Amort------------------';
            echo affiche_tab_sref([
                'ar' => 'Titres de participation',
                'as' => 'Autres immobilisations incorporelles'
            ], [$ar_dep, $as_dep]);
            echo '<b>Valeur net de la référence = </b>';
            echo $aq_brut - $aq_dep;
            break;

        case 'az':
            echo '-----------------Brut------------------';
            echo affiche_tab_sref([
                'ad' => 'Immobilisations incorporelles',
                'ai' => 'Immobilisations corporelles',
                'aq' => 'Immobilisations financières'
            ], [$ad_brut, $ai_brut, $aq_brut]);

            echo '-----------------Amort------------------';
            echo affiche_tab_sref([
                'ad' => 'Immobilisations incorporelles',
                'ai' => 'Immobilisations corporelles',
                'aq' => 'Immobilisations financières'
            ], [$ad_dep, $ai_dep, $aq_dep]);
            echo '<b>Valeur net de la référence = </b>';
            echo $az_brut - $az_dep;
            break;
            
        
        case 'bg':
            echo '-----------------Brut------------------';
            echo affiche_tab_sref([
                'bh' => 'Fournisseurs, avances versées',
                'bi' => 'Clients',
                'bj' => 'Autres créances',
            ], [$bh_brut, $bi_brut, $bj_brut]);

            echo '-----------------Amort------------------';
            echo affiche_tab_sref([
                'bh' => 'Fournisseurs, avances versées',
                'bi' => 'Clients',
                'bj' => 'Autres créances',
            ], [$bh_dep, $bi_dep, $bj_dep]);
            echo '<b>Valeur net de la référence = </b>';
            echo $bg_brut - $bg_dep;
            break;

        case 'bk':
            echo '-----------------Brut------------------';
            echo affiche_tab_sref([
                'ba' => 'Actifs circulant HAO',
                'bb' => 'Stocks et encours',
                'bg' => 'Créances et emplois assimilés',
            ], [$ba_brut, $bb_brut, $bg_brut]);

            echo '-----------------Amort------------------';
            echo affiche_tab_sref([
                'ba' => 'Actifs circulant HAO',
                'bb' => 'Stocks et encours',
                'bg' => 'Créances et emplois assimilés',
            ], [$ba_dep, $bb_dep, $bg_dep]);
            echo '<b>Valeur net de la référence = </b>';
            echo $bk_brut - $bk_dep;
            break;

        case 'bt':
            echo '-----------------Brut------------------';
            echo affiche_tab_sref([
                'bq' => 'Titres de placement',
                'br' => 'Valeurs à encaisser',
                'bs' => 'Banques, chèques postaux, caisse et assimilés'
            ], [$bq_brut, $br_brut, $bs_brut]);

            echo '-----------------Amort------------------';
            echo affiche_tab_sref([
                'bq' => 'Titres de placement',
                'br' => 'Valeurs à encaisser',
                'bs' => 'Banques, chèques postaux, caisse et assimilés'
            ], [$bq_dep, $br_dep, $bs_dep]);
            echo '<b>Valeur net de la référence = </b>';
            echo $bt_brut - $bt_dep;
            break;

        case 'bz':
            echo '-----------------Brut------------------';
            echo affiche_tab_sref([
                'az' => 'Total actif immobilisé',
                'bk' => 'Total actif circulant',
                'bt' => 'Total trésorerie actif',
                'bu' => 'Ecart de conversion-Actif'
            ], [$az_brut, $bk_brut, $bt_brut, $bu_brut]);

            echo '-----------------Amort------------------';
            echo affiche_tab_sref([
                'az' => 'Total actif immobilisé',
                'bk' => 'Total actif circulant',
                'bt' => 'Total trésorerie actif',
                'bu' => 'Ecart de conversion-Actif'
            ], [$az_dep, $bk_dep, $bt_dep, $bu_dep]);
            echo '<b>Valeur net de la référence = </b>';
            echo $bz_brut - $bz_dep;
            break;

            //PASSIF

        case 'ca':
            echo affiche_tab_dep([101, 102, 103, 104]);
            echo '<b>Valeur de la référence = </b>';
            echo $ca;
            break;
        case 'cb':
            echo affiche_tab_dep([109]);
            echo '<b>Valeur de la référence = </b>';
            echo $cb;
            break;
        case 'cd':
            echo affiche_tab_dep([105]);
            echo '<b>Valeur de la référence = </b>';
            echo $cd;
            break;
        case 'ce':
            echo affiche_tab_dep([106]);
            echo '<b>Valeur de la référence = </b>';
            echo $ce;
            break;
        case 'cf':
            echo affiche_tab_dep([111, 112, 113]);
            echo '<b>Valeur de la référence = </b>';
            echo $cf;
            break;
        case 'cg':
            echo affiche_tab_dep([118]);
            echo '<b>Valeur de la référence = </b>';
            echo $cg;
            break;
        case 'ch':
            echo affiche_tab_dep([12], [121, 129]);
            echo '<b>Valeur de la référence = </b>';
            echo $ch;
            break;
        case 'cj':
            echo affiche_tab_dep([13], [131, 139]);
            echo '<b>Valeur de la référence = </b>';
            echo $cj;
            break;
        case 'cl':
            echo affiche_tab_dep([14]);
            echo '<b>Valeur de la référence = </b>';
            echo $cl;
            break;
        case 'cm':
            echo affiche_tab_dep([15]);
            echo '<b>Valeur de la référence = </b>';
            echo $cm;
            break;
        case 'da':
            echo affiche_tab_dep([16, 181, 182, 183, 184]);
            echo '<b>Valeur de la référence = </b>';
            echo $da;
            break;
        case 'db':
            echo affiche_tab_dep([17]);
            echo '<b>Valeur de la référence = </b>';
            echo $db;
            break;
        case 'dc':
            echo affiche_tab_dep([19]);
            echo '<b>Valeur de la référence = </b>';
            echo $dc;
            break;
        case 'dh':
            echo affiche_tab_dep([481, 482, 484, 4998]);
            echo '<b>Valeur de la référence = </b>';
            echo $dh;
            break;
        case 'di':
            echo affiche_tab_dep([419]);
            echo '<b>Valeur de la référence = </b>';
            echo $di;
            break;
        case 'dj':
            echo affiche_tab_dep([40], [409]);
            echo '<b>Valeur de la référence = </b>';
            echo $dj;
            break;
        case 'dk':
            echo affiche_tab_dep([42, 43, 44]);
            echo '<b>Valeur de la référence = </b>';
            echo $dk;
            break;
        case 'dm':
            echo affiche_tab_dep([185, 45, 46, 47], [479]);
            echo '<b>Valeur de la référence = </b>';
            echo $dm;
            break;
        case 'dn':
            echo affiche_tab_dep([499, 599], [4998]);
            echo '<b>Valeur de la référence = </b>';
            echo $dn;
            break;
        case 'dq':
            echo affiche_tab_dep([564, 565]);
            echo '<b>Valeur de la référence = </b>';
            echo $dq;
            break;
        case 'dr':
            echo affiche_tab_dep([52, 53, 561, 566]);
            echo '<b>Valeur de la référence = </b>';
            echo $dr;
            break;
        case 'dv':
            echo affiche_tab_dep([479]);
            echo '<b>Valeur de la référence = </b>';
            echo $dv;
            break;



        case 'cp':
            echo affiche_tab_sref([
                'ca' => 'Capital',
                'cb' => 'Apporteurs capital non appelé',
                'cd' => 'Primes liés au capital social',
                'ce' => 'Ecarts de réévaluation',
                'cf' => 'Réserves indisponibles',
                'cg' => 'Réserves libres',
                'ch' => 'Report à nouveau',
                'cj' => 'Résultat net de l\'exercice',
                'cl' => 'Subvention d\'investissement',
                'cm' => 'Provision réglementées'
            ], [$ca, $cb, $cd, $ce, $cf, $cg, $ch, $cj, $cl, $cm]);
            echo '<b>Valeur de la référence = </b>';
            echo $cp;
            break;

        case 'dd':
            echo affiche_tab_sref([
                'da' => 'Emprunts et dettes financières diverses',
                'db' => 'Dettes de location acquisition',
                'dc' => 'Provisions pour risques et charges'
            ], [$da, $db, $dc]);
            echo '<b>Valeur de la référence = </b>';
            echo $dd;
            break;

        case 'df':
            echo affiche_tab_sref([
                'cp' => 'Total capitaux propres et ressources assimilées',
                'dd' => 'Total dettes financières et ressources assimilées'
            ], [$cp, $dd]);
            echo '<b>Valeur de la référence = </b>';
            echo $df;
            break;

        case 'dp':
            echo affiche_tab_sref([
                'dh' => 'Dettes circulants HAO',
                'di' => 'Clients, avances reçues',
                'dj' => 'Fournisseurs d\'exploitation',
                'dk' => 'Dettes fiscales et sociales',
                'dm' => 'Autres dettes',
                'dn' => 'Provisions pour riques à court terme'
            ], [$dh, $di, $dj, $dk, $dm, $dn]);
            echo '<b>Valeur de la référence = </b>';
            echo $dp;
            break;

        case 'dt':
            echo affiche_tab_sref([
                'dq' => 'Banques, crédits d\'escompte',
                'dr' => 'Banques, établissements financiers et crédits de trésorerie'
            ], [$dq, $dr]);
            echo '<b>Valeur de la référence = </b>';
            echo $dt;
            break;

        case 'dz':
            echo affiche_tab_sref([
                'cp' => 'Total capitaux propres et ressources assimilées',
                'dd' => 'Total dettes financières et ressources assililées',
                'df' => 'Total ressources stables',
                'dp' => 'Total passif circulant',
                'dt' => 'Total trésorerie passif'
            ], [$cp, $dd, $df, $dp, $dt]);
            echo '<b>Valeur de la référence = </b>';
            echo $dp;
            break;

        default:
            # code...
            break;
    }
    ?>

</section>

</body>

</html>