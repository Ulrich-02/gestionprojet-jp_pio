<?php
$title = 'Affiche compte ref';
?>


<?php
function tab_body($cpte, $ref, $exp_cpte = [])
{
    $db = new PDO('mysql:host=localhost;dbname=mist', 'root', ''/*,[PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]*/);

    $implode_head = '';
    $implode_body = '';
    $implode_foot = '';
    if (!empty($exp_cpte)) {
        $implode_head = "and cpte NOT LIKE '";
        $implode_body = implode("%' and cpte NOT LIKE '", $exp_cpte);
        $implode_foot = "%'";
    }

    $query = "SELECT cpte, inti, $ref FROM balance_n WHERE cpte LIKE  '$cpte%' $implode_head$implode_body$implode_foot";
    $statement = $db->prepare($query);
    $statement->execute();
    $result = $statement->fetchAll();

    $tab_body = '';
    foreach ($result as $ligne) {
        $tab_body .= "
        <tr>
            <td>{$ligne['cpte']}</td>
            <td>{$ligne['inti']}</td>
            <td>{$ligne["$ref"]}</td>
        </tr>
        ";
    }

    return $tab_body;
}
function tab_foot($cpte, $ref, $exp_cpte = [])
{
    $db = new PDO('mysql:host=localhost;dbname=mist', 'root', ''/*,[PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]*/);

    $implode_head = '';
    $implode_body = '';
    $implode_foot = '';
    if (!empty($exp_cpte)) {
        $implode_head = "and cpte NOT LIKE '";
        $implode_body = implode("%' and cpte NOT LIKE '", $exp_cpte);
        $implode_foot = "%'";
    }

    $query = "SELECT SUM($ref) FROM balance_n WHERE cpte LIKE  '$cpte%' $implode_head$implode_body$implode_foot";
    $statement = $db->prepare($query);
    $statement->execute();
    $result = $statement->fetch();

    return $result["SUM($ref)"];
}



function affiche_tab_brut($cpte, $exp_cpte = [])
{
    $tab_head = '-----------------Brut------------------
                <table class="table mt-2">
                    <thead class="table-light">
                        <tr>
                            <th scope="col">Compte</th>
                            <th scope="col">Intitulé</th>
                            <th scope="col">SFD</th>
                        </tr>
                    </thead>
                    <tbody id="display_table">';

    $tab_body = '';
    $val_tab_foot = 0;
    foreach ($cpte as $value) {
        $tab_body .= tab_body($value, 'sfd', $exp_cpte);
        $val_tab_foot += tab_foot($value, 'sfd', $exp_cpte);
    }

    $tab_foot = "       <tr style=\"background-color: #dadbdd;\">
                            <td colspan=\"2\" class=\"text-end\"><b>Total :</b></td>
                            <td>$val_tab_foot</td>
                        </tr>
                    </tbody>
                </table>";
    return $tab_head . $tab_body . $tab_foot;
}

function affiche_tab_dep($cpte, $exp_cpte = [])
{
    $tab_head = '-----------------Amort------------------
                <table class="table mt-2">
                    <thead class="table-light">
                        <tr>
                            <th scope="col">Compte</th>
                            <th scope="col">Intitulé</th>
                            <th scope="col">SFC</th>
                        </tr>
                    </thead>
                    <tbody id="display_table">';

    $tab_body = '';
    $val_tab_foot = 0;
    foreach ($cpte as $value) {
        $tab_body .= tab_body($value, 'sfc', $exp_cpte);
        $val_tab_foot += tab_foot($value, 'sfc', $exp_cpte);
    }

    $tab_foot = "       <tr style=\"background-color: #dadbdd;\">
                            <td colspan=\"2\" class=\"text-end\"><b>Total :</b></td>
                            <td>$val_tab_foot</td>
                        </tr>
                    </tbody>
                </table>";
    return $tab_head . $tab_body . $tab_foot;
}

function affiche_tab_sref($ref, $val_ref)
{
    $tab_head = '<table class="table mt-2">
                    <thead class="table-light">
                        <tr>
                            <th scope="col">REF</th>
                            <th scope="col">Intitulé</th>
                            <th scope="col">Valeur</th>
                        </tr>
                    </thead>
                    <tbody id="display_table">';
    $i = 0;
    $tab_body = '';
    foreach ($ref as $key => $value) {
        foreach ($val_ref as $key1 => $value1) {
            
            if ($i == $key1) {
                $tab_body .= "
                        <tr>
                            <td>$key</td>
                            <td>$value</td>
                            <td>$value1</td>
                        </tr>";
            }
        }
        $i++;
        $val_tab_foot = somme($val_ref);
    }
    $tab_foot = "       <tr style=\"background-color: #dadbdd;\">
                            <td colspan=\"2\" class=\"text-end\"><b>Total :</b></td>
                            <td>$val_tab_foot</td>
                        </tr>
                    </tbody>
                </table>";

    return $tab_head . $tab_body . $tab_foot;
}

?>