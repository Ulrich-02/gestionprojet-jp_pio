<?php
/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 23/12/2021
 * Time: 08:15
 */

include('../../vendor/autoload.php');
include('../../db.php');

if(!isset($_SESSION['id_user']))
{
    header("location:../../connexion.php");
    exit();
}


$query0 = "SELECT cpte FROM balance_n ";
$statement0 = $db->prepare($query0);
$statement0->execute();
$count0 = $statement0->rowCount();

$query2 = "SELECT cpte FROM balance_n_1 ";
$statement2 = $db->prepare($query2);
$statement2->execute();
$count2 = $statement2->rowCount();

$query3 = "SELECT xi FROM resultat ";
$statement3 = $db->prepare($query3);
$statement3->execute();
$result3 = $statement3->fetchAll();
$xi3 = 0;
foreach($result3 as $row3) {
    $xi3 = $row3["xi"];
}



// Si la balance n est vide
if($count0 == 0)
{
    header("location:../../balances/balance-n/balance-n.php");
    exit();
}


// Si la balance n-1 est vide
if($count2 == 0)
{
    header("location:../../balances/balance-n-1/balance-n-1.php");
    exit();
}


// Si le resultat est vide
/*
if($xi3 == 0)
{
    header("location:preparation.php");
    exit();
}
*/




$query = "SELECT * FROM resultat ";
$statement = $db->prepare($query);
$statement->execute();
$result = $statement->fetchAll();



$query1 = "SELECT * FROM infos ";
$statement1 = $db->prepare($query1);
$statement1->execute();
$result1 = $statement1->fetchAll();

$nom = '';
$numero = '';
$an_n = '';
$an_n_1 = '';
$duree = '';
foreach($result1 as $row1) {
    $nom = $row1["nom_infos"];
    $numero = $row1["numero_infos"];
    $an_n = $row1["an_n_infos"];
    $an_n_1 = $row1["an_n_1_infos"];
    $duree = $row1["duree_infos"];
}


$date = gmdate("d-m-Y");
$hour = gmdate("H:i");
$hour2 = gmdate("H-i");


// On met tiret quand une valeur est 0;
function jp($a) {
    $b = ($a == 0) ? "-" : number_format($a, 0, ',', ' ');
    return $b;
}

// reference the Dompdf namespace
use Dompdf\Dompdf;

$output = '
        <html>
        <head>
            <style>
                .gras {font-weight: bold;}
                .pl-5 {padding-left: 5px;}
                .pr-5 {padding-right: 5px;}
                .bg-col-blue {background-color: #95b3d7;}
                .bg-col-gris {background-color: #D3D3D3;}
                .txt-center {text-align: center;}
                .txt-right {text-align: right;}
                .bg-color-blue-fonce {background-color: #95b3d7;}
                .bg-color-green-fonce {background-color: #33ff00;}
                .col-white {color: white;}
            </style>
        </head>
        <body>
		<div class="table-responsive" style="font-size: 12px !important;">
		    <h1 style="font-size: 13px !important;">
		        <span>Désignation entité: '.$nom.'</span>
		        <span style="float: right;">Exercice clos le 31-12-'.$an_n.'</span>
		    </h1>
		    <h1 style="font-size: 13px !important;">
		        <span>Numéro d\'identification: '.$numero.'</span>
		        <span style="float: right;">Durée (en mois): '.$duree.'</span>
		    </h1>
		    <h1 style="margin-top: 30px; font-size: 17px !important; text-align: center; color: #0070c0;">COMPTE DE RESULTAT AU 31 DECEMBRE '.$an_n.'</h1>
		    <!--<h1>Bilan du '.$date.' à '.$hour.' (Heure GMT)</h1>-->
            <table border="1" style="border-collapse:collapse;" >
                <tr class="bg-col-blue">
                    
                    <th rowspan="2" colspan="2" class="pl-5 pr-5">REF</th>
                    <th rowspan="2" colspan="15">LIBELLES</th>
                    <th rowspan="2" class="pl-5 pr-5"></th>
                    <th rowspan="2" colspan="2" class="pl-5 pr-5">NOTE</th>
                    <th colspan="3" class="pl-5 pr-5">EXERCICE <br> AU 31/12/'.$an_n.'</th>
                    <th colspan="3" class="pl-5 pr-5">EXERCICE <br> AU 31/12/'.$an_n_1.'</th>
                    
                    
                    
                </tr>
';



$output .= '
    			<tr class="bg-col-blue">
    			
                    <th colspan="3">NET</th>
                    <th colspan="3">NET</th>
    			    
    			</tr>
';


foreach($result as $row) {
    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">TA</td>
    			    <td colspan="15" class="pl-5 pr-5 gras" style="width: 400px;">Ventes de marchandises <span style="float: right;">A</span></td>
    			    <td class="txt-center gras" style="width: 25px;">+</td>
    			    <td colspan="2" class="txt-center gras">21</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["ta"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["ta_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">RA</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Achat de marchandises</td>
    			    <td class="txt-center gras">-</td>
    			    <td colspan="2" class="txt-center gras">22</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["ra"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["ra_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">RB</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Variation  de stocks de marchandises</td>
    			    <td class="txt-center gras">-/+</td>
    			    <td colspan="2" class="txt-center gras">6</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rb"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rb_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">XA</td>
    			    <td colspan="15" class="pl-5 pr-5 gras bg-color-blue-fonce">MARGE COMMERCIALE (Somme TA à RB)</td>
    			    <td class="txt-center gras bg-color-blue-fonce"></td>
    			    <td colspan="2" class="txt-center gras bg-color-blue-fonce"></td>
    			    <td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">' . jp($row["xa"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">' . jp($row["xa_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">TB</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Vente de produits fabriqués <span style="float: right;">B</span></td>
    			    <td class="txt-center gras">+</td>
    			    <td colspan="2" class="txt-center gras">21</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["tb"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["tb_1"]) . '</td>
                    
    			    
    			</tr>
';

    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">TC</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Travaux, services vendus <span style="float: right;">C</span></td>
    			    <td class="txt-center gras">+</td>
    			    <td colspan="2" class="txt-center gras">21</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["tc"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["tc_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">TD</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Produits accessoires <span style="float: right;">D</span></td>
    			    <td class="txt-center gras">+</td>
    			    <td colspan="2" class="txt-center gras">21</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["td"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["td_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">XB</td>
    			    <td colspan="15" class="pl-5 pr-5 gras bg-color-blue-fonce">CHIFFRES D’AFFAIRES (A + B + C+ D)</td>
    			    <td class="txt-center gras bg-color-blue-fonce"></td>
    			    <td colspan="2" class="txt-center gras bg-color-blue-fonce"></td>
    			    <td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">' . jp($row["xb"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">' . jp($row["xb_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">TE</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Production stockées (ou déstockage)</td>
    			    <td class="txt-center gras">-/+</td>
    			    <td colspan="2" class="txt-center gras">6</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["te"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["te_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">TF</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Production immobilisée</td>
    			    <td class="txt-center gras"></td>
    			    <td colspan="2" class="txt-center gras">21</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["tf"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["tf_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">TG</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Subventions d\'exploitation</td>
    			    <td class="txt-center gras"></td>
    			    <td colspan="2" class="txt-center gras">21</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["tg"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["tg_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">TH</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Autres produits</td>
    			    <td class="txt-center gras">+</td>
    			    <td colspan="2" class="txt-center gras">21</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["th"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["th_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">TI</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Transferts de charges d’exploitation</td>
    			    <td class="txt-center gras">+</td>
    			    <td colspan="2" class="txt-center gras">12</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["ti"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["ti_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">RC</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Achats de matières premières et fournitures liées</td>
    			    <td class="txt-center gras">-</td>
    			    <td colspan="2" class="txt-center gras">22</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rc"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rc_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">RD</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Variations de stocks de matières premières et fournitures liées</td>
    			    <td class="txt-center gras">-/+</td>
    			    <td colspan="2" class="txt-center gras">6</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rd"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rd_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">RE</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Autres achats</td>
    			    <td class="txt-center gras">-</td>
    			    <td colspan="2" class="txt-center gras">22</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["re"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["re_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">RF</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Variations des autres approvisionnements</td>
    			    <td class="txt-center gras">-/+</td>
    			    <td colspan="2" class="txt-center gras">6</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rf"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rf_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">RG</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Transports</td>
    			    <td class="txt-center gras">-</td>
    			    <td colspan="2" class="txt-center gras">23</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rg"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rg_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">RH</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Services extérieurs</td>
    			    <td class="txt-center gras">-</td>
    			    <td colspan="2" class="txt-center gras">24</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rh"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rh_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">RI</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Impôts et taxes</td>
    			    <td class="txt-center gras">-</td>
    			    <td colspan="2" class="txt-center gras">25</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["ri"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["ri_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">RJ</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Autres charges</td>
    			    <td class="txt-center gras">-</td>
    			    <td colspan="2" class="txt-center gras">26</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rj"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rj_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">XC</td>
    			    <td colspan="15" class="pl-5 pr-5 gras bg-color-blue-fonce">VALEUR AJOUTEE (XB+RA+RB) + (somme TE à RJ)</td>
    			    <td class="txt-center gras bg-color-blue-fonce"></td>
    			    <td colspan="2" class="txt-center gras bg-color-blue-fonce"></td>
    			    <td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">' . jp($row["xc"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">' . jp($row["xc_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">RK</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Charges de personnel</td>
    			    <td class="txt-center gras">-</td>
    			    <td colspan="2" class="txt-center gras">27</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rk"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rk_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">XD</td>
    			    <td colspan="15" class="pl-5 pr-5 gras bg-color-blue-fonce">EXCEDENT BRUT D\'EXPLOITATION (XC + RK)</td>
    			    <td class="txt-center gras bg-color-blue-fonce"></td>
    			    <td colspan="2" class="txt-center gras bg-color-blue-fonce">28</td>
    			    <td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">' . jp($row["xd"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">' . jp($row["xd_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">TJ</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Reprise d\'amortissements, provisions et dépréciations</td>
    			    <td class="txt-center gras">+</td>
    			    <td colspan="2" class="txt-center gras">28</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["tj"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["tj_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">RL</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Dotations aux amortissements, aux provisions et dépréciations</td>
    			    <td class="txt-center gras">-</td>
    			    <td colspan="2" class="txt-center gras">3C&28</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rl"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rl_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">XE</td>
    			    <td colspan="15" class="pl-5 pr-5 gras bg-color-blue-fonce">RESULTAT D\'EXPLOITATION (XD + TJ + RL)</td>
    			    <td class="txt-center gras bg-color-blue-fonce"></td>
    			    <td colspan="2" class="txt-center gras bg-color-blue-fonce"></td>
    			    <td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">' . jp($row["xe"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">' . jp($row["xe_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">TK</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Revenus financiers et assimilés</td>
    			    <td class="txt-center gras">+</td>
    			    <td colspan="2" class="txt-center gras">29</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["tk"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["tk_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">TL</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Reprises de provisions et dépréciations financières</td>
    			    <td class="txt-center gras">+</td>
    			    <td colspan="2" class="txt-center gras">28</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["tl"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["tl_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">TM</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Transferts de charges financières</td>
    			    <td class="txt-center gras">+</td>
    			    <td colspan="2" class="txt-center gras">12</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["tm"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["tm_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">RM</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Frais financiers et charges assimilées</td>
    			    <td class="txt-center gras">-</td>
    			    <td colspan="2" class="txt-center gras">29</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rm"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rm_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">RN</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Dotations aux provisions et aux dépréciations financières</td>
    			    <td class="txt-center gras">-</td>
    			    <td colspan="2" class="txt-center gras">3C&28</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rn"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rn_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">XF</td>
    			    <td colspan="15" class="pl-5 pr-5 gras bg-color-blue-fonce">RESULTAT FINANCIER (Somme TK à RN)</td>
    			    <td class="txt-center gras bg-color-blue-fonce"></td>
    			    <td colspan="2" class="txt-center gras bg-color-blue-fonce"></td>
    			    <td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">' . jp($row["xf"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5 bg-color-blue-fonce">' . jp($row["xf_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">XG</td>
    			    <td colspan="15" class="pl-5 pr-5 gras bg-col-gris">RESULTAT DES ACTIVITES ORDINAIRES (XE + XF)</td>
    			    <td class="txt-center gras bg-col-gris"></td>
    			    <td colspan="2" class="txt-center gras bg-col-gris"></td>
    			    <td colspan="3" class="txt-right gras pr-5 bg-col-gris">' . jp($row["xg"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5 bg-col-gris">' . jp($row["xg_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">TN</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Produits des cessions d\'immobilisations</td>
    			    <td class="txt-center gras">+</td>
    			    <td colspan="2" class="txt-center gras">3D</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["tn"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["tn_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">TO</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Autres produits H.A.O</td>
    			    <td class="txt-center gras">+</td>
    			    <td colspan="2" class="txt-center gras">30</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["tlettreo"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["tlettreo_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			    <td colspan="2" class="gras txt-center">RO</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Valeurs comptables des cessions d\'immobilisations</td>
    			    <td class="txt-center gras">-</td>
    			    <td colspan="2" class="txt-center gras">3D</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rlettreo"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rlettreo_1"]) . '</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td colspan="2" class="gras txt-center">RP</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Autres charges H.A.O</td>
    			    <td class="txt-center gras">-</td>
    			    <td colspan="2" class="txt-center gras">30</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rlettrep"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rlettrep_1"]) . '</td>
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td colspan="2" class="gras txt-center">XH</td>
    			    <td colspan="15" class="pl-5 pr-5 gras bg-col-gris">RESULTAT HORS ACTIVITES ORDINAIRES (somme TN à RP)</td>
    			    <td class="txt-center gras bg-col-gris"></td>
    			    <td colspan="2" class="txt-center gras bg-col-gris"></td>
    			    <td colspan="3" class="txt-right gras pr-5 bg-col-gris">' . jp($row["xh"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5 bg-col-gris">' . jp($row["xh_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			    <td colspan="2" class="gras txt-center">RQ</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Participations des travailleurs</td>
    			    <td class="txt-center gras">-</td>
    			    <td colspan="2" class="txt-center gras">30</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rq"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rq_1"]) . '</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td colspan="2" class="gras txt-center">RS</td>
    			    <td colspan="15" class="pl-5 pr-5 gras">Impôts sur le résultat</td>
    			    <td class="txt-center gras">-</td>
    			    <td colspan="2" class="txt-center gras"></td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rs"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5">' . jp($row["rs_1"]) . '</td>
    			</tr>
';


    $output .= '
    			<tr>
    			    <td colspan="2" class="gras txt-center">XI</td>
    			    <td colspan="15" class="pl-5 pr-5 gras bg-color-green-fonce">RESULTAT NET (XG + XH +RQ + RS)</td>
    			    <td class="txt-center gras bg-color-green-fonce"></td>
    			    <td colspan="2" class="txt-center gras bg-color-green-fonce"></td>
    			    <td colspan="3" class="txt-right gras pr-5 bg-color-green-fonce">' . jp($row["xi"]) . '</td>
    			    <td colspan="3" class="txt-right gras pr-5 bg-color-green-fonce">' . jp($row["xi_1"]) . '</td>
    			</tr>
';
}


$output .= '
            </table>
        </div>
        </body>
        </html>
';





// instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->loadHtml($output);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'portrait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
//$dompdf->stream(); // Télécharge directement le document
$file_name = 'Resultat_'.$date.'_'.$hour2.'.pdf';
$dompdf->stream($file_name, array("Attachment" => false));