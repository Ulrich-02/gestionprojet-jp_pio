<?php
/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 15/12/2021
 * Time: 10:31
 */

include('../../db.php');

if(!isset($_SESSION['id_user']))
{
    header("location:../../connexion.php");
    exit();
}


$query0 = "SELECT cpte FROM balance_n ";
$statement0 = $db->prepare($query0);
$statement0->execute();
$count0 = $statement0->rowCount();

$query2 = "SELECT cpte FROM balance_n_1 ";
$statement2 = $db->prepare($query2);
$statement2->execute();
$count2 = $statement2->rowCount();

// Si la balance n est vide
if($count0 == 0)
{
    header("location:../../balances/balance-n/balance-n.php");
    exit();
}


// Si la balance n-1 est vide
if($count2 == 0)
{
    header("location:../../balances/balance-n-1/balance-n-1.php");
    exit();
}




$query = "SELECT * FROM infos ";
$statement = $db->prepare($query);
$statement->execute();
$count = $statement->rowCount();

if($count > 0)
{
    header("location:preparation-plein.php");
} else {
    header("location:preparation-vide.php");
}

?>