<?php

/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 23/12/2021
 * Time: 08:15
 */

// include('../../vendor/autoload.php');
// include('../../db.php');

// if(!isset($_SESSION['id_user']))
// {
//     header("location:../../connexion.php");
//     exit();
// }


// $query0 = "SELECT cpte FROM balance_n ";
// $statement0 = $db->prepare($query0);
// $statement0->execute();
// $count0 = $statement0->rowCount();

// $query2 = "SELECT cpte FROM balance_n_1 ";
// $statement2 = $db->prepare($query2);
// $statement2->execute();
// $count2 = $statement2->rowCount();

// $query3 = "SELECT xi FROM resultat ";
// $statement3 = $db->prepare($query3);
// $statement3->execute();
// $result3 = $statement3->fetchAll();
// $xi3 = 0;
// foreach($result3 as $row3) {
//     $xi3 = $row3["xi"];
// }



// // Si la balance n est vide
// if($count0 == 0)
// {
//     header("location:../../balances/balance-n/balance-n.php");
//     exit();
// }


// // Si la balance n-1 est vide
// if($count2 == 0)
// {
//     header("location:../../balances/balance-n-1/balance-n-1.php");
//     exit();
// }


// // Si le bilan est vide
// if($xi3 == 0)
// {
//     header("location:preparation.php");
//     exit();
// }




// $query = "SELECT * FROM resultat ";
// $statement = $db->prepare($query);
// $statement->execute();
// $result = $statement->fetchAll();



// $query1 = "SELECT * FROM infos ";
// $statement1 = $db->prepare($query1);
// $statement1->execute();
// $result1 = $statement1->fetchAll();

$nom = '';
$numero = '';
$an_n = 2020;
$an_n_1 = 2021;
$duree = '';
// foreach($result1 as $row1) {
//     $nom = $row1["nom_infos"];
//     $numero = $row1["numero_infos"];
//     $an_n = $row1["an_n_infos"];
//     $an_n_1 = $row1["an_n_1_infos"];
//     $duree = $row1["duree_infos"];
// }


$date = gmdate("d-m-Y");
$hour = gmdate("H:i");
$hour2 = gmdate("H-i");


// On met tiret quand une valeur est 0;
function jp($a)
{
    $b = ($a == 0) ? "-" : number_format($a, 0, ',', ' ');
    return $b;
}

// reference the Dompdf namespace
use Dompdf\Dompdf;

require_once "lib-php/dompdf/autoload.inc.php";

$output = '
        <html>
        <head>
            <style>
                .gras {font-weight: bold;}
                .pl-5 {padding-left: 5px;}
                .pr-5 {padding-right: 5px;}
                .bg-col-blue {background-color: #95b3d7;}
                .bg-col-gris {background-color: #D3D3D3;}
                .txt-center {text-align: center;}
                .txt-right {text-align: right;}
                .txt-white {color: white;}
                .txt-underline {text-decoration: underline;}
                .bg-color-blue {background-color: #95b3d7;}
                .bg-color-grey {background-color: #D3D3D3;}
                .bg-color-green {background-color: #33ff00;}
                .bg-color-white {background-color: #ffffff;}
                .col-white {color: white;}
                .border-white {border-color: white;}
                .border-black {border-color: black;}
                
                .b-top-grey {border-top: 2px grey solid;}
                .b-bottom-grey {border-bottom: 2px grey solid;}
                .b-right-grey {border-right: 2px grey solid;}
                .b-left-grey {border-left: 2px grey solid;}

                .b-top-black {border-top: 2px black solid;}
                .b-bottom-black {border-bottom: 2px black solid;}
                .b-right-black {border-right: 2px black solid;}
                .b-left-black {border-left: 2px black solid;}

                .b-top-white {border-top: 2px white solid;}
                .b-bottom-white {border-bottom: 2px white solid;}
                .b-right-white {border-right: 2px white solid;}
                .b-left-white {border-left: 2px white solid;}
            </style>
        </head>
        <body>
		<div style="font-size: 12px !important;">
		    <h1 style="font-size: 13px !important;">
		        <span>Désignation entité: ' . $nom . '</span>
		        <span style="float: right;">Exercice clos le 31-12-' . $an_n . '</span>
		    </h1>
		    <h1 style="font-size: 13px !important;">
		        <span>Numéro d\'identification: ' . $numero . '</span>
		        <span style="float: right;">Durée (en mois): ' . $duree . '</span>
		    </h1>
		    <h1 style="margin-top: 30px; font-size: 14px !important; text-align: center;">NOTE 27B <br> <span style="color: #0070c0;">EFFECTIFS, MASSE SALARIALE ET PERSONNEL EXTERIEUR</span></h1>
		    <!--<h1>Bilan du ' . $date . ' à ' . $hour . ' (Heure GMT)</h1>-->
            <table class="table-responsive" border="1" style="border-collapse:collapse; font-size: 10px !important;" >
                <tr class="bg-col-blue">
                    <th class="pl-5 pr-5 bg-color-white border-white b-right-grey"></th>
                    <th class="pl-5 pr-5" rowspan="2" style="min-width: 175px;">EFFECTIF ET MASSE SALARIALE</th>
                    <th class="pl-5 pr-5" colspan="7">EFFECTIFS</th>
                    <th class="pl-5 pr-5" colspan="7">MASSE SALARIALE</th>
                </tr>
                <tr class="bg-col-blue">
                    <th class="pl-5 pr-5 bg-color-white border-white b-right-grey"></th>
                    <th class="pl-5 pr-5" colspan="2" rowspan="2">Nationaux</th>
                    <th class="pl-5 pr-5" colspan="2" rowspan="2">Autres Etats de l\'OHADA</th>
                    <th class="pl-5 pr-5" colspan="2" rowspan="2">Hors OHADA</th>
                    <th class="pl-5 pr-5" rowspan="2">TOTAL</th>
                    <th class="pl-5 pr-5" colspan="2" rowspan="2">Nationaux</th>
                    <th class="pl-5 pr-5" colspan="2" rowspan="2">Autres Etats de l\'OHADA</th>
                    <th class="pl-5 pr-5" colspan="2" rowspan="2">Hors OHADA</th>
                    <th class="pl-5 pr-5" rowspan="2">TOTAL</th>
                </tr>
                <tr class="bg-col-blue">
                    <th class="pl-5 pr-5 bg-color-white border-white b-right-grey"></th>
                    <th class="pl-5 pr-5" rowspan="2">QUALIFICATIONS</th>
                </tr>
                <tr class="bg-col-blue">
                    <th class="pl-5 pr-5 bg-color-white border-white b-right-grey b-bottom-grey"></th>
                    <th class="pl-5 pr-5">M</th>
                    <th class="pl-5 pr-5">F</th>
                    <th class="pl-5 pr-5">M</th>
                    <th class="pl-5 pr-5">F</th>
                    <th class="pl-5 pr-5">M</th>
                    <th class="pl-5 pr-5">F</th>
                    <th class="pl-5 pr-5"></th>
                    <th class="pl-5 pr-5">M</th>
                    <th class="pl-5 pr-5">F</th>
                    <th class="pl-5 pr-5">M</th>
                    <th class="pl-5 pr-5">F</th>
                    <th class="pl-5 pr-5">M</th>
                    <th class="pl-5 pr-5">F</th>
                    <th class="pl-5 pr-5"></th>
                </tr>
';

$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras bg-color-grey">YA</td>
    			    <td class="pl-5 pr-5">1. Cadres supérieurs</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';

$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras bg-color-grey">YB</td>
    			    <td class="pl-5 pr-5">2. Techniciens supérieurs et cadres
moyens</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras bg-color-grey">YC</td>
    			    <td class="pl-5 pr-5">3. Techniciens, agents de maîtrise et
ouvriers qualifiés</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras bg-color-grey">YD</td>
    			    <td class="pl-5 pr-5">4. Employés, manœuvres, ouvriers, et
apprentis</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras bg-color-grey">YE</td>
    			    <td class="pl-5 pr-5">TOTAL (1)</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras bg-color-grey" style="padding: 5px;"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras bg-color-grey">YF</td>
    			    <td class="pl-5 pr-5">Permanents</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras bg-color-grey">YG</td>
    			    <td class="pl-5 pr-5">Saisonniers</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 border-white" style="padding: 8px;"></td>
    			    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white" style="border-right: 1px #cacdce solid;"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 border-white"></td>
    			    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white border-black b-top-grey b-left-grey" rowspan="2" colspan="2">Facturation à l\'entité</td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white" style="border-right: 1px #cacdce solid;"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 border-white b-bottom-grey"></td>
    			    <td class="pl-5 pr-5 border-white b-bottom-grey gras">2. Personnel extérieur</td>
                    <td class="pl-5 pr-5 border-white b-bottom-grey"></td>
                    <td class="pl-5 pr-5 border-white b-bottom-grey"></td>
                    <td class="pl-5 pr-5 border-white b-bottom-grey"></td>
                    <td class="pl-5 pr-5 border-white b-bottom-grey"></td>
                    <td class="pl-5 pr-5 border-white b-bottom-grey"></td>
                    <td class="pl-5 pr-5 border-white b-bottom-grey"></td>
                    <td class="pl-5 pr-5 border-white b-bottom-grey"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white" style="border-right: 1px #cacdce solid;"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras bg-color-grey">YH</td>
    			    <td class="pl-5 pr-5">1. Cadres supérieurs</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white" style="border-right: 1px #cacdce solid;"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras bg-color-grey">YI</td>
    			    <td class="pl-5 pr-5">2. Techniciens supérieurs et cadres
moyens</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white" style="border-right: 1px #cacdce solid;"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras bg-color-grey">YJ</td>
    			    <td class="pl-5 pr-5">3. Techniciens, agents de maîtrise et
ouvriers qualifiés</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white" style="border-right: 1px #cacdce solid;"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras bg-color-grey">YK</td>
    			    <td class="pl-5 pr-5">4. Employés, manœuvres, ouvriers, et
apprentis</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 txt-center border-white" style="border-right: 1px #cacdce solid;" colspan="5">M : Masculin</td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras bg-color-grey">YL</td>
    			    <td class="pl-5 pr-5">TOTAL (2)</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white" style="border-right: 1px #cacdce solid;"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras bg-color-grey">YM</td>
    			    <td class="pl-5 pr-5">Permanents</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white" style="border-right: 1px #cacdce solid;"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras bg-color-grey">YN</td>
    			    <td class="pl-5 pr-5">Saisonniers</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white"></td>
                    <td class="pl-5 pr-5 border-white" style="border-right: 1px #cacdce solid;"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras bg-color-grey">YO</td>
    			    <td class="pl-5 pr-5">TOTAL (1 + 2)</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 border-white" style="border-bottom: 1px #cacdce solid;"></td>
                    <td class="pl-5 pr-5 border-white" style="border-bottom: 1px #cacdce solid;"></td>
                    <td class="pl-5 pr-5 border-white" style="border-bottom: 1px #cacdce solid;"></td>
                    <td class="pl-5 pr-5 border-white" style="border-bottom: 1px #cacdce solid;"></td>
                    <td class="pl-5 pr-5 border-white" style="border-right: 1px #cacdce solid; border-bottom: 1px #cacdce solid;"></td>
    			</tr>
';


$output .= '
            </table>
        </div>
        </body>
        </html>
';



// instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->loadHtml($output);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'portrait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
//$dompdf->stream(); // Télécharge directement le document
$file_name = 'Resultat_' . $date . '_' . $hour2 . '.pdf';
$dompdf->stream($file_name, array("Attachment" => false));
