<?php

/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 23/12/2021
 * Time: 08:15
 */

// include('../../vendor/autoload.php');
// include('../../db.php');

// if(!isset($_SESSION['id_user']))
// {
//     header("location:../../connexion.php");
//     exit();
// }


// $query0 = "SELECT cpte FROM balance_n ";
// $statement0 = $db->prepare($query0);
// $statement0->execute();
// $count0 = $statement0->rowCount();

// $query2 = "SELECT cpte FROM balance_n_1 ";
// $statement2 = $db->prepare($query2);
// $statement2->execute();
// $count2 = $statement2->rowCount();

// $query3 = "SELECT xi FROM resultat ";
// $statement3 = $db->prepare($query3);
// $statement3->execute();
// $result3 = $statement3->fetchAll();
// $xi3 = 0;
// foreach($result3 as $row3) {
//     $xi3 = $row3["xi"];
// }



// // Si la balance n est vide
// if($count0 == 0)
// {
//     header("location:../../balances/balance-n/balance-n.php");
//     exit();
// }


// // Si la balance n-1 est vide
// if($count2 == 0)
// {
//     header("location:../../balances/balance-n-1/balance-n-1.php");
//     exit();
// }


// // Si le bilan est vide
// if($xi3 == 0)
// {
//     header("location:preparation.php");
//     exit();
// }




// $query = "SELECT * FROM resultat ";
// $statement = $db->prepare($query);
// $statement->execute();
// $result = $statement->fetchAll();



// $query1 = "SELECT * FROM infos ";
// $statement1 = $db->prepare($query1);
// $statement1->execute();
// $result1 = $statement1->fetchAll();

$nom = '';
$numero = '';
$an_n = 2020;
$an_n_1 = 2021;
$duree = '';
// foreach($result1 as $row1) {
//     $nom = $row1["nom_infos"];
//     $numero = $row1["numero_infos"];
//     $an_n = $row1["an_n_infos"];
//     $an_n_1 = $row1["an_n_1_infos"];
//     $duree = $row1["duree_infos"];
// }


$date = gmdate("d-m-Y");
$hour = gmdate("H:i");
$hour2 = gmdate("H-i");


// On met tiret quand une valeur est 0;
function jp($a)
{
    $b = ($a == 0) ? "-" : number_format($a, 0, ',', ' ');
    return $b;
}

// reference the Dompdf namespace
use Dompdf\Dompdf;

require_once "lib-php/dompdf/autoload.inc.php";

$output = '
        <html>
        <head>
            <style>
                .gras {font-weight: bold;}
                .pl-5 {padding-left: 5px;}
                .pr-5 {padding-right: 5px;}
                .bg-col-blue {background-color: #95b3d7;}
                .bg-col-gris {background-color: #D3D3D3;}
                .txt-center {text-align: center;}
                .txt-right {text-align: right;}
                .txt-white {color: white;}
                .txt-underline {text-decoration: underline;}
                .bg-color-blue {background-color: #95b3d7;}
                .bg-color-grey {background-color: #D3D3D3;}
                .bg-color-green {background-color: #33ff00;}
                .col-white {color: white;}
            </style>
        </head>
        <body>
		<div style="font-size: 12px !important;">
		    <h1 style="font-size: 13px !important;">
		        <span>Désignation entité: ' . $nom . '</span>
		        <span style="float: right;">Exercice clos le 31-12-' . $an_n . '</span>
		    </h1>
		    <h1 style="font-size: 13px !important;">
		        <span>Numéro d\'identification: ' . $numero . '</span>
		        <span style="float: right;">Durée (en mois): ' . $duree . '</span>
		    </h1>
		    <h1 style="margin-top: 30px; font-size: 14px !important; text-align: center;">NOTE 36 <br> <span style="color: #0070c0;">Table des codes</span></h1>
		    <!--<h1>Bilan du ' . $date . ' à ' . $hour . ' (Heure GMT)</h1>-->
            <table class="table-responsive" border="1" style="border-collapse:collapse; font-size: 13px !important;" >
                <tr class="">
                    
                    <th style="min-width: 350px;" colspan="3">1 - Code forme juridique (1)</th>
                    <th style="min-width: 350px;" colspan="3">3 - Code pays du siège social</th>
                    
                </tr>
';

$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5" style="min-width: 250px; padding: 5px;"></td>
                    <td class="pl-5 pr-5" style="min-width: 15px; padding: 5px;"></td>
                    <td class="pl-5 pr-5" style="min-width: 15px; padding: 5px;"></td>
                    <td class="pl-5 pr-5" style="min-width: 250px; padding: 5px;"></td>
                    <td class="pl-5 pr-5" style="min-width: 15px; padding: 5px;"></td>
                    <td class="pl-5 pr-5" style="min-width: 15px; padding: 5px;"></td>
    			</tr>
';

$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5">Société Anonyme (SA) à participation</td>
                    <td class="pl-5 pr-5">0</td>
                    <td class="pl-5 pr-5">0</td>
                    <td class="pl-5 pr-5">Pays OHADA (2)</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';

$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5">Société Anonyme (SA)</td>
                    <td class="pl-5 pr-5">0</td>
                    <td class="pl-5 pr-5">1</td>
                    <td class="pl-5 pr-5">France</td>
                    <td class="pl-5 pr-5">2</td>
                    <td class="pl-5 pr-5">1</td>
    			</tr>
';
$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5">Société à Responsabilité Limitée (SARL)</td>
                    <td class="pl-5 pr-5">0</td>
                    <td class="pl-5 pr-5">2</td>
                    <td class="pl-5 pr-5">Autres pays de l\'Union Européenne </td>
                    <td class="pl-5 pr-5">2</td>
                    <td class="pl-5 pr-5">3</td>
    			</tr>
';
$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5">Société en Commandite Simple (SCS)</td>
                    <td class="pl-5 pr-5">0</td>
                    <td class="pl-5 pr-5">3</td>
                    <td class="pl-5 pr-5">U.S.A.</td>
                    <td class="pl-5 pr-5">3</td>
                    <td class="pl-5 pr-5">9</td>
    			</tr>
';
$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5">Société en Nom Collectif (SNC)</td>
                    <td class="pl-5 pr-5">0</td>
                    <td class="pl-5 pr-5">4</td>
                    <td class="pl-5 pr-5">Canada</td>
                    <td class="pl-5 pr-5">4</td>
                    <td class="pl-5 pr-5">0</td>
    			</tr>
';
$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5">Société en Participation (SP)</td>
                    <td class="pl-5 pr-5">0</td>
                    <td class="pl-5 pr-5">5</td>
                    <td class="pl-5 pr-5">Autres pays américains</td>
                    <td class="pl-5 pr-5">4</td>
                    <td class="pl-5 pr-5">1</td>
    			</tr>
';
$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5">Groupement d\'Intérêt Economique (GIE)</td>
                    <td class="pl-5 pr-5">0</td>
                    <td class="pl-5 pr-5">6</td>
                    <td class="pl-5 pr-5">Pays asiatiques</td>
                    <td class="pl-5 pr-5">4</td>
                    <td class="pl-5 pr-5">9</td>
    			</tr>
';
$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5">Association</td>
                    <td class="pl-5 pr-5">0</td>
                    <td class="pl-5 pr-5">7</td>
                    <td class="pl-5 pr-5">Autres pays</td>
                    <td class="pl-5 pr-5">5</td>
                    <td class="pl-5 pr-5">0</td>
    			</tr>
';
$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5">Société par Actions Simplifiée (SAS)</td>
                    <td class="pl-5 pr-5">0</td>
                    <td class="pl-5 pr-5">8</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5">9</td>
                    <td class="pl-5 pr-5">9</td>
    			</tr>
';
$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5">Autre forme juridique (à préciser)</td>
                    <td class="pl-5 pr-5">0</td>
                    <td class="pl-5 pr-5">9</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5" style="min-width: 250px; padding: 5px;"></td>
                    <td class="pl-5 pr-5" style="min-width: 15px; padding: 5px;"></td>
                    <td class="pl-5 pr-5" style="min-width: 15px; padding: 5px;"></td>
                    <td class="pl-5 pr-5" style="min-width: 250px; padding: 5px;"></td>
                    <td class="pl-5 pr-5" style="min-width: 15px; padding: 5px;"></td>
                    <td class="pl-5 pr-5" style="min-width: 15px; padding: 5px;"></td>
    			</tr>
';
$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5 gras txt-center" colspan="3">2 - Code régime fiscal</td>
                    <td class="pl-5 pr-5" style="min-width: 15px; padding: 5px;"></td>
                    <td class="pl-5 pr-5" style="min-width: 15px; padding: 5px;"></td>
                    <td class="pl-5 pr-5" style="min-width: 15px; padding: 5px;"></td>
    			</tr>
';
$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5">Réel normal 1</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5">1</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5">Réel simplifié</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5">2</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5">Synthétique</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5">3</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5">Forfait</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5">4</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5">Société</td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';



$output .= '
</table>
<div style="padding: 5px; font-size: 12px;">
(1) Remplacer le premier 0 par 1 si l\'entité bénéficie d\'un agrément prioritaire<br>
(2) Bénin = 01 ; Burkina = 02 ; Côte d\'ivoire = 03 ; Guinée Bissau = 04 ; Mali = 05 ; Niger = 06 ; Sénégal =
07 ; Togo = 08; Cameroun = 09; Congo = 10; Gabon = 11; République Centrafricaine = 12; Tchad = 13;
Comores = 14; Guinée = 15; Guinée Equatoriale = 16; Congo RDC = 17.
</div>
</div>
</body>
        </html>
';



// instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->loadHtml($output);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'portrait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
//$dompdf->stream(); // Télécharge directement le document
$file_name = 'Resultat_' . $date . '_' . $hour2 . '.pdf';
$dompdf->stream($file_name, array("Attachment" => false));
