<?php

/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 23/12/2021
 * Time: 08:15
 */

// include('../../vendor/autoload.php');
// include('../../db.php');

// if(!isset($_SESSION['id_user']))
// {
//     header("location:../../connexion.php");
//     exit();
// }


// $query0 = "SELECT cpte FROM balance_n ";
// $statement0 = $db->prepare($query0);
// $statement0->execute();
// $count0 = $statement0->rowCount();

// $query2 = "SELECT cpte FROM balance_n_1 ";
// $statement2 = $db->prepare($query2);
// $statement2->execute();
// $count2 = $statement2->rowCount();

// $query3 = "SELECT xi FROM resultat ";
// $statement3 = $db->prepare($query3);
// $statement3->execute();
// $result3 = $statement3->fetchAll();
// $xi3 = 0;
// foreach($result3 as $row3) {
//     $xi3 = $row3["xi"];
// }



// // Si la balance n est vide
// if($count0 == 0)
// {
//     header("location:../../balances/balance-n/balance-n.php");
//     exit();
// }


// // Si la balance n-1 est vide
// if($count2 == 0)
// {
//     header("location:../../balances/balance-n-1/balance-n-1.php");
//     exit();
// }


// // Si le bilan est vide
// if($xi3 == 0)
// {
//     header("location:preparation.php");
//     exit();
// }




// $query = "SELECT * FROM resultat ";
// $statement = $db->prepare($query);
// $statement->execute();
// $result = $statement->fetchAll();



// $query1 = "SELECT * FROM infos ";
// $statement1 = $db->prepare($query1);
// $statement1->execute();
// $result1 = $statement1->fetchAll();

$nom = '';
$numero = '';
$an_n = 2020;
$an_n_1 = 2021;
$duree = '';
// foreach($result1 as $row1) {
//     $nom = $row1["nom_infos"];
//     $numero = $row1["numero_infos"];
//     $an_n = $row1["an_n_infos"];
//     $an_n_1 = $row1["an_n_1_infos"];
//     $duree = $row1["duree_infos"];
// }


$date = gmdate("d-m-Y");
$hour = gmdate("H:i");
$hour2 = gmdate("H-i");


// On met tiret quand une valeur est 0;
function jp($a)
{
    $b = ($a == 0) ? "-" : number_format($a, 0, ',', ' ');
    return $b;
}

// reference the Dompdf namespace
use Dompdf\Dompdf;

require_once "lib-php/dompdf/autoload.inc.php";

$output = '
        <html>
        <head>
            <style>
                .gras {font-weight: bold;}
                .pl-5 {padding-left: 5px;}
                .pr-5 {padding-right: 5px;}
                .bg-col-blue {background-color: #95b3d7;}
                .bg-col-gris {background-color: #D3D3D3;}
                .txt-center {text-align: center;}
                .txt-right {text-align: right;}
                .txt-white {color: white;}
                .txt-underline {text-decoration: underline;}
                .bg-color-blue {background-color: #95b3d7;}
                .bg-color-grey {background-color: #D3D3D3;}
                .bg-color-green {background-color: #33ff00;}
                .col-white {color: white;}
            </style>
        </head>
        <body>
		<div style="font-size: 12px !important;">
		    <h1 style="font-size: 13px !important;">
		        <span>Désignation entité: ' . $nom . '</span>
		        <span style="float: right;">Exercice clos le 31-12-' . $an_n . '</span>
		    </h1>
		    <h1 style="font-size: 13px !important;">
		        <span>Numéro d\'identification: ' . $numero . '</span>
		        <span style="float: right;">Durée (en mois): ' . $duree . '</span>
		    </h1>
		    <h1 style="margin-top: 30px; font-size: 12px !important; text-align: center;">NOTE 34 <br> <span style="color: #0070c0;">FICHE DE SYNTHESE DES PRINCIPAUX INDICATEURS FINANCIERS</span></h1>
		    <!--<h1>Bilan du ' . $date . ' à ' . $hour . ' (Heure GMT)</h1>-->
            <table class="table-responsive" border="1" style="border-collapse:collapse; font-size: 10px !important;" >
                <tr class="bg-col-blue">
                    
                    <th style="min-width: 500px;">( EN MILLIERS DE FRANCS)</th>
                    <th class="pl-5 pr-5" style="min-width: 75px;">Année N</th>
                    <th class="pl-5 pr-5" style="min-width: 75px;">Année N-1</th>
                    <th class="pl-5 pr-5" style="min-width: 75px;">Variation en %</th>
                    
                </tr>
                <tr class="" style="background-color: #80a2d7;">
                    
                    <th class="pl-5 pr-5 txt-center" colspan="4">ANALYSE DE L\'ACTIVITE</th>
                    
                </tr>
';

$output .= '
    			<tr class="bg-color-grey">
    			
                    <td class="pl-5 pr-5 gras">SOLDES INTERMEDIAIRES DE GESTION</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">CHIFFRE D\'AFFAIRES</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">MARGE COMMERCIALE</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">VALEUR AJOUTEE</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">EXCEDENT BRUT D\'EXPLOITATION (EBE)</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">RESULTAT D\'EXPLOITATION</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">RESULTAT FINANCIER</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">RESULTAT DES ACTIVITES ORDINAIRES</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">RESULTAT HORS ACTIVITES ORDINAIRES</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">RESULTAT NET</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="bg-color-grey">
    			
                    <td class="pl-5 pr-5 gras">DETERMINATION DE LA CAPACITE D\'AUTOFINANCEMENT</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras">EBE</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5"> + Valeurs comptables des cessions courantes d’immobilisation (compte 654)</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5"> - Produits des cessions courantes d’immobilisation (compte 754)</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';
$output .= '
    			<tr class="bg-color-blue">
    			
                    <td class="pl-5 pr-5 gras"> = CAPACITE D\'AUTOFINANCEMENT D\'EXPLOITATION</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5"> + Revenus financiers</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5"> + Gains de change</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5"> + Transferts de charges financières</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5"> + Produits HAO</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5"> + Transferts de charges HAO</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5"> - Frais financiers</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5"> - Pertes de change</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5"> - Participation</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5"> - Impôts sur les résultats</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';
$output .= '
    			<tr class="bg-color-blue">
    			
                    <td class="pl-5 pr-5 gras"> = CAPACITE D\'AUTOFINANCEMENT GLOBALE (C.A.F.G.)</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras"> - Distributions de dividendes opérées durant l\'exercice</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="bg-color-blue">
    			
                    <td class="pl-5 pr-5 gras"> = AUTOFINANCEMENT</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="" style="background-color: #80a2d7;">
    			
                    <td class="pl-5 pr-5 gras txt-center" colspan="4">ANALYSE DE LA RENTABILITE</td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras">Rentabilité économique = Résultat d’exploitation (a) /Capitaux propres + dettes financières</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras">Rentabilité financière = Résultat net/Capitaux propres</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="" style="background-color: #80a2d7;">
    			
                    <td class="pl-5 pr-5 gras txt-center" colspan="4">ANALYSE DE LA STRUCTURE FINANCIERE</td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Capitaux propres et ressources assimilées</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5"> + Dettes financières* et autres ressources assimilées (b)</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras"> = Ressources stables</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras"> - Actif immobilisé (b)</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="bg-color-blue">
    			
                    <td class="pl-5 pr-5 gras"> = FONDS DE ROULEMENT (1)</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5"> Actif circulant d\'exploitation (b)</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5"> - Passif circulant d\'exploitation (b)</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras"> = BESOIN DE FINANCEMENT D\'EXPLOITATION (2)</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5"> Actif circulant HAO (b)</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5"> - Passif circulant HAO (b)</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras"> = BESOIN DE FINANCEMENT HAO (3)</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="bg-color-blue">
    			
                    <td class="pl-5 pr-5 gras">BESOIN DE FINANCEMENT GLOBAL (4) = (2) + (3)</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5" style="padding: 3px;"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="bg-color-blue">
    			
                    <td class="pl-5 pr-5 gras">TRESORERIE NETTE (5) = (1) - (4)</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">CONTRÔLE : TRESORERIE NETTE = (TRESORERIE - ACTIF) - (TRESORERIE - PASSIF)</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="" style="background-color: #80a2d7;">
    			
                    <td class="pl-5 pr-5 gras txt-center" colspan="4">ANALYSE DE LA VARIATION DE LA TRESORERIE</td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras">Flux de trésorerie des activités opérationnelle</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras"> -Flux de trésorerie des activités d\'investissement</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras"> +Flux de trésorerie des activités de financement</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="bg-color-blue">
    			
                    <td class="pl-5 pr-5 gras"> = VARIATION DE LA TRESORERIE NETTE DE LA PERIODE</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="" style="background-color: #80a2d7;">
    			
                    <td class="pl-5 pr-5 gras txt-center" colspan="4">ANALYSE DE LA VARIATION DE L’ENDETTEMENT FINANCIER NET</td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras">Endettement financier brut (Dettes financières* + Trésorerie – passif)</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5 gras">-Trésorerie - actif</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="bg-color-blue">
    			
                    <td class="pl-5 pr-5 gras">= ENDETTEMENT FINANCIER NET</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';

$output .= '
</table>

(a) Résultat d’exploitation après impôt théorique sur le bénéfice. <br>
(b) Les écarts de conversion doivent être éliminés afin de ramener les créances et les dettes concernées à leur valeur initiale. <br>
 <b>Dettes financières</b>* = emprunts et dettes financières diverses + dettes de location acquisition.
</div>
</body>
        </html>
';



// instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->loadHtml($output);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'portrait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
//$dompdf->stream(); // Télécharge directement le document
$file_name = 'Resultat_' . $date . '_' . $hour2 . '.pdf';
$dompdf->stream($file_name, array("Attachment" => false));
