<?php

/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 23/12/2021
 * Time: 08:15
 */

// include('../../vendor/autoload.php');
// include('../../db.php');

// if(!isset($_SESSION['id_user']))
// {
//     header("location:../../connexion.php");
//     exit();
// }


// $query0 = "SELECT cpte FROM balance_n ";
// $statement0 = $db->prepare($query0);
// $statement0->execute();
// $count0 = $statement0->rowCount();

// $query2 = "SELECT cpte FROM balance_n_1 ";
// $statement2 = $db->prepare($query2);
// $statement2->execute();
// $count2 = $statement2->rowCount();

// $query3 = "SELECT xi FROM resultat ";
// $statement3 = $db->prepare($query3);
// $statement3->execute();
// $result3 = $statement3->fetchAll();
// $xi3 = 0;
// foreach($result3 as $row3) {
//     $xi3 = $row3["xi"];
// }



// // Si la balance n est vide
// if($count0 == 0)
// {
//     header("location:../../balances/balance-n/balance-n.php");
//     exit();
// }


// // Si la balance n-1 est vide
// if($count2 == 0)
// {
//     header("location:../../balances/balance-n-1/balance-n-1.php");
//     exit();
// }


// // Si le bilan est vide
// if($xi3 == 0)
// {
//     header("location:preparation.php");
//     exit();
// }




// $query = "SELECT * FROM resultat ";
// $statement = $db->prepare($query);
// $statement->execute();
// $result = $statement->fetchAll();



// $query1 = "SELECT * FROM infos ";
// $statement1 = $db->prepare($query1);
// $statement1->execute();
// $result1 = $statement1->fetchAll();

$nom = '';
$numero = '';
$an_n = 2020;
$an_n_1 = 2021;
$duree = '';
// foreach($result1 as $row1) {
//     $nom = $row1["nom_infos"];
//     $numero = $row1["numero_infos"];
//     $an_n = $row1["an_n_infos"];
//     $an_n_1 = $row1["an_n_1_infos"];
//     $duree = $row1["duree_infos"];
// }


$date = gmdate("d-m-Y");
$hour = gmdate("H:i");
$hour2 = gmdate("H-i");


// On met tiret quand une valeur est 0;
function jp($a)
{
    $b = ($a == 0) ? "-" : number_format($a, 0, ',', ' ');
    return $b;
}

// reference the Dompdf namespace
use Dompdf\Dompdf;

require_once "lib-php/dompdf/autoload.inc.php";

$output = '
        <html>
        <head>
            <style>
                .gras {font-weight: bold;}
                .pl-5 {padding-left: 5px;}
                .pr-5 {padding-right: 5px;}
                .bg-col-blue {background-color: #95b3d7;}
                .bg-col-gris {background-color: #D3D3D3;}
                .txt-center {text-align: center;}
                .txt-right {text-align: right;}
                .txt-white {color: white;}
                .txt-underline {text-decoration: underline;}
                .bg-color-blue {background-color: #95b3d7;}
                .bg-color-grey {background-color: #D3D3D3;}
                .bg-color-green {background-color: #33ff00;}
                .col-white {color: white;}
            </style>
        </head>
        <body>
		<div style="font-size: 12px !important;">
		    <h1 style="font-size: 13px !important;">
		        <span>Désignation entité: ' . $nom . '</span>
		        <span style="float: right;">Exercice clos le 31-12-' . $an_n . '</span>
		    </h1>
		    <h1 style="font-size: 13px !important;">
		        <span>Numéro d\'identification: ' . $numero . '</span>
		        <span style="float: right;">Durée (en mois): ' . $duree . '</span>
		    </h1>
		    <h1 style="margin-top: 30px; font-size: 13px !important; text-align: center;">NOTE 31 <br> <span style="color: #0070c0;">REPARTITION DU RESULTAT ET AUTRES ELEMENTS CARACTERISTIQUES DES CINQ DERNIERS EXERCICES</span></h1>
		    <!--<h1>Bilan du ' . $date . ' à ' . $hour . ' (Heure GMT)</h1>-->
            <table class="table-responsive" border="1" style="border-collapse:collapse; font-size: 12px !important;" >
                <tr class="bg-col-blue">
                    
                    <th style="min-width: 350px;">EXERCICES CONCERNES (1)</th>
                    <th class="pl-5 pr-5" rowspan="2" style="min-width: 75px;">N</th>
                    <th class="pl-5 pr-5" rowspan="2" style="min-width: 75px;">N-1</th>
                    <th class="pl-5 pr-5" rowspan="2" style="min-width: 75px;">N-2</th>
                    <th class="pl-5 pr-5" rowspan="2" style="min-width: 75px;">N-3</th>
                    <th class="pl-5 pr-5" rowspan="2" style="min-width: 75px;">N-4</th>
                    
                </tr>
                <tr class="bg-col-blue">
                    
                    <th style="min-width: 350px;">NATURE DES INDICATIONS</th>
                    
                </tr>
';

$output .= '
    			<tr class="bg-color-grey">
    			
                    <td class="pl-5 pr-5 gras">STRUCTURE DU CAPITAL A LA CLOTURE DE L\'EXERCICE (
2
)</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Capital social</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Actions ordinaires</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Actions à dividendes prioritaires (A.D.P) sans droit de vote</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Actions nouvelles à émettre :</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5"> - par conversion d\'obligations</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5"> - par exercice de droits de souscription</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="bg-color-grey">
    			
                    <td class="pl-5 pr-5 gras">OPERATIONS ET RESULTATS DE L\'EXERCICE (3)</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Chiffre d\'affaires hors taxes</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Résultat des activités ordinaires (R.A.O) hors dotations et reprises
(exploitation et financières)</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Participation des travailleurs aux bénéfices</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Impôt sur le résultat</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Résultat net (4
)</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="bg-color-grey">
    			
                    <td class="pl-5 pr-5 gras">RESULTAT ET DIVIDENDE DISTRIBUES</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Résultat distribué (5
)</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Dividende attribué à chaque action</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="bg-color-grey">
    			
                    <td class="pl-5 pr-5 gras">PERSONNEL ET POLITIQUE SALARIALE</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Effectif moyen des travailleurs au cours de l\'exercice (
6
)</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Effectif moyen de personnel extérieur</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Masse salariale distribuée au cours de l\'exercice (7
)</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Avantages sociaux versés au cours de l\'exercice (8
)
[Sécurité sociale, œuvres sociales]</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Personnel extérieur facturé à l\'entité (9)</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
</table>
';

$output .= '<table class="table-responsive" border="1" style="border-collapse:collapse; font-size: 9.5px !important; margin-top:; 15px" >
                <tr class="">
                    
                    <td class="pl-5 pr-5" style="min-width: 363px;">(1) Y compris l\'exercice dont les états financiers sont soumis à l\'approbation de l\'Assemblée</td>
                    <td class="pl-5 pr-5" style="min-width: 363px;">(6) Personnel propre</td>
                    
                </tr>
';
$output .= '

                <tr class="">
                                    
                    <td class="pl-5 pr-5" style="min-width: 363px;">(2) Indication, en cas de libération partielle du capital, du montant du capital non appelé</td>
                    <td class="pl-5 pr-5" style="min-width: 363px;">(7) Total des comptes 661, 662, 663</td>

                </tr>

';
$output .= '

                <tr class="">
                                    
                    <td class="pl-5 pr-5" style="min-width: 363px;">(3) Les éléments de cette rubrique sont ceux figurant au compte de résultat</td>
                    <td class="pl-5 pr-5" style="min-width: 363px;">(8) Total des comptes 664, 668</td>

                </tr>

';
$output .= '

                <tr class="">
                                    
                    <td class="pl-5 pr-5" style="min-width: 363px;">(4) Le résultat, lorsqu\'il est négatif, doit être mis entre parenthèses</td>
                    <td class="pl-5 pr-5" style="min-width: 363px;">(9)  Compte 667</td>

                </tr>

';
$output .= '

                <tr class="">
                                    
                    <td class="pl-5 pr-5" style="min-width: 363px;">(5) L\'exercice N correspond au dividende proposé du dernier exercice</td>
                    <td class="pl-5 pr-5" style="min-width: 363px;"></td>

                </tr>

';


$output .= '
</table>
</div>
</body>
        </html>
';



// instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->loadHtml($output);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'portrait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
//$dompdf->stream(); // Télécharge directement le document
$file_name = 'Resultat_' . $date . '_' . $hour2 . '.pdf';
$dompdf->stream($file_name, array("Attachment" => false));
