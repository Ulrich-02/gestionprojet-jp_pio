<?php

/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 23/12/2021
 * Time: 08:15
 */

include('../../vendor/autoload.php');
include('../../db.php');

// if(!isset($_SESSION['id_user']))
// {
//     header("location:../../connexion.php");
//     exit();
// }


// $query0 = "SELECT cpte FROM balance_n ";
// $statement0 = $db->prepare($query0);
// $statement0->execute();
// $count0 = $statement0->rowCount();

// $query2 = "SELECT cpte FROM balance_n_1 ";
// $statement2 = $db->prepare($query2);
// $statement2->execute();
// $count2 = $statement2->rowCount();

// $query3 = "SELECT xi FROM resultat ";
// $statement3 = $db->prepare($query3);
// $statement3->execute();
// $result3 = $statement3->fetchAll();
// $xi3 = 0;
// foreach($result3 as $row3) {
//     $xi3 = $row3["xi"];
// }



// // Si la balance n est vide
// if($count0 == 0)
// {
//     header("location:../../balances/balance-n/balance-n.php");
//     exit();
// }


// // Si la balance n-1 est vide
// if($count2 == 0)
// {
//     header("location:../../balances/balance-n-1/balance-n-1.php");
//     exit();
// }


// // Si le bilan est vide
// if($xi3 == 0)
// {
//     header("location:preparation.php");
//     exit();
// }




// $query = "SELECT * FROM resultat ";
// $statement = $db->prepare($query);
// $statement->execute();
// $result = $statement->fetchAll();



// $query1 = "SELECT * FROM infos ";
// $statement1 = $db->prepare($query1);
// $statement1->execute();
// $result1 = $statement1->fetchAll();

$nom = '';
$numero = '';
$an_n = 2020;
$an_n_1 = 2021;
$duree = '';
// foreach($result1 as $row1) {
//     $nom = $row1["nom_infos"];
//     $numero = $row1["numero_infos"];
//     $an_n = $row1["an_n_infos"];
//     $an_n_1 = $row1["an_n_1_infos"];
//     $duree = $row1["duree_infos"];
// }


$date = gmdate("d-m-Y");
$hour = gmdate("H:i");
$hour2 = gmdate("H-i");


// On met tiret quand une valeur est 0;
function jp($a)
{
    $b = ($a == 0) ? "-" : number_format($a, 0, ',', ' ');
    return $b;
}

// reference the Dompdf namespace
use Dompdf\Dompdf;

//require_once "lib-php/dompdf/autoload.inc.php";

$output = '
        <html>
        <head>
            <style>
                .gras {font-weight: bold;}
                .pl-5 {padding-left: 5px;}
                .pr-5 {padding-right: 5px;}
                .bg-col-blue {background-color: #95b3d7;}
                .bg-col-gris {background-color: #D3D3D3;}
                .txt-center {text-align: center;}
                .txt-right {text-align: right;}
                .txt-white {color: white;}
                .txt-underline {text-decoration: underline;}
                .bg-color-blue {background-color: #95b3d7;}
                .bg-color-grey {background-color: #D3D3D3;}
                .bg-color-green {background-color: #33ff00;}
                .col-white {color: white;}
            </style>
        </head>
        <body>
		<div style="font-size: 12px !important;">
		    <h1 style="font-size: 13px !important;">
		        <span>Désignation entité: ' . $nom . '</span>
		        <span style="float: right;">Exercice clos le 31-12-' . $an_n . '</span>
		    </h1>
		    <h1 style="font-size: 13px !important;">
		        <span>Numéro d\'identification: ' . $numero . '</span>
		        <span style="float: right;">Durée (en mois): ' . $duree . '</span>
		    </h1>
		    <h1 style="margin-top: 30px; font-size: 17px !important; text-align: center; color: #0070c0;">NOTE 1 : DETTES GARANTIES PAR DES SURETES REELLES ' . $an_n . '</h1>
		    <!--<h1>Bilan du ' . $date . ' à ' . $hour . ' (Heure GMT)</h1>-->
            <table class="table-responsive" border="1" style="border-collapse:collapse;" >
                <tr class="bg-col-blue">
                    
                    <th rowspan="3">LIBELLES</th>
                    <th rowspan="3" class="pl-5 pr-5">NOTE</th>
                    <th rowspan="3" class="pl-5 pr-5">Montant brut</th>
                    <th colspan="3" class="pl-5 pr-5">SURETES REELLES</th>
                    
                </tr>
';

$output .= '
    			<tr class="bg-col-blue">
    			
                    <th rowspan="2" class="pl-5 pr-5">Hypothèques</th>
                    <th rowspan="2" class="pl-5 pr-5">Nantissements</th>
                    <th class="pl-5 pr-5">Gages/</th>
    			    
    			</tr>
';

$output .= '
    			<tr class="bg-col-blue">

                    <th class="pl-5 pr-5">autres</th>
    			    
    			</tr>
';


$output .= '
<tr class="bg-color-grey">
    			
    <td class="pl-5 pr-5 txt-underline gras" style="width: 350px;">Dettes financières et ressources assimilées :</td>
    <td class="txt-center" style="width: 25px;"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
                    
    			    
</tr>
';


$output .= '
<tr class="">

    <td class="pl-5 pr-5">Emprunts obligataires convertibles </td>
    <td class="txt-center" style="width: 25px;"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="">

    <td class="pl-5 pr-5">Autres emprunts obligataires</td>
    <td class="txt-center" style="width: 25px;"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';



$output .= '
<tr class="">

    <td class="pl-5 pr-5">Emprunts et dettes des établissements de crédit</td>
    <td class="txt-center" style="width: 25px;"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';




$output .= '
<tr class="">

    <td class="pl-5 pr-5">Autres dettes financières</td>
    <td class="txt-center" style="width: 25px;"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="bg-color-blue">

    <td class="pl-5 pr-5 txt-center">SOUS TOTAL (1)</td>
    <td class="txt-center" style="width: 25px;"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="">

    <td class="pl-5 pr-5" style="padding: 8px;"></td>
    <td class="txt-center" style="width: 25px; padding: 8px;"></td>
    <td class="txt-right pl-5 pr-5" style="padding: 8px;"></td>
    <td class="txt-right pl-5 pr-5" style="padding: 8px;"></td>
    <td class="txt-right pl-5 pr-5" style="padding: 8px;"></td>
    <td class="txt-right pl-5 pr-5" style="padding: 8px;"></td>
    
    
</tr>
';


$output .= '
<tr class="bg-color-grey">

    <td class="pl-5 pr-5 txt-underline gras">Dettes de location-acquisition :</td>
    <td class="txt-center" style="width: 25px;"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="">

    <td class="pl-5 pr-5">Dettes de crédit-bail immobilier</td>
    <td class="txt-center" style="width: 25px;"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="">

    <td class="pl-5 pr-5">Dettes de crédit-bail mobilier</td>
    <td class="txt-center" style="width: 25px;"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="">

    <td class="pl-5 pr-5">Dettes sur contrats de location-vente</td>
    <td class="txt-center" style="width: 25px;"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="">

    <td class="pl-5 pr-5">Dettes sur contrats de location-acquisition</td>
    <td class="txt-center" style="width: 25px;"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="bg-color-blue">

    <td class="pl-5 pr-5 txt-center">SOUS TOTAL (2)</td>
    <td class="txt-center" style="width: 25px;"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="">

    <td class="pl-5 pr-5" style="padding: 8px;"></td>
    <td class="txt-center" style="width: 25px; padding: 8px;"></td>
    <td class="txt-right pl-5 pr-5" style="padding: 8px;"></td>
    <td class="txt-right pl-5 pr-5" style="padding: 8px;"></td>
    <td class="txt-right pl-5 pr-5" style="padding: 8px;"></td>
    <td class="txt-right pl-5 pr-5" style="padding: 8px;"></td>
    
    
</tr>
';


$output .= '
<tr class="bg-color-grey">

    <td class="pl-5 pr-5 txt-underline gras">Dettes du passif circulant :</td>
    <td class="txt-center" style="width: 25px;"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="">

    <td class="pl-5 pr-5">Fournisseurs et comptes rattachés</td>
    <td class="txt-center" style="width: 25px;"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="">

    <td class="pl-5 pr-5">Clients</td>
    <td class="txt-center" style="width: 25px;"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="">

    <td class="pl-5 pr-5">Personnel</td>
    <td class="txt-center" style="width: 25px;"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="">

    <td class="pl-5 pr-5">Sécurité sociale et organismes sociaux</td>
    <td class="txt-center" style="width: 25px;"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="">

    <td class="pl-5 pr-5">Etat</td>
    <td class="txt-center" style="width: 25px;"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="">

    <td class="pl-5 pr-5">Organismes internationaux</td>
    <td class="txt-center" style="width: 25px;"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="">

    <td class="pl-5 pr-5">Associés et groupe</td>
    <td class="txt-center" style="width: 25px;"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="">

    <td class="pl-5 pr-5">Créditeurs divers</td>
    <td class="txt-center" style="width: 25px;"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="bg-color-blue">

    <td class="pl-5 pr-5 txt-center">SOUS TOTAL (3)</td>
    <td class="txt-center" style="width: 25px;"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="bg-color-green">

    <td class="pl-5 pr-5">TOTAL (1) + (2) + (3)</td>
    <td class="txt-center" style="width: 25px;"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="bg-color-blue">

    <td colspan="3" class="pl-5 pr-5">ENGAGEMENTS FINANCIERS</td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-center pl-5 pr-5">Engagements<br>donnés</td>
    <td class="txt-center pl-5 pr-5">Engagements<br>reçus</td>
    
    
</tr>
';


$output .= '
<tr class="">

    <td colspan="3" class="pl-5 pr-5">Engagements consentis à des entités liées</td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="">

    <td colspan="3" class="pl-5 pr-5">Primes de remboursement non échues</td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="">

    <td colspan="3" class="pl-5 pr-5">Avals, cautions, garanties</td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="">

    <td colspan="3" class="pl-5 pr-5">hypothèques, nantissements, gages, autres</td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="">

    <td colspan="3" class="pl-5 pr-5">Effets escomptés non échus</td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="">

    <td colspan="3" class="pl-5 pr-5">Créances commerciales et professionnelles cédées</td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="">

    <td colspan="3" class="pl-5 pr-5">Abandons de créances conditionnels</td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';


$output .= '
<tr class="bg-color-green">

    <td colspan="3" class="pl-5 pr-5">TOTAL</td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    <td class="txt-right pl-5 pr-5"></td>
    
    
</tr>
';

$output .= '
            </table>
        </div>
        </body>
        </html>
';





// instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->loadHtml($output);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'portrait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
//$dompdf->stream(); // Télécharge directement le document
$file_name = 'Note1_' . $date . '_' . $hour2 . '.pdf';
$dompdf->stream($file_name, array("Attachment" => false));
