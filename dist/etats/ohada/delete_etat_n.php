<?php
/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 18/12/2021
 * Time: 16:05
 */

include('../../db.php');
include('../../fonctions.php');
include('../../fonctions-sql.php');


$message = '';

$query = "DELETE FROM infos";
$statement = $db->prepare($query);
$statement->execute();


// On réinitialise le compte de résultat
update('resultat', [
    'id_resultat' => 1,
    'ta' => 0,
    'ta_1' => 0,

    'ra' => 0,
    'ra_1' => 0,

    'rb' => 0,
    'rb_1' => 0,

    'xa' => 0,
    'xa_1' => 0,

    'tb' => 0,
    'tb_1' => 0,

    'tc' => 0,
    'tc_1' => 0,

    'td' => 0,
    'td_1' => 0,

    'xb' => 0,
    'xb_1' => 0,

    'te' => 0,
    'te_1' => 0,

    'tf' => 0,
    'tf_1' => 0,

    'tg' => 0,
    'tg_1' => 0,

    'th' => 0,
    'th_1' => 0,

    'ti' => 0,
    'ti_1' => 0,

    'rc' => 0,
    'rc_1' => 0,

    'rd' => 0,
    'rd_1' => 0,

    're' => 0,
    're_1' => 0,

    'rf' => 0,
    'rf_1' => 0,

    'rg' => 0,
    'rg_1' => 0,

    'rh' => 0,
    'rh_1' => 0,

    'ri' => 0,
    'ri_1' => 0,

    'rj' => 0,
    'rj_1' => 0,

    'xc' => 0,
    'xc_1' => 0,

    'rk' => 0,
    'rk_1' => 0,

    'xd' => 0,
    'xd_1' => 0,

    'tj' => 0,
    'tj_1' => 0,

    'rl' => 0,
    'rl_1' => 0,

    'xe' => 0,
    'xe_1' => 0,

    'tk' => 0,
    'tk_1' => 0,

    'tl' => 0,
    'tl_1' => 0,

    'tm' => 0,
    'tm_1' => 0,

    'rm' => 0,
    'rm_1' => 0,

    'rn' => 0,
    'rn_1' => 0,

    'xf' => 0,
    'xf_1' => 0,

    'xg' => 0,
    'xg_1' => 0,

    'tn' => 0,
    'tn_1' => 0,

    'tlettreo' => 0,
    'tlettreo_1' => 0,

    'rlettreo' => 0,
    'rlettreo_1' => 0,

    'rlettrep' => 0,
    'rlettrep_1' => 0,

    'xh' => 0,
    'xh_1' => 0,

    'rq' => 0,
    'rq_1' => 0,

    'rs' => 0,
    'rs_1' => 0,

    'xi' => 0,
    'xi_1' => 0
], 'id_resultat = 1', $db);






// On réinitilise le bilan
update('bilan', [
    'id_bilan' => 1,
    'ad_brut' => 0,
    'ad_dep' => 0,
    'ad_net' => 0,
    'ad_net_1' => 0,

    'ae_brut' => 0,
    'ae_dep' => 0,
    'ae_net' => 0,
    'ae_net_1' => 0,

    'af_brut' => 0,
    'af_dep' => 0,
    'af_net' => 0,
    'af_net_1' => 0,

    'ag_brut' => 0,
    'ag_dep' => 0,
    'ag_net' => 0,
    'ag_net_1' => 0,

    'ah_brut' => 0,
    'ah_dep' => 0,
    'ah_net' => 0,
    'ah_net_1' => 0,

    'ai_brut' => 0,
    'ai_dep' => 0,
    'ai_net' => 0,
    'ai_net_1' => 0,

    'aj_brut' => 0,
    'aj_dep' => 0,
    'aj_net' => 0,
    'aj_net_1' => 0,

    'ak_brut' => 0,
    'ak_dep' => 0,
    'ak_net' => 0,
    'ak_net_1' => 0,

    'al_brut' => 0,
    'al_dep' => 0,
    'al_net' => 0,
    'al_net_1' => 0,

    'am_brut' => 0,
    'am_dep' => 0,
    'am_net' => 0,
    'am_net_1' => 0,

    'an_brut' => 0,
    'an_dep' => 0,
    'an_net' => 0,
    'an_net_1' => 0,

    'ap_brut' => 0,
    'ap_dep' => 0,
    'ap_net' => 0,
    'ap_net_1' => 0,

    'aq_brut' => 0,
    'aq_dep' => 0,
    'aq_net' => 0,
    'aq_net_1' => 0,

    'ar_brut' => 0,
    'ar_dep' => 0,
    'ar_net' => 0,
    'ar_net_1' => 0,

    'as_brut' => 0,
    'as_dep' => 0,
    'as_net' => 0,
    'as_net_1' => 0,

    'az_brut' => 0,
    'az_dep' => 0,
    'az_net' => 0,
    'az_net_1' => 0,

    'ba_brut' => 0,
    'ba_dep' => 0,
    'ba_net' => 0,
    'ba_net_1' => 0,

    'bb_brut' => 0,
    'bb_dep' => 0,
    'bb_net' => 0,
    'bb_net_1' => 0,

    'bg_brut' => 0,
    'bg_dep' => 0,
    'bg_net' => 0,
    'bg_net_1' => 0,

    'bh_brut' => 0,
    'bh_dep' => 0,
    'bh_net' => 0,
    'bh_net_1' => 0,

    'bi_brut' => 0,
    'bi_dep' => 0,
    'bi_net' => 0,
    'bi_net_1' => 0,

    'bj_brut' => 0,
    'bj_dep' => 0,
    'bj_net' => 0,
    'bj_net_1' => 0,

    'bk_brut' => 0,
    'bk_dep' => 0,
    'bk_net' => 0,
    'bk_net_1' => 0,

    'bq_brut' => 0,
    'bq_dep' => 0,
    'bq_net' => 0,
    'bq_net_1' => 0,

    'br_brut' => 0,
    'br_dep' => 0,
    'br_net' => 0,
    'br_net_1' => 0,

    'bs_brut' => 0,
    'bs_dep' => 0,
    'bs_net' => 0,
    'bs_net_1' => 0,

    'bt_brut' => 0,
    'bt_dep' => 0,
    'bt_net' => 0,
    'bt_net_1' => 0,

    'bu_brut' => 0,
    'bu_dep' => 0,
    'bu_net' => 0,
    'bu_net_1' => 0,

    'bz_brut' => 0,
    'bz_dep' => 0,
    'bz_net' => 0,
    'bz_net_1' => 0,

    'ca' => 0,
    'ca_1' => 0,

    'cb' => 0,
    'cb_1' => 0,

    'cd' => 0,
    'cd_1' => 0,

    'ce' => 0,
    'ce_1' => 0,

    'cf' => 0,
    'cf_1' => 0,

    'cg' => 0,
    'cg_1' => 0,

    'ch' => 0,
    'ch_1' => 0,

    'cj' => 0,
    'cj_1' => 0,

    'cl' => 0,
    'cl_1' => 0,

    'cm' => 0,
    'cm_1' => 0,

    'cp' => 0,
    'cp_1' => 0,

    'da' => 0,
    'da_1' => 0,

    'db' => 0,
    'db_1' => 0,

    'dc' => 0,
    'dc_1' => 0,

    'dd' => 0,
    'dd_1' => 0,

    'df' => 0,
    'df_1' => 0,

    'dh' => 0,
    'dh_1' => 0,

    'di' => 0,
    'di_1' => 0,

    'dj' => 0,
    'dj_1' => 0,

    'dk' => 0,
    'dk_1' => 0,

    'dm' => 0,
    'dm_1' => 0,

    'dn' => 0,
    'dn_1' => 0,

    'dp' => 0,
    'dp_1' => 0,

    'dq' => 0,
    'dq_1' => 0,

    'dr' => 0,
    'dr_1' => 0,

    'dt' => 0,
    'dt_1' => 0,

    'dv' => 0,
    'dv_1' => 0,

    'dz' => 0,
    'dz_1' => 0

],'id_bilan = 1', $db);






$message = 'donnees supprimees';

echo json_encode($message);