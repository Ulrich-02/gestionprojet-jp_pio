
<?php require_once(__DIR__ . '/../db.php'); ?>
<?php require_once(__DIR__ . '/../fonctions.php'); ?>
<?php require_once(__DIR__ . '/../fonctions-sql.php'); ?>



<?php

    function val_cpte_debit($cpte, $exp_cpte = [])
    {
        //global $an, $db;
        $db = new PDO('mysql:host=localhost;dbname=mistral', 'root', ''/*,[PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]*/);
        $implode_head = '';
        $implode_body = '';
        $implode_foot = '';
        if (!empty($exp_cpte)) {
            $implode_head = "and cpte NOT LIKE '";
            $implode_body = implode("%' and cpte NOT LIKE '", $exp_cpte);
            $implode_foot = "%'";
        }

        $query = "SELECT SUM(sfd) FROM balance_n WHERE cpte LIKE '$cpte%' $implode_head$implode_body$implode_foot";
        $statement = $db->prepare($query);
        $statement->execute();
        $result = $statement->fetch();

        $compte_val = $result['SUM(sfd)'];

        return $compte_val;
    }




    function val_cpte_credit($cpte, $exp_cpte = [])
    {

        //global $an, $db;
        $db = new PDO('mysql:host=localhost;dbname=mistral', 'root', ''/*,[PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]*/);
        $implode_head = '';
        $implode_body = '';
        $implode_foot = '';
        if (!empty($exp_cpte)) {
            $implode_head = "and cpte NOT LIKE '";
            $implode_body = implode("%' and cpte NOT LIKE '", $exp_cpte);
            $implode_foot = "%'";
        }

        $query = "SELECT SUM(sfc) FROM balance_n WHERE cpte LIKE '$cpte%' $implode_head$implode_body$implode_foot";
        $statement = $db->prepare($query);
        $statement->execute();
        $result = $statement->fetch();

        $compte_val = $result['SUM(sfc)'];

        return $compte_val;
    }



    function brut_cpte($cpte, $exp_cpte = [])
    {
        $val_cpte = 0;
        foreach ($cpte as $value) {
            $val_cpte += val_cpte_debit($value, $exp_cpte);
        }
        return $val_cpte;
    }


    function dep_cpte($cpte, $exp_cpte = [])
    {
        $val_cpte = 0;
        foreach ($cpte as $value) {
            $val_cpte += val_cpte_credit($value, $exp_cpte);
        }
        return $val_cpte;
    }


?>
<?php

//  ACTIF    

//Immo incorporelles AD

$ae_brut = brut_cpte([211, 2181, 2191]);
$af_brut = brut_cpte([212, 213, 214, 2193]);
$ag_brut = brut_cpte([215, 216]);
$ah_brut = brut_cpte([217, 218, 2198], [2181]);

$ae_dep = dep_cpte([2811, 2818, 2911, 2918, 2919]);
$af_dep = dep_cpte([2812, 2813, 2814, 2912, 2913, 2914, 2919]);
$ag_dep = dep_cpte([2815, 2816, 2915, 2916]);
$ah_dep = dep_cpte([2817, 2818, 2917, 2918, 2919]);

$ae_net = $ae_brut - $ae_dep;
$af_net = $af_brut - $af_dep;
$ag_net = $ag_brut - $ag_dep;
$ah_net = $ah_brut - $ah_dep;

//ad = ae + af + ag + ah
$ad_brut = $ae_brut + $af_brut + $ag_brut + $ah_brut;
$ad_dep = $ae_dep + $af_dep + $ag_dep + $ah_dep;

//Valeur nette de AD
$ad_net = $ad_brut - $ad_dep;



//Immo corporelles AI

$aj_brut = brut_cpte([22]);
$ak_brut = brut_cpte([231, 232, 233, 237, 2391]);
$al_brut = brut_cpte([234, 235, 238, 2392, 2393]);
$am_brut = brut_cpte([24], [245, 2495]);
$an_brut = brut_cpte([245, 2495]);
$ap_brut = brut_cpte([251, 252]);

$aj_dep = dep_cpte([282, 292]);
$ak_dep = dep_cpte([2831, 2832, 2833, 2837, 2931, 2932, 2933, 2937, 2939]);
$al_dep = dep_cpte([2834, 2835, 2838, 2934, 2935, 2938, 2939]);
$am_dep = dep_cpte([284, 294, 2949], [2845, 2945, 2949]);
$an_dep = dep_cpte([2845, 2945, 2949]);
$ap_dep = dep_cpte([2951, 2952]);

$aj_net = $aj_brut - $aj_dep;
$ak_net = $ak_brut - $ak_dep;
$al_net = $al_brut - $al_dep;
$am_net = $am_brut - $am_dep;
$an_net = $an_brut - $an_dep;
$ap_net = $ap_brut - $ap_dep;



//ai = aj + ak + al + an + am + ap
$ai_brut = $aj_brut + $ak_brut + $al_brut + $am_brut + $an_brut + $ap_brut;
$ai_dep = $aj_dep + $ak_dep + $al_dep + $am_dep + $an_dep + $ap_dep;

//Valeur nette de AI
$ai_net = $ai_brut - $ai_dep;


//Immo financière AQ

$ar_brut = brut_cpte([26]);
$as_brut = brut_cpte([27]);

$ar_dep = dep_cpte([296]);
$as_dep = dep_cpte([297]);

$ar_net = $ar_brut - $ar_dep;
$as_net = $as_brut - $as_dep;

//aq = ar + as
$aq_brut = $ar_brut + $as_brut;
$aq_dep = $ar_dep + $as_dep;

//Valeur nette de AQ
$aq_net = $aq_brut - $aq_dep;



//Total actif immo AZ

//az = ad + ai + aq
$az_brut = $ad_brut + $ai_brut + $aq_brut;
$az_dep = $ad_dep + $ai_dep + $aq_dep;

//Valeur nette de AZ
$az_net = $az_brut - $az_dep;



//Total actif cirlulant BK

$ba_brut = brut_cpte([485, 488]);
$bb_brut = brut_cpte([31, 32, 33, 34, 35, 36, 37, 38]);
$bh_brut = brut_cpte([409]);
$bi_brut = brut_cpte([41], [419]);
$bj_brut = brut_cpte([185, 42, 43, 44, 45, 46, 47], [478]);

$bg_brut = $bh_brut + $bi_brut + $bj_brut;

$ba_dep = dep_cpte([498]);
$bb_dep = dep_cpte([39]);
$bh_dep = dep_cpte([490]);
$bi_dep = dep_cpte([491]);
$bj_dep = dep_cpte([492, 493, 494, 495, 496, 497]);

$bg_dep = $bh_dep + $bi_dep + $bj_dep;

$ba_net = $ba_brut - $ba_dep;
$bb_net = $bb_brut - $bb_dep;
$bh_net = $bh_brut - $bh_dep;
$bi_net = $bi_brut - $bi_dep;
$bj_net = $bj_brut - $bj_dep;

$bg_net = $bg_brut - $bg_dep;

//bk = ba + bb + bg, avec bg = bh + bi + bj
$bk_brut = $ba_brut + $bb_brut + $bg_brut;
$bk_dep = $ba_dep + $bb_dep + $bg_dep;

//Valeur nette de BK
$bk_net = $bk_brut - $bk_dep;


//Total trésorerie actif BT

$bq_brut = brut_cpte([50]);
$br_brut = brut_cpte([51]);
$bs_brut = brut_cpte([52, 53, 54, 55, 57, 581, 582]);

$bq_dep = dep_cpte([590]);
$br_dep = dep_cpte([591]);
$bs_dep = dep_cpte([592, 593, 594]);

$bq_net = $bq_brut - $bq_dep;
$br_net = $br_brut - $br_dep;
$bs_net = $bs_brut - $bs_dep;

//bt = bq + br + bs
$bt_brut = $bq_brut + $br_brut + $bs_brut;
$bt_dep = $bq_dep + $br_dep + $bs_dep;

//Valeur nette de BT
$bt_net = $bt_brut - $bt_dep;


// Ecart de conversion
$bu_brut = brut_cpte([478]);
$bu_dep = 0;

//Valeur nette de BU
$bu_net = $bu_brut - $bu_dep;


//Total général
$bz_brut = $ad_brut + $ai_brut + $aq_brut + $az_brut + $bk_brut + $bt_brut + $bu_brut;
$bz_dep = $ad_dep + $ai_dep + $aq_dep + $az_dep + $bk_dep + $bt_dep + $bu_dep;


//Valeur nette de BZ
$bz_net = $bz_brut - $bz_dep;



//  PASSIF

//Total capitaux propres et ressources assimilées CP

$ca = dep_cpte([101, 102, 103, 104]);

// $cb, brut_cpte car sa valeur reste dans SFD, et on le rend négatif (voir tableau de correspondance)
$cb = 0 - (brut_cpte([109]));

$cd = dep_cpte([105]);
$ce = dep_cpte([106]);
$cf = dep_cpte([111, 112, 113]);
$cg = dep_cpte([118]);


// Le report à nouveau peut être débiteur ou créditeur
$ch_debiteur = brut_cpte([12]);
$ch_crediteur = dep_cpte([12]);
$ch = 0;
if ($ch_debiteur > 0 && $ch_crediteur == 0) {
    $ch = 0 - $ch_debiteur; //on rend négatif la valeur de ch
}
if ($ch_crediteur > 0 && $ch_debiteur == 0) {
    $ch = $ch_crediteur;
}
if ($ch_crediteur == 0 && $ch_debiteur == 0) {
    $ch = 0;
}


// Le résultat net peut être débiteur ou créditeur
$cj_debiteur = brut_cpte([13]);
$cj_crediteur = dep_cpte([13]);
$cj = 0;
if ($cj_debiteur > 0 && $cj_crediteur == 0) {
    $cj = 0 - $cj_debiteur; //on rend négatif la valeur de cj
}
if ($cj_crediteur > 0 && $cj_debiteur == 0) {
    $cj = $cj_crediteur;
}
if ($cj_crediteur == 0 && $cj_debiteur == 0) {
    $cj = 0;
}



$cl = dep_cpte([14]);
$cm = dep_cpte([15]);

$cp = $ca + $cb + $cd + $ce + $cf + $cg + $ch + $cj + $cl + $cm;



//Total dette financières et ressources assimilées DD

$da = dep_cpte([16, 181, 182, 183, 184]);
$db = dep_cpte([17]);
$dc = dep_cpte([19]);

$dd = $da + $db + $dc;


// Total Ressources stables DF
$df = $cp + $dd;



//Total passif circulant
$dh = dep_cpte([481, 482, 484, 4998]);
$di = dep_cpte([419]);
$dj = dep_cpte([40], [409]);
$dk = dep_cpte([42, 43, 44]);
$dm = dep_cpte([185, 45, 46, 47], [479]);
$dn = dep_cpte([499, 599], [4998]);

$dp = $dh + $di + $dj + $dk + $dm + $dn;



//Total trésorerie passif
$dq = dep_cpte([564, 565]);
$dr = dep_cpte([52, 53, 561, 566]);

$dt = $dq + $dr;



//Ecart de conversion-Passif
$dv = dep_cpte([479]);


//Total général
$dz = $cp + $df + $dp + $dt + $dv;


//Insertion dans la base
update('bilan', [
    'id_bilan' => 1,
    'ad_brut' => $ad_brut,
    'ad_dep' => $ad_dep,
    'ad_net' => $ad_net,
    'ae_brut' => $ae_brut,
    'ae_dep' => $ae_dep,
    'ae_net' => $ae_net,
    'af_brut' => $af_brut,
    'af_dep' => $af_dep,
    'af_net' => $af_net,
    'ag_brut' => $ag_brut,
    'ag_dep' => $ag_dep,
    'ag_net' => $ag_net,
    'ah_brut' => $ah_brut,
    'ah_dep' => $ah_dep,
    'ah_net' => $ah_net,
    'ai_brut' => $ai_brut,
    'ai_dep' => $ai_dep,
    'ai_net' => $ai_net,
    'aj_brut' => $aj_brut,
    'aj_dep' => $aj_dep,
    'aj_net' => $aj_net,
    'ak_brut' => $ak_brut,
    'ak_dep' => $ak_dep,
    'ak_net' => $ak_net,
    'al_brut' => $al_brut,
    'al_dep' => $al_dep,
    'al_net' => $al_net,
    'am_brut' => $am_brut,
    'am_dep' => $am_dep,
    'am_net' => $am_net,
    'an_brut' => $an_brut,
    'an_dep' => $an_dep,
    'an_net' => $an_net,
    'ap_brut' => $ap_brut,
    'ap_dep' => $ap_dep,
    'ap_net' => $ap_net,
    'aq_brut' => $aq_brut,
    'aq_dep' => $aq_dep,
    'aq_net' => $aq_net,
    'ar_brut' => $ar_brut,
    'ar_dep' => $ar_dep,
    'ar_net' => $ar_net,
    'as_brut' => $as_brut,
    'as_dep' => $as_dep,
    'as_net' => $as_net,
    'az_brut' => $az_brut,
    'az_dep' => $az_dep,
    'az_net' => $az_net,
    'ba_brut' => $ba_brut,
    'ba_dep' => $ba_dep,
    'ba_net' => $ba_net,
    'bb_brut' => $bb_brut,
    'bb_dep' => $bb_dep,
    'bb_net' => $bb_net,
    'bg_brut' => $bg_brut,
    'bg_dep' => $bg_dep,
    'bg_net' => $bg_net,
    'bh_brut' => $bh_brut,
    'bh_dep' => $bh_dep,
    'bh_net' => $bh_net,
    'bi_brut' => $bi_brut,
    'bi_dep' => $bi_dep,
    'bi_net' => $bi_net,
    'bj_brut' => $bj_brut,
    'bj_dep' => $bj_dep,
    'bj_net' => $bj_net,
    'bk_brut' => $bk_brut,
    'bk_dep' => $bk_dep,
    'bk_net' => $bk_net,
    'bq_brut' => $bq_brut,
    'bq_dep' => $bq_dep,
    'bq_net' => $bq_net,
    'br_brut' => $br_brut,
    'br_dep' => $br_dep,
    'br_net' => $br_net,
    'bs_brut' => $bs_brut,
    'bs_dep' => $bs_dep,
    'bs_net' => $bs_net,
    'bt_brut' => $bt_brut,
    'bt_dep' => $bt_dep,
    'bt_net' => $bt_net,
    'bu_brut' => $bu_brut,
    'bu_dep' => $bu_dep,
    'bu_net' => $bu_net,
    'bz_brut' => $bz_brut,
    'bz_dep' => $bz_dep,
    'bz_net' => $bz_net,

    'ca' => $ca,
    'cb' => $cb,
    'cd' => $cd,
    'ce' => $ce,
    'cf' => $cf,
    'cg' => $cg,
    'ch' => $ch,
    'cj' => $cj,
    'cl' => $cl,
    'cm' => $cm,
    'cp' => $cp,
    'da' => $da,
    'db' => $db,
    'dc' => $dc,
    'dd' => $dd,
    'df' => $df,
    'dh' => $dh,
    'di' => $di,
    'dj' => $dj,
    'dk' => $dk,
    'dm' => $dm,
    'dn' => $dn,
    'dp' => $dp,
    'dq' => $dq,
    'dr' => $dr,
    'dt' => $dt,
    'dv' => $dv,
    'dz' => $dz
],'id_bilan = 1', $db);

echo 'Terminé';
     
?>