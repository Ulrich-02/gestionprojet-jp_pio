<?php

/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 23/12/2021
 * Time: 08:15
 */

// include('../../vendor/autoload.php');
// include('../../db.php');

// if(!isset($_SESSION['id_user']))
// {
//     header("location:../../connexion.php");
//     exit();
// }


// $query0 = "SELECT cpte FROM balance_n ";
// $statement0 = $db->prepare($query0);
// $statement0->execute();
// $count0 = $statement0->rowCount();

// $query2 = "SELECT cpte FROM balance_n_1 ";
// $statement2 = $db->prepare($query2);
// $statement2->execute();
// $count2 = $statement2->rowCount();

// $query3 = "SELECT xi FROM resultat ";
// $statement3 = $db->prepare($query3);
// $statement3->execute();
// $result3 = $statement3->fetchAll();
// $xi3 = 0;
// foreach($result3 as $row3) {
//     $xi3 = $row3["xi"];
// }



// // Si la balance n est vide
// if($count0 == 0)
// {
//     header("location:../../balances/balance-n/balance-n.php");
//     exit();
// }


// // Si la balance n-1 est vide
// if($count2 == 0)
// {
//     header("location:../../balances/balance-n-1/balance-n-1.php");
//     exit();
// }


// // Si le bilan est vide
// if($xi3 == 0)
// {
//     header("location:preparation.php");
//     exit();
// }




// $query = "SELECT * FROM resultat ";
// $statement = $db->prepare($query);
// $statement->execute();
// $result = $statement->fetchAll();



// $query1 = "SELECT * FROM infos ";
// $statement1 = $db->prepare($query1);
// $statement1->execute();
// $result1 = $statement1->fetchAll();

$nom = '';
$numero = '';
$an_n = 2020;
$an_n_1 = 2021;
$duree = '';
// foreach($result1 as $row1) {
//     $nom = $row1["nom_infos"];
//     $numero = $row1["numero_infos"];
//     $an_n = $row1["an_n_infos"];
//     $an_n_1 = $row1["an_n_1_infos"];
//     $duree = $row1["duree_infos"];
// }


$date = gmdate("d-m-Y");
$hour = gmdate("H:i");
$hour2 = gmdate("H-i");


// On met tiret quand une valeur est 0;
function jp($a)
{
    $b = ($a == 0) ? "-" : number_format($a, 0, ',', ' ');
    return $b;
}

// reference the Dompdf namespace
use Dompdf\Dompdf;

require_once "lib-php/dompdf/autoload.inc.php";

$output = '
        <html>
        <head>
            <style>
                .gras {font-weight: bold;}
                .pl-5 {padding-left: 5px;}
                .pr-5 {padding-right: 5px;}
                .bg-col-blue {background-color: #95b3d7;}
                .bg-col-gris {background-color: #D3D3D3;}
                .txt-center {text-align: center;}
                .txt-right {text-align: right;}
                .txt-white {color: white;}
                .txt-underline {text-decoration: underline;}
                .bg-color-blue {background-color: #95b3d7;}
                .bg-color-grey {background-color: #D3D3D3;}
                .bg-color-green {background-color: #33ff00;}
                .col-white {color: white;}
            </style>
        </head>
        <body>
		<div style="font-size: 12px !important;">
		    <h1 style="font-size: 13px !important;">
		        <span>Désignation entité: ' . $nom . '</span>
		        <span style="float: right;">Exercice clos le 31-12-' . $an_n . '</span>
		    </h1>
		    <h1 style="font-size: 13px !important;">
		        <span>Numéro d\'identification: ' . $numero . '</span>
		        <span style="float: right;">Durée (en mois): ' . $duree . '</span>
		    </h1>
		    <h1 style="margin-top: 30px; font-size: 14px !important; text-align: center;">NOTE 35 <br> <span style="color: #0070c0;">LISTE DES INFORMATIONS SOCIALES, ENVIRONNEMENTALES ET SOCIETALES A FOURNIR <br> <b style="color: black;">Note obligatoire pour les entités ayant un effectif de plus de 250 salariés</b></span></h1>
		    <!--<h1>Bilan du ' . $date . ' à ' . $hour . ' (Heure GMT)</h1>-->
            <table class="table-responsive" border="1" style="border-collapse:collapse; font-size: 12px !important;" >
                <tr class="bg-col-blue">
                    
                    <th style="min-width: 700px;">Liste des informations sociales, environnementales et sociétales à fournir</th>
                    
                </tr>
';

$output .= '
    			<tr class="bg-color-grey">
                    <td class="pl-5 pr-5 gras txt-center">SOLDES INTERMEDIAIRES DE GESTION</td>
    			</tr>
';

$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5"><b>Emploi :</b><br>
• l\'effectif total et la répartition des salariés par sexe, âge et zone géographique ;<br>
• les embauches et les licenciements ;<br>
• les rémunérations et leur évolution.</td>
    			</tr>
';
$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5"><b>Relations sociales :</b><br>
• l\'organisation du dialogue social ;<br>
• le bilan des accords collectifs.</td>
    			</tr>
';
$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5"><b>Santé et sécurité :</b><br>
• les conditions de santé et de sécurité au travail ;<br>
• le bilan des accords signés avec les organisations syndicales ou les représentants du personnel en
matière de santé et de sécurité au travail.</td>
    			</tr>
';
$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5"><b>Formation :</b><br>
• les politiques mises en œuvre en matière de formation ;<br>
• le nombre total d\'heures de formation.</td>
    			</tr>
';
$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5"><b>Égalité de traitement :</b><br>
• les mesures prises en faveur de l\'égalité entre les femmes et les hommes ;<br>
• les mesures prises en faveur de l\'emploi et de l\'insertion des personnes handicapées ;<br></td>
    			</tr>
';

$output .= '
    			<tr class="bg-color-grey">
                    <td class="pl-5 pr-5 gras txt-center">INFORMATIONS ENVIRONNEMENTALES</td>
    			</tr>
';

$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5"><b>Politique générale en matière environnementale :</b><br>
• l\'organisation de la société pour prendre en compte les questions environnementales et, le cas échéant,
les démarches d\'évaluation ou de certification en matière d\'environnement ;<br>
• les actions de formation et d\'information des salariés menées en matière de protection de
l\'environnement ;<br>
les moyens consacrés à la prévention des risques environnementaux et des pollutions.</td>
    			</tr>
';
$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5"><b>Pollution et gestion des déchets :</b><br>
• les mesures de prévention, de réduction ou de réparation de rejets dans l\'air, l\'eau et le sol affectant
gravement l\'environnement ;<br>
• les mesures de prévention, de recyclage et d\'élimination des déchets ;<br>
• la prise en compte des nuisances sonores et de toute autre forme de pollution spécifique à une activité.</td>
    			</tr>
';
$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5"><b>Utilisation durable des ressources :</b><br>
• la consommation d\'eau et l\'approvisionnement en eau en fonction des contraintes locales ;<br>
• la consommation de matières premières et les mesures prises pour améliorer l\'efficacité dans leur
utilisation ;<br>
• la consommation d\'énergie, les mesures prises pour améliorer l\'efficacité énergétique et le recours aux
énergies renouvelables.</td>
    			</tr>
';
$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5"><b>Changement climatique :</b><br>
• les rejets de gaz à effet de serre.</td>
    			</tr>
';
$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5"><b>Protection de la biodiversité :</b><br>
• les mesures prises pour préserver ou développer la biodiversité.</td>
    			</tr>
';

$output .= '
    			<tr class="bg-color-grey">
                    <td class="pl-5 pr-5 gras txt-center">INFORMATIONS RELATIVES AUX ENGAGEMENTS SOCIÉTAUX EN FAVEUR DU
                    DÉVELOPPEMENT DURABLE
                    </td>
    			</tr>
';

$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5"><b>Impact territorial, économique et social de l\'activité de la société :</b><br>
• en matière d\'emploi et de développement régional ;<br>
• sur les populations riveraines ou locales.</td>
    			</tr>
';
$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5"><b>Relations entretenues avec les personnes ou les organisations intéressées par l\'activité de la
société (associations d\'insertion, établissements d\'enseignement...) :</b><br>
• les conditions du dialogue avec ces personnes ou organisations ;<br>
• les actions de partenariat ou de mécénat.</td>
    			</tr>
';
$output .= '
    			<tr class="">
                    <td class="pl-5 pr-5"><b>Sous-traitance et fournisseurs :</b><br>
• la prise en compte dans la politique d\'achat des enjeux sociaux et environnementaux.</td>
    			</tr>
';

$output .= '
</table>
</div>
</body>
        </html>
';



// instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->loadHtml($output);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'portrait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
//$dompdf->stream(); // Télécharge directement le document
$file_name = 'Resultat_' . $date . '_' . $hour2 . '.pdf';
$dompdf->stream($file_name, array("Attachment" => false));
