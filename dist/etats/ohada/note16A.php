<?php

/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 23/12/2021
 * Time: 08:15
 */

// include('../../vendor/autoload.php');
// include('../../db.php');

// if(!isset($_SESSION['id_user']))
// {
//     header("location:../../connexion.php");
//     exit();
// }


// $query0 = "SELECT cpte FROM balance_n ";
// $statement0 = $db->prepare($query0);
// $statement0->execute();
// $count0 = $statement0->rowCount();

// $query2 = "SELECT cpte FROM balance_n_1 ";
// $statement2 = $db->prepare($query2);
// $statement2->execute();
// $count2 = $statement2->rowCount();

// $query3 = "SELECT xi FROM resultat ";
// $statement3 = $db->prepare($query3);
// $statement3->execute();
// $result3 = $statement3->fetchAll();
// $xi3 = 0;
// foreach($result3 as $row3) {
//     $xi3 = $row3["xi"];
// }



// // Si la balance n est vide
// if($count0 == 0)
// {
//     header("location:../../balances/balance-n/balance-n.php");
//     exit();
// }


// // Si la balance n-1 est vide
// if($count2 == 0)
// {
//     header("location:../../balances/balance-n-1/balance-n-1.php");
//     exit();
// }


// // Si le bilan est vide
// if($xi3 == 0)
// {
//     header("location:preparation.php");
//     exit();
// }




// $query = "SELECT * FROM resultat ";
// $statement = $db->prepare($query);
// $statement->execute();
// $result = $statement->fetchAll();



// $query1 = "SELECT * FROM infos ";
// $statement1 = $db->prepare($query1);
// $statement1->execute();
// $result1 = $statement1->fetchAll();

$nom = '';
$numero = '';
$an_n = 2020;
$an_n_1 = 2021;
$duree = '';
// foreach($result1 as $row1) {
//     $nom = $row1["nom_infos"];
//     $numero = $row1["numero_infos"];
//     $an_n = $row1["an_n_infos"];
//     $an_n_1 = $row1["an_n_1_infos"];
//     $duree = $row1["duree_infos"];
// }


$date = gmdate("d-m-Y");
$hour = gmdate("H:i");
$hour2 = gmdate("H-i");


// On met tiret quand une valeur est 0;
function jp($a)
{
    $b = ($a == 0) ? "-" : number_format($a, 0, ',', ' ');
    return $b;
}

// reference the Dompdf namespace
use Dompdf\Dompdf;

require_once "lib-php/dompdf/autoload.inc.php";

$output = '
        <html>
        <head>
            <style>
                .gras {font-weight: bold;}
                .pl-5 {padding-left: 5px;}
                .pr-5 {padding-right: 5px;}
                .bg-col-blue {background-color: #95b3d7;}
                .bg-col-gris {background-color: #D3D3D3;}
                .txt-center {text-align: center;}
                .txt-right {text-align: right;}
                .txt-white {color: white;}
                .txt-underline {text-decoration: underline;}
                .bg-color-blue {background-color: #95b3d7;}
                .bg-color-grey {background-color: #D3D3D3;}
                .bg-color-green {background-color: #33ff00;}
                .col-white {color: white;}
            </style>
        </head>
        <body>
		<div style="font-size: 12px !important;">
		    <h1 style="font-size: 13px !important;">
		        <span>Désignation entité: ' . $nom . '</span>
		        <span style="float: right;">Exercice clos le 31-12-' . $an_n . '</span>
		    </h1>
		    <h1 style="font-size: 13px !important;">
		        <span>Numéro d\'identification: ' . $numero . '</span>
		        <span style="float: right;">Durée (en mois): ' . $duree . '</span>
		    </h1>
		    <h1 style="margin-top: 30px; font-size: 14px !important; text-align: center;">NOTE 16A <br> <span style="color: #0070c0;">DETTES FINANCIERES ET RESSOURCES ASSIMILES</span></h1>
		    <!--<h1>Bilan du ' . $date . ' à ' . $hour . ' (Heure GMT)</h1>-->
            <table class="table-responsive" border="1" style="border-collapse:collapse; font-size: 12px !important;" >
                <tr class="bg-col-blue">
                    
                    <th style="min-width: 250px;">Libellés</th>
                    <th class="pl-5 pr-5">Année N</th>
                    <th class="pl-5 pr-5">Année N-1</th>
                    <th class="pl-5 pr-5">Variation en valeur absolue</th>
                    <th class="pl-5 pr-5">Variation en %</th>
                    <th class="pl-5 pr-5">Dettes à un an au plus</th>
                    <th class="pl-5 pr-5">Dettes à plus d\'un an et à deux ans au plus</th>
                    <th class="pl-5 pr-5">Dettes à plus de deux ans</th>
                    
                </tr>
';

$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Emprunts obligataires</td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Emprunts et dettes auprès des établissemnts de crédit</td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Avances reçues de l\'Etat</td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Communes et collectivités publiques décentralisées</td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Avances reçues et comptes courants bloqués</td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Dépôts et cautionnements reçus</td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Intérêts courus</td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Avances assorties de conditions particulières</td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Autres emprunts et dettes</td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Dettes liées à des participations</td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Comptes permanents bloqués des établissements et
succursales</td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="bg-color-grey">
    			
                    <td class="pl-5 pr-5 gras">TOTAL EMPRUNTS ET DETTES FINANCIERES</td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5" style="padding: 5px;"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Crédit bail immobilier</td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Crédit bail mobilier</td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Location vente</td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Intérêts courus Autres dettes de location acquisition</td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="bg-color-grey">
    			
                    <td class="pl-5 pr-5 gras">TOTAL DETTES DE LOCATION ACQUISITION</td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5" style="padding: 5px;"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Provisions pour litiges </td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Provisions pour garantie donnés aux clients </td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Provisions pour pertes sur marchés à achèvement futur </td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Provisions pour perts de change </td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Provisions pour impôts </td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Provisions pour pensions et obligations assimilées </td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Actif du régime de retraite</td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Provisions pour restructuration</td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Provisions pour amendes et pénalités</td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Provisions de propre assureur</td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Provisions pour démantèlement et remise en état</td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Provisions de droits à déduction</td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';
$output .= '
    			<tr class="">
    			
                    <td class="pl-5 pr-5">Autres provisions</td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';
$output .= '
    			<tr class="bg-color-grey">
    			
                    <td class="pl-5 pr-5 gras">TOTAL SUBVENTIONS ET PROVISIONS REGLEMENTEES</td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
    			    <td class="pl-5 pr-5"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
                    <td class="pl-5 pr-5 bg-color-grey"></td>
    			</tr>
';

$output .= '
            </table>
        </div>
        </body>
        </html>
';



// instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->loadHtml($output);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'portrait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
//$dompdf->stream(); // Télécharge directement le document
$file_name = 'Resultat_' . $date . '_' . $hour2 . '.pdf';
$dompdf->stream($file_name, array("Attachment" => false));
