<?php
/**
 * Created by PhpStorm.
 * User: Utilisateur
 * Date: 23/12/2021
 * Time: 08:15
 */

include('../../vendor/autoload.php');
include('../../db.php');

if(!isset($_SESSION['id_user']))
{
     header("location:../../connexion.php");
     exit();
}


$query0 = "SELECT cpte FROM balance_n ";
$statement0 = $db->prepare($query0);
$statement0->execute();
$count0 = $statement0->rowCount();

$query2 = "SELECT cpte FROM balance_n_1 ";
$statement2 = $db->prepare($query2);
$statement2->execute();
$count2 = $statement2->rowCount();

$query3 = "SELECT za FROM tft ";
$statement3 = $db->prepare($query3);
$statement3->execute();
$result3 = $statement3->fetchAll();
$za3 = 0;
foreach($result3 as $row3) {
    $za3 = $row3["za"];
}



// Si la balance n est vide
if($count0 == 0)
{
    header("location:../../balances/balance-n/balance-n.php");
    exit();
}


// Si la balance n-1 est vide
if($count2 == 0)
{
    header("location:../../balances/balance-n-1/balance-n-1.php");
    exit();
}


// Si le tft est vide
/*
if($za3 == 0)
{
    header("location:preparation.php");
    exit();
}
*/




$query = "SELECT * FROM tft ";
$statement = $db->prepare($query);
$statement->execute();
$result = $statement->fetchAll();



$query1 = "SELECT * FROM infos ";
$statement1 = $db->prepare($query1);
$statement1->execute();
$result1 = $statement1->fetchAll();

$nom = '';
$numero = '';
$an_n = '';
$an_n_1 = '';
$duree = '';
foreach($result1 as $row1) {
    $nom = $row1["nom_infos"];
    $numero = $row1["numero_infos"];
    $an_n = $row1["an_n_infos"];
    $an_n_1 = $row1["an_n_1_infos"];
    $duree = $row1["duree_infos"];
}


$date = gmdate("d-m-Y");
$hour = gmdate("H:i");
$hour2 = gmdate("H-i");


// On met tiret quand une valeur est 0;
function jp($a) {
    $b = ($a == 0) ? "-" : number_format($a, 0, ',', ' ');
    return $b;
}

// reference the Dompdf namespace
use Dompdf\Dompdf;

$output = '
        <html>
        <head>
            <style>
                .gras {font-weight: bold;}
                .pl-5 {padding-left: 5px;}
                .pr-5 {padding-right: 5px;}
                .bg-col-blue {background-color: #95b3d7;}
                .bg-col-gris {background-color: #D3D3D3;}
                .txt-center {text-align: center;}
                .txt-right {text-align: right;}
                .txt-white {color: white;}
                .bg-color-blue-claire {background-color: #95b3d7;}
                .bg-color-blue-fonce {background-color: #001f5f;}
                .bg-color-grey {background-color: #D3D3D3;}
                .bg-color-green {background-color: #33ff00;}
                .col-white {color: white;}
            </style>
        </head>
        <body>
		<div class="table-responsive" style="font-size: 12px !important;">
		    <h1 style="font-size: 13px !important;">
		        <span>Désignation entité: '.$nom.'</span>
		        <span style="float: right;">Exercice clos le 31-12-'.$an_n.'</span>
		    </h1>
		    <h1 style="font-size: 13px !important;">
		        <span>Numéro d\'identification: '.$numero.'</span>
		        <span style="float: right;">Durée (en mois): '.$duree.'</span>
		    </h1>
		    <h1 style="margin-top: 30px; font-size: 17px !important; text-align: center; color: #0070c0;">TABLEAU DES FLUX DE TRESORERIE AU 31 DECEMBRE '.$an_n.'</h1>
		    <!--<h1>Bilan du '.$date.' à '.$hour.' (Heure GMT)</h1>-->
            <table border="1" style="border-collapse:collapse;" >
                <tr class="bg-col-blue">
                    
                    <th class="pl-5 pr-5">REF</th>
                    <th colspan="2">LIBELLES</th>
                    <th class="pl-5 pr-5">NOTE</th>
                    <th class="pl-5 pr-5">EXERCICE <br> AU 31/12/'.$an_n.'</th>
                    <th class="pl-5 pr-5">EXERCICE <br> AU 31/12/'.$an_n_1.'</th>
                    
                    
                    
                </tr>
';

foreach($result as $row) {
    $output .= '
    			<tr class="gras">
    			
    			    <td class="txt-center">ZA</td>
    			    <td class="pl-5 pr-5 bg-color-green" style="width: 400px;">Trésorerie nette au 1er janvier <br> (Trésorerie actif N-1 - Trésorerie passif N-1)</td>
    			    <td class="txt-center bg-color-green" style="width: 25px;">A</td>.
    			    <td class="txt-center bg-color-green"></td>
    			    <td class="txt-right pr-5 bg-color-green">' . jp($row["za"]) . '</td>
    			    <td class="txt-right pr-5 bg-color-green">' . jp($row["za_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td class="txt-center"></td>
    			    <td class="pl-5 pr-5 bg-color-blue-claire">Flux de trésorerie provenant des activités opérationnelles</td>
    			    <td class="txt-center bg-color-blue-claire"></td>.
    			    <td class="txt-center bg-color-blue-claire"></td>
    			    <td class="txt-right pr-5 bg-color-blue-claire">' . '' . '</td>
    			    <td class="txt-right pr-5 bg-color-blue-claire">' . '' . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td class="txt-center">FA</td>
    			    <td class="pl-5 pr-5">Capacité d\'Autofinancement Globale (CAFG)</td>
    			    <td class="txt-center"></td>.
    			    <td class="txt-center"></td>
    			    <td class="txt-right pr-5">' . jp($row["fa"]) . '</td>
    			    <td class="txt-right pr-5">' . jp($row["fa_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td class="txt-center">FB</td>
    			    <td class="pl-5 pr-5">-Actif circulant HAO (1)</td>
    			    <td class="txt-center"></td>.
    			    <td class="txt-center"></td>
    			    <td class="txt-right pr-5">' . jp($row["fb"]) . '</td>
    			    <td class="txt-right pr-5">' . jp($row["fb_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td class="txt-center">FC</td>
    			    <td class="pl-5 pr-5">- Variation des stocks</td>
    			    <td class="txt-center"></td>.
    			    <td class="txt-center"></td>
    			    <td class="txt-right pr-5">' . jp($row["fc"]) . '</td>
    			    <td class="txt-right pr-5">' . jp($row["fc_1"]) . '</td>
                    
    			    
    			</tr>
';

    $output .= '
    			<tr>
    			
    			    <td class="txt-center">FD</td>
    			    <td class="pl-5 pr-5">- Variation des créances</td>
    			    <td class="txt-center"></td>.
    			    <td class="txt-center"></td>
    			    <td class="txt-right pr-5">' . jp($row["fd"]) . '</td>
    			    <td class="txt-right pr-5">' . jp($row["fd_1"]) . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td class="txt-center">FE</td>
    			    <td class="pl-5 pr-5">+ Variation du passif circulant (1)</td>
    			    <td class="txt-center"></td>.
    			    <td class="txt-center"></td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td class="txt-center"></td>
                    (FB+FC+FD+FE) : ………...............</td>
    			    <td class="pl-5 pr-5">Variation du BF lié aux activités opérationnelles <br>
    			    <td class="txt-center"></td>.
    			    <td class="txt-center"></td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr class="gras">
    			
    			    <td class="txt-center">ZB</td>
    			    <td class="pl-5 pr-5 bg-color-blue-claire">Flux de trésorerie provenant des activités opérationnelles<br>
                    (somme FA à FE)
                    </td>
    			    <td class="txt-center bg-color-blue-claire">B</td>
    			    <td class="txt-center bg-color-blue-claire"></td>
    			    <td class="txt-right pr-5 bg-color-blue-claire">' . '1 000 000 000' . '</td>
    			    <td class="txt-right pr-5 bg-color-blue-claire">' . '1 000 000 000' . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td class="txt-center"></td>
    			    <td class="pl-5 pr-5 bg-color-grey">Flux de trésorerie provenant des activités d’investissements</td>
    			    <td class="txt-center bg-color-grey"></td>.
    			    <td class="txt-center bg-color-grey"></td>
    			    <td class="txt-right pr-5 bg-color-grey">' . '' . '</td>
    			    <td class="txt-right pr-5 bg-color-grey">' . '' . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td class="txt-center">FF</td>
    			    <td class="pl-5 pr-5">- Décaissements liés aux acquisitions d\'immobilisations
                    incorporelles</td>
    			    <td class="txt-center"></td>.
    			    <td class="txt-center"></td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td class="txt-center">FG</td>
    			    <td class="pl-5 pr-5">- Décaissements liés aux acquisitions d\'immobilisations
                    corporelles</td>
    			    <td class="txt-center"></td>.
    			    <td class="txt-center"></td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td class="txt-center">FH</td>
    			    <td class="pl-5 pr-5">- Décaissements liés aux acquisitions d\'immobilisations
                    financières</td>
    			    <td class="txt-center"></td>.
    			    <td class="txt-center"></td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td class="txt-center">FI</td>
    			    <td class="pl-5 pr-5">+ Encaissements liés aux cessions d\'immobilisations
                    incorporelles et corporelles</td>
    			    <td class="txt-center"></td>.
    			    <td class="txt-center"></td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td class="txt-center">FJ</td>
    			    <td class="pl-5 pr-5">+ Encaissements liés aux cessions d’immobilisations financières</td>
    			    <td class="txt-center"></td>.
    			    <td class="txt-center"></td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr class="gras">
    			
    			    <td class="txt-center">ZC</td>
    			    <td class="pl-5 pr-5 bg-color-blue-claire">Flux de trésorerie provenant des activités d’investissement<br>
                    (somme FF à FJ)</td>
    			    <td class="txt-center bg-color-blue-claire">C</td>.
    			    <td class="txt-center bg-color-blue-claire"></td>
    			    <td class="txt-right pr-5 bg-color-blue-claire">' . '1 000 000 000' . '</td>
    			    <td class="txt-right pr-5 bg-color-blue-claire">' . '1 000 000 000' . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td class="txt-center"></td>
    			    <td class="pl-5 pr-5 bg-color-grey">Flux de trésorerie provenant du financement par les capitaux
                    propres</td>
    			    <td class="txt-center bg-color-grey"></td>.
    			    <td class="txt-center bg-color-grey"></td>
    			    <td class="txt-right pr-5 bg-color-grey">' . '' . '</td>
    			    <td class="txt-right pr-5 bg-color-grey">' . '' . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td class="txt-center">FK</td>
    			    <td class="pl-5 pr-5">+ Augmentations de capital par apports nouveaux</td>
    			    <td class="txt-center"></td>.
    			    <td class="txt-center"></td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td class="txt-center">FL</td>
    			    <td class="pl-5 pr-5">+ Subventions d\'investissement reçues</td>
    			    <td class="txt-center"></td>.
    			    <td class="txt-center"></td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td class="txt-center">FM</td>
    			    <td class="pl-5 pr-5">- Prélèvements sur le capital </td>
    			    <td class="txt-center"></td>.
    			    <td class="txt-center"></td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td class="txt-center">FN</td>
    			    <td class="pl-5 pr-5">- Dividendes versés</td>
    			    <td class="txt-center"></td>.
    			    <td class="txt-center"></td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr class="gras">
    			
    			    <td class="txt-center">ZD</td>
    			    <td class="pl-5 pr-5 bg-color-blue-claire">Flux de trésorerie provenant des capitaux propres<br>
                    (somme FK à FN) </td>
    			    <td class="txt-center bg-color-blue-claire">D</td>
    			    <td class="txt-center bg-color-blue-claire"></td>
    			    <td class="txt-right pr-5 bg-color-blue-claire">' . '1 000 000 000' . '</td>
    			    <td class="txt-right pr-5 bg-color-blue-claire">' . '1 000 000 000' . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td class="txt-center"></td>
    			    <td class="pl-5 pr-5 bg-color-grey">Trésorerie provenant du financement par les capitaux
                    étrangers
                    </td>
    			    <td class="txt-center bg-color-grey"></td>
    			    <td class="txt-center bg-color-grey"></td>
    			    <td class="txt-right pr-5 bg-color-grey">' . '' . '</td>
    			    <td class="txt-right pr-5 bg-color-grey">' . '' . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td class="txt-center">FO</td>
    			    <td class="pl-5 pr-5">+ Emprunts</td>
    			    <td class="txt-center"></td>
    			    <td class="txt-center"></td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td class="txt-center">FP</td>
    			    <td class="pl-5 pr-5">+ Autres dettes financières</td>
    			    <td class="txt-center"></td>
    			    <td class="txt-center"></td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td class="txt-center">FQ</td>
    			    <td class="pl-5 pr-5">- Remboursements des emprunts et autres dettes financières</td>
    			    <td class="txt-center"></td>
    			    <td class="txt-center"></td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td class="txt-center">ZE</td>
    			    <td class="pl-5 pr-5 bg-color-grey">Flux de trésorerie provenant des capitaux étrangers<br>
                    (somme FO à FQ)</td>
    			    <td class="txt-center bg-color-grey">E</td>
    			    <td class="txt-center bg-color-grey"></td>
    			    <td class="txt-right pr-5 bg-color-grey">' . '1 000 000 000' . '</td>
    			    <td class="txt-right pr-5 bg-color-grey">' . '1 000 000 000' . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr class="gras">
    			
    			    <td class="txt-center">ZF</td>
    			    <td class="pl-5 pr-5 bg-color-blue-claire">Flux de trésorerie provenant des activités de financement<br>
                    (D+E)
                    </td>
    			    <td class="txt-center bg-color-blue-claire">F</td>
    			    <td class="txt-center bg-color-blue-claire"></td>
    			    <td class="txt-right pr-5 bg-color-blue-claire">' . '1 000 000 000' . '</td>
    			    <td class="txt-right pr-5 bg-color-blue-claire">' . '1 000 000 000' . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr>
    			
    			    <td class="txt-center">ZG</td>
    			    <td class="pl-5 pr-5">VARIATION DE LA TRÉSORERIE NETTE DE LA
                    PÉRIODE (B+C+F)</td>
    			    <td class="txt-center">G</td>
    			    <td class="txt-center"></td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
    			    <td class="txt-right pr-5">' . '1 000 000 000' . '</td>
                    
    			    
    			</tr>
';


    $output .= '
    			<tr class="gras">
    			
    			    <td class="txt-center">ZH</td>
    			    <td class="pl-5 pr-5 bg-color-green">Trésorerie nette au 31 Décembre (G+A)<br>
                    Contrôle : Trésorerie actif N - Trésorerie passif N =</td>
    			    <td class="txt-center bg-color-green">H</td>
    			    <td class="txt-center bg-color-green"></td>
    			    <td class="txt-right pr-5 bg-color-green">' . '1 000 000 000' . '</td>
    			    <td class="txt-right pr-5 bg-color-green">' . '1 000 000 000' . '</td>
                    
    			    
    			</tr>
';

}

$output .= '
            </table>
        </div>
        </body>
        </html>
';





// instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->loadHtml($output);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'portrait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
//$dompdf->stream(); // Télécharge directement le document
$file_name = 'TFT_'.$date.'_'.$hour2.'.pdf';
$dompdf->stream($file_name, array("Attachment" => false));