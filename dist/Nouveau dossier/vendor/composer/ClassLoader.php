f ($repo['type'] === 'filesystem') {
$repos[$name] = new FilesystemRepository($repo['json']);
} else {
$repos[$name] = $rm->createRepository($repo['type'], $repo, $index);
}
}

return $repos;
}








public static function generateRepositoryName($index, array $repo, array $existingRepos)
{
$name = is_int($index) && isset($repo['url']) ? preg_replace('{^https?://}i', '', $repo['url']) : $index;
while (isset($existingRepos[$name])) {
$name .= '2';
}

return $name;
}
}
<?php











namespace Composer\Repository;

use Composer\Package\PackageInterface;
use Composer\Package\BasePackage;
use Composer\Semver\Constraint\ConstraintInterface;








interface RepositoryInterface extends \Countable
{
const SEARCH_FULLTEXT = 0;
const SEARCH_NAME = 1;








public function hasPackage(PackageInterface $package);









public function findPackage($name, $constraint);









public function findPackages($name, $constraint = null);






public function getPackages();

















public function loadPackages(array $packageNameMap, array $acceptableStabilities, array $stabilityFlags, array $alreadyLoaded = array());











public function search($query, $mode = 0, $type = null);











public function getProviders($packageName);








public function getRepoName();
}
<?php











namespace Composer\Repository;

use Composer\IO\IOInterface;
use Composer\Config;
use Composer\EventDispatcher\EventDispatcher;
use Composer\Package\PackageInterface;
use Composer\Util\HttpDownloader;
use Composer\Util\ProcessExecutor;








class RepositoryManager
{

private $localRepository;

private $repositories = array();

private $repositoryClasses = array();

private $io;

private $config;

private $httpDownloader;

private $eventDispatcher;

private $process;

public function __construct(IOInterface $io, Config $config, HttpDownloader $httpDownloader, EventDispatcher $eventDispatcher = null, ProcessExecutor $process = null)
{
$this->io = $io;
$this->config = $config;
$this->httpDownloader = $httpDownloader;
$this->eventDispatcher = $eventDispatcher;
$this->process = $process ?: new ProcessExecutor($io);
}









public function findPackage($name, $constraint)
{
foreach ($this->repositories as $repository) {

if ($package = $repository->findPackage($name, $constraint)) {
return $package;
}
}

return null;
}









public function findPackages($name, $constraint)
{
$packages = array();

foreach ($this->getRepositories() as $repository) {
$packages = array_merge($packages, $repository->findPackages($name, $constraint));
}

return $packages;
}








public function addRepository(RepositoryInterface $repository)
{
$this->repositories[] = $repository;
}










public function prependRepository(RepositoryInterface $repository)
{
array_unshift($this->repositories, $repository);
}










public function createRepository($type, $config, $name = null)
{
if (!isset($this->repositoryClasses[$type])) {
throw new \InvalidArgumentException('Repository type is not registered: '.$type);
}

if (isset($config['packagist']) && false === $config['packagist']) {
$this->io->writeError('<warning>Repository "'.$name.'" ('.json_encode($config).') has a packagist key which should be in its own repository definition</warning>');
}

$class = $this->repositoryClasses[$type];

if (isset($config['only']) || isset($config['exclude']) || isset($config['canonical'])) {
$filterConfig = $config;
unset($config['only'], $config['exclude'], $config['canonical']);
}

$repository = new $class($config, $this->io, $this->config, $this->httpDownloader, $this->eventDispatcher, $this->process);

if (isset($filterConfig)) {
$repository = new FilterRepository($repository, $filterConfig);
}

return $repository;
}









public function setRepositoryClass($type, $class)
{
$this->repositoryClasses[$type] = $class;
}






public function getRepositories()
{
return $this->repositories;
}








public function setLocalRepository(InstalledRepositoryInterface $repository)
{
$this->localRepository = $repository;
}






public function getLocalRepository()
{
return $this->localRepository;
}
}
<?php











namespace Composer\Repository;






class RepositorySecurityException extends \Exception
{
}
<?php











namespace Composer\Repository;

use Composer\DependencyResolver\Pool;
use Composer\DependencyResolver\PoolBuilder;
use Composer\DependencyResolver\Request;
use Composer\EventDispatcher\EventDispatcher;
use Composer\IO\IOInterface;
use Composer\IO\NullIO;
use Composer\Package\BasePackage;
use Composer\Package\AliasPackage;
use Composer\Package\CompleteAliasPackage;
use Composer\Package\CompletePackage;
use Composer\Semver\Constraint\ConstraintInterface;
use Composer\Package\Version\StabilityFilter;




class RepositorySet
{



const ALLOW_UNACCEPTABLE_STABILITIES = 1;



const ALLOW_SHADOWED_REPOSITORIES = 2;





private $rootAliases;





private $rootReferences;


private $repositories = array();





private $acceptableStabilities;





private $stabilityFlags;





private $rootRequires;


private $locked = false;

private $allowInstalledRepositories = false;
















public function __construct($minimumStability = 'stable', array $stabilityFlags = array(), array $rootAliases = array(), array $rootReferences = array(), array $rootRequires = array())
{
$this->rootAliases = self::getRootAliasesPerPackage($rootAliases);
$this->rootReferences = $rootReferences;

$this->acceptableStabilities = array();
foreach (BasePackage::$stabilities as $stability => $value) {
if ($value <= BasePackage::$stabilities[$minimumStability]) {
$this->acceptableStabilities[$stability] = $value;
}
}
$this->stabilityFlags = $stabilityFlags;
$this->rootRequires = $rootRequires;
foreach ($rootRequires as $name => $constraint) {
if (PlatformRepository::isPlatformPackage($name)) {
unset($this->rootRequires[$name]);
}
}
}






public function allowInstalledRepositories($allow = true)
{
$this->allowInstalledRepositories = $allow;
}





public function getRootRequires()
{
return $this->rootRequires;
}











public function addRepository(RepositoryInterface $repo)
{
if ($this->locked) {
throw new \RuntimeException("Pool has already been created from this repository set, it cannot be modified anymore.");
}

if ($repo instanceof CompositeRepository) {
$repos = $repo->getRepositories();
} else {
$repos = array($repo);
}

foreach ($repos as $repo) {
$this->repositories[] = $repo;
}
}











public function findPackages($name, ConstraintInterface $constraint = null, $flags = 0)
{
$ignoreStability = ($flags & self::ALLOW_UNACCEPTABLE_STABILITIES) !== 0;
$loadFromAllRepos = ($flags & self::ALLOW_SHADOWED_REPOSITORIES) !== 0;

$packages = array();
if ($loadFromAllRepos) {
foreach ($this->repositories as $repository) {
$packages[] = $repository->findPackages($name, $constraint) ?: array();
}
} else {
foreach ($this->repositories as $repository) {
$result = $repository->loadPackages(array($name => $constraint), $ignoreStability ? BasePackage::$stabilities : $this->acceptableStabilities, $ignoreStability ? array() : $this->stabilityFlags);

$packages[] = $result['packages'];
foreach ($result['namesFound'] as $nameFound) {

if ($name === $nameFound) {
break 2;
}
}
}
}

$candidates = $packages ? call_user_func_array('array_merge', $packages) : array();


if ($ignoreStability || !$loadFromAllRepos) {
return $candidates;
}

$result = array();
foreach ($candidates as $candidate) {
if ($this->isPackageAcceptable($candidate->getNames(), $candidate->getStability())) {
$result[] = $candidate;
}
}

return $result;
}







public function getProviders($packageName)
{
$providers = array();
foreach ($this->repositories as $repository) {
if ($repoProviders = $repository->getProviders($packageName)) {
$providers = array_merge($providers, $repoProviders);
}
}

return $providers;
}








public function isPackageAcceptable($names, $stability)
{
return StabilityFilter::isPackageAcceptable($this->acceptableStabilities, $this->stabilityFlags, $names, $stability);
}






public function createPool(Request $request, IOInterface $io, EventDispatcher $eventDispatcher = null)
{
$poolBuilder = new PoolBuilder($this->acceptableStabilities, $this->stabilityFlags, $this->rootAliases, $this->rootReferences, $io, $eventDispatcher);

foreach ($this->repositories as $repo) {
if (($repo instanceof InstalledRepositoryInterface || $repo instanceof InstalledRepository) && !$this->allowInstalledRepositories) {
throw new \LogicException('The pool can not accept packages from an installed repository');
}
}

$this->locked = true;

return $poolBuilder->buildPool($this->repositories, $request);
}






public function createPoolWithAllPackages()
{
foreach ($this->repositories as $repo) {
if (($repo instanceof InstalledRepositoryInterface || $repo instanceof InstalledRepository) && !$this->allowInstalledRepositories) {
throw new \LogicException('The pool can not accept packages from an installed repository');
}
}

$this->locked = true;

$packages = array();
foreach ($this->repositories as $repository) {
foreach ($repository->getPackages() as $package) {
$packages[] = $package;

if (isset($this->rootAliases[$package->getName()][$package->getVersion()])) {
$alias = $this->rootAliases[$package->getName()][$package->getVersion()];
while ($package instanceof AliasPackage) {
$package = $package->getAliasOf();
}
if ($package instanceof CompletePackage) {
$aliasPackage = new CompleteAliasPackage($package, $alias['alias_normalized'], $alias['alias']);
} else {
$aliasPackage = new AliasPackage($package, $alias['alias_normalized'], $alias['alias']);
}
$aliasPackage->setRootPackageAlias(true);
$packages[] = $aliasPackage;
}
}
}

return new Pool($packages);
}






public function createPoolForPackage($packageName, LockArrayRepository $lockedRepo = null)
{

return $this->createPoolForPackages(array($packageName), $lockedRepo);
}






public function createPoolForPackages($packageNames, LockArrayRepository $lockedRepo = null)
{
$request = new Request($lockedRepo);

foreach ($packageNames as $packageName) {
if (PlatformRepository::isPlatformPackage($packageName)) {
throw new \LogicException('createPoolForPackage(s) can not be used for platform packages, as they are never loaded by the PoolBuilder which expects them to be fixed. Use createPoolWithAllPackages or pass in a proper request with the platform packages you need fixed in it.');
}

$request->requireName($packageName);
}

return $this->createPool($request, new NullIO());
}







private static function getRootAliasesPerPackage(array $aliases)
{
$normalizedAliases = array();

foreach ($aliases as $alias) {
$normalizedAliases[$alias['package']][$alias['version']] = array(
'alias' => $alias['alias'],
'alias_normalized' => $alias['alias_normalized'],
);
}

return $normalizedAliases;
}
}
<?php











namespace Composer\Repository;

use Composer\Package\RootPackageInterface;








class RootPackageRepository extends ArrayRepository
{
public function __construct(RootPackageInterface $package)
{
parent::__construct(array($package));
}

public function getRepoName()
{
return 'root package repo';
}
}
<?php











namespace Composer\Repository\Vcs;

use Composer\Cache;
use Composer\Downloader\TransportException;
use Composer\Json\JsonFile;
use Composer\Util\Bitbucket;
use Composer\Util\Http\Response;

abstract class BitbucketDriver extends VcsDriver
{

protected $owner;

protected $repository;

protected $hasIssues = false;

protected $rootIdentifier;

protected $tags;

protected $branches;

protected $branchesUrl = '';

protected $tagsUrl = '';

protected $homeUrl = '';

protected $website = '';

protected $cloneHttpsUrl = '';




protected $fallbackDriver = null;

protected $vcsType;




public function initialize()
{
preg_match('#^https?://bitbucket\.org/([^/]+)/([^/]+?)(\.git|/?)$#i', $this->url, $match);
$this->owner = $match[1];
$this->repository = $match[2];
$this->originUrl = 'bitbucket.org';
$this->cache = new Cache(
$this->io,
implode('/', array(
$this->config->get('cache-repo-dir'),
$this->originUrl,
$this->owner,
$this->repository,
))
);
$this->cache->setReadOnly($this->config->get('cache-read-only'));
}




public function getUrl()
{
if ($this->fallbackDriver) {
return $this->fallbackDriver->getUrl();
}

return $this->cloneHttpsUrl;
}








protected function getRepoData()
{
$resource = sprintf(
'https://api.bitbucket.org/2.0/repositories/%s/%s?%s',
$this->owner,
$this->repository,
http_build_query(
array('fields' => '-project,-owner'),
'',
'&'
)
);

$repoData = $this->fetchWithOAuthCredentials($resource, true)->decodeJson();
if ($this->fallbackDriver) {
return false;
}
$this->parseCloneUrls($repoData['links']['clone']);

$this->hasIssues = !empty($repoData['has_issues']);
$this->branchesUrl = $repoData['links']['branches']['href'];
$this->tagsUrl = $repoData['links']['tags']['href'];
$this->homeUrl = $repoData['links']['html']['href'];
$this->website = $repoData['website'];
$this->vcsType = $repoData['scm'];

return true;
}




public function getComposerInformation($identifier)
{
if ($this->fallbackDriver) {
return $this->fallbackDriver->getComposerInformation($identifier);
}

if (!isset($this->infoCache[$identifier])) {
if ($this->shouldCache($identifier) && $res = $this->cache->read($identifier)) {
$composer = JsonFile::parseJson($res);
} else {
$composer = $this->getBaseComposerInformation($identifier);

if ($this->shouldCache($i