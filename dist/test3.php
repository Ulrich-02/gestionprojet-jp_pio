<?php

// ========================================== BILAN ===========================================================


//Calcul avec la balance N
//  ACTIF

//Immo incorporelles AD

$ae_brut = brut_cpte([211, 2181, 2191]);
$af_brut = brut_cpte([212, 213, 214, 2193]);
$ag_brut = brut_cpte([215, 216]);
$ah_brut = brut_cpte([217, 218, 2198], [2181]);

$ae_dep = dep_cpte([2811, 2818, 2911, 2918, 2919]);
$af_dep = dep_cpte([2812, 2813, 2814, 2912, 2913, 2914, 2919]);
$ag_dep = dep_cpte([2815, 2816, 2915, 2916]);
$ah_dep = dep_cpte([2817, 2818, 2917, 2918, 2919]);

$ae_net = $ae_brut - $ae_dep;
$af_net = $af_brut - $af_dep;
$ag_net = $ag_brut - $ag_dep;
$ah_net = $ah_brut - $ah_dep;

//ad = ae + af + ag + ah
$ad_brut = $ae_brut + $af_brut + $ag_brut + $ah_brut;
$ad_dep = $ae_dep + $af_dep + $ag_dep + $ah_dep;

//Valeur nette de AD
$ad_net = $ad_brut - $ad_dep;



//Immo corporelles AI

$aj_brut = brut_cpte([22]);
$ak_brut = brut_cpte([231, 232, 233, 237, 2391]);
$al_brut = brut_cpte([234, 235, 238, 2392, 2393]);
$am_brut = brut_cpte([24], [245, 2495]);
$an_brut = brut_cpte([245, 2495]);
$ap_brut = brut_cpte([251, 252]);

$aj_dep = dep_cpte([282, 292]);
$ak_dep = dep_cpte([2831, 2832, 2833, 2837, 2931, 2932, 2933, 2937, 2939]);
$al_dep = dep_cpte([2834, 2835, 2838, 2934, 2935, 2938, 2939]);
$am_dep = dep_cpte([284, 294, 2949], [2845, 2945, 2949]);
$an_dep = dep_cpte([2845, 2945, 2949]);
$ap_dep = dep_cpte([2951, 2952]);

$aj_net = $aj_brut - $aj_dep;
$ak_net = $ak_brut - $ak_dep;
$al_net = $al_brut - $al_dep;
$am_net = $am_brut - $am_dep;
$an_net = $an_brut - $an_dep;
$ap_net = $ap_brut - $ap_dep;



//ai = aj + ak + al + an + am (La balance Enlive n'a pas pris ap)
$ai_brut = $aj_brut + $ak_brut + $al_brut + $am_brut + $an_brut;
$ai_dep = $aj_dep + $ak_dep + $al_dep + $am_dep + $an_dep;

//Valeur nette de AI
$ai_net = $ai_brut - $ai_dep;


//Immo financière AQ

$ar_brut = brut_cpte([26]);
$as_brut = brut_cpte([27]);

$ar_dep = dep_cpte([296]);
$as_dep = dep_cpte([297]);

$ar_net = $ar_brut - $ar_dep;
$as_net = $as_brut - $as_dep;

//aq = ar + as
$aq_brut = $ar_brut + $as_brut;
$aq_dep = $ar_dep + $as_dep;

//Valeur nette de AQ
$aq_net = $aq_brut - $aq_dep;



//Total actif immo AZ

//az = ad + ai + ap + aq
$az_brut = $ad_brut + $ai_brut + $ap_brut + $aq_brut;
$az_dep = $ad_dep + $ai_dep + $ap_dep + $aq_dep;

//Valeur nette de AZ
$az_net = $az_brut - $az_dep;



//Total actif cirlulant BK

$ba_brut = brut_cpte([485, 488]);
$bb_brut = brut_cpte([31, 32, 33, 34, 35, 36, 37, 38]);
$bh_brut = brut_cpte([409]);
$bi_brut = brut_cpte([41], [419]);
$bj_brut = brut_cpte([185, 42, 43, 44, 45, 46, 47], [478]);

$bg_brut = $bh_brut + $bi_brut + $bj_brut;

$ba_dep = dep_cpte([498]);
$bb_dep = dep_cpte([39]);
$bh_dep = dep_cpte([490]);
$bi_dep = dep_cpte([491]);
$bj_dep = dep_cpte([492, 493, 494, 495, 496, 497]);

$bg_dep = $bh_dep + $bi_dep + $bj_dep;

$ba_net = $ba_brut - $ba_dep;
$bb_net = $bb_brut - $bb_dep;
$bh_net = $bh_brut - $bh_dep;
$bi_net = $bi_brut - $bi_dep;
$bj_net = $bj_brut - $bj_dep;

$bg_net = $bg_brut - $bg_dep;

//bk = ba + bb + bg, avec bg = bh + bi + bj
$bk_brut = $ba_brut + $bb_brut + $bg_brut;
$bk_dep = $ba_dep + $bb_dep + $bg_dep;

//Valeur nette de BK
$bk_net = $bk_brut - $bk_dep;


//Total trésorerie actif BT

$bq_brut = brut_cpte([50]);
$br_brut = brut_cpte([51]);
$bs_brut = brut_cpte([52, 53, 54, 55, 57, 581, 582]);

$bq_dep = dep_cpte([590]);
$br_dep = dep_cpte([591]);
$bs_dep = dep_cpte([592, 593, 594]);

$bq_net = $bq_brut - $bq_dep;
$br_net = $br_brut - $br_dep;
$bs_net = $bs_brut - $bs_dep;

//bt = bq + br + bs
$bt_brut = $bq_brut + $br_brut + $bs_brut;
$bt_dep = $bq_dep + $br_dep + $bs_dep;

//Valeur nette de BT
$bt_net = $bt_brut - $bt_dep;


// Ecart de conversion
$bu_brut = brut_cpte([478]);
$bu_dep = 0;

//Valeur nette de BU
$bu_net = $bu_brut - $bu_dep;


//Total général
$bz_brut = $az_brut + $bk_brut + $bt_brut + $bu_brut;
$bz_dep = $az_dep + $bk_dep + $bt_dep + $bu_dep;


//Valeur nette de BZ
$bz_net = $bz_brut - $bz_dep;



//  PASSIF

//Total capitaux propres et ressources assimilées CP

$ca = dep_cpte([101, 102, 103, 104]);

// $cb, brut_cpte car sa valeur reste dans SFD, et on le rend négatif (voir tableau de correspondance)
$cb = 0 - (brut_cpte([109]));

$cd = dep_cpte([105]);
$ce = dep_cpte([106]);
$cf = dep_cpte([111, 112, 113]);
$cg = dep_cpte([118]);


// Le report à nouveau peut être débiteur ou créditeur
$ch_debiteur = brut_cpte([12]);
$ch_crediteur = dep_cpte([12]);
$ch = 0;
if ($ch_debiteur > 0 && $ch_crediteur == 0) {
    $ch = 0 - $ch_debiteur; //on rend négatif la valeur de ch
}
if ($ch_crediteur > 0 && $ch_debiteur == 0) {
    $ch = $ch_crediteur;
}
if ($ch_crediteur == 0 && $ch_debiteur == 0) {
    $ch = 0;
}


// Le résultat net peut être débiteur ou créditeur
/*
$cj_debiteur = brut_cpte([13]);
$cj_crediteur = dep_cpte([13]);
$cj = 0;
if ($cj_debiteur > 0 && $cj_crediteur == 0) {
    $cj = 0 - $cj_debiteur; //on rend négatif la valeur de cj
}
if ($cj_crediteur > 0 && $cj_debiteur == 0) {
    $cj = $cj_crediteur;
}
if ($cj_crediteur == 0 && $cj_debiteur == 0) {
    $cj = 0;
}
*/

// Le résultat net est égal au résultat net du compte de résultat
//$cj = $xi;
$cj = 0;



$cl = dep_cpte([14]);
$cm = dep_cpte([15]);

$cp = $ca + $cb + $cd + $ce + $cf + $cg + $ch + $cj + $cl + $cm;



//Total dette financières et ressources assimilées DD

$da = dep_cpte([16, 181, 182, 183, 184]);
$db = dep_cpte([17]);
$dc = dep_cpte([19]);

$dd = $da + $db + $dc;


// Total Ressources stables DF
$df = $cp + $dd;



//Total passif circulant
$dh = dep_cpte([481, 482, 484, 4998]);
$di = dep_cpte([419]);
$dj = dep_cpte([40], [409]);
$dk = dep_cpte([42, 43, 44]);
$dm = dep_cpte([185, 45, 46, 47], [479]);
$dn = dep_cpte([499, 599], [4998]);

$dp = $dh + $di + $dj + $dk + $dm + $dn;



//Total trésorerie passif
$dq = dep_cpte([564, 565]);
$dr = dep_cpte([52, 53, 561, 566]);

$dt = $dq + $dr;



//Ecart de conversion-Passif
$dv = dep_cpte([479]);


//Total général
$dz = $cp + $df + $dp + $dt + $dv;

















//=================================================================================================================================


// Calcul avec la balance N-1

//  ACTIF

//Immo incorporelles AD

$ae_brut_n_1 = brut_cpte_n_1([211, 2181, 2191]);
$af_brut_n_1 = brut_cpte_n_1([212, 213, 214, 2193]);
$ag_brut_n_1 = brut_cpte_n_1([215, 216]);
$ah_brut_n_1 = brut_cpte_n_1([217, 218, 2198], [2181]);

$ae_dep_n_1 = dep_cpte_n_1([2811, 2818, 2911, 2918, 2919]);
$af_dep_n_1 = dep_cpte_n_1([2812, 2813, 2814, 2912, 2913, 2914, 2919]);
$ag_dep_n_1 = dep_cpte_n_1([2815, 2816, 2915, 2916]);
$ah_dep_n_1 = dep_cpte_n_1([2817, 2818, 2917, 2918, 2919]);

$ae_net_n_1 = $ae_brut_n_1 - $ae_dep_n_1;
$af_net_n_1 = $af_brut_n_1 - $af_dep_n_1;
$ag_net_n_1 = $ag_brut_n_1 - $ag_dep_n_1;
$ah_net_n_1 = $ah_brut_n_1 - $ah_dep_n_1;

//ad = ae + af + ag + ah
$ad_brut_n_1 = $ae_brut_n_1 + $af_brut_n_1 + $ag_brut_n_1 + $ah_brut_n_1;
$ad_dep_n_1 = $ae_dep_n_1 + $af_dep_n_1 + $ag_dep_n_1 + $ah_dep_n_1;

//Valeur nette de AD
$ad_net_n_1 = $ad_brut_n_1 - $ad_dep_n_1;



//Immo corporelles AI

$aj_brut_n_1 = brut_cpte_n_1([22]);
$ak_brut_n_1 = brut_cpte_n_1([231, 232, 233, 237, 2391]);
$al_brut_n_1 = brut_cpte_n_1([234, 235, 238, 2392, 2393]);
$am_brut_n_1 = brut_cpte_n_1([24], [245, 2495]);
$an_brut_n_1 = brut_cpte_n_1([245, 2495]);
$ap_brut_n_1 = brut_cpte_n_1([251, 252]);

$aj_dep_n_1 = dep_cpte_n_1([282, 292]);
$ak_dep_n_1 = dep_cpte_n_1([2831, 2832, 2833, 2837, 2931, 2932, 2933, 2937, 2939]);
$al_dep_n_1 = dep_cpte_n_1([2834, 2835, 2838, 2934, 2935, 2938, 2939]);
$am_dep_n_1 = dep_cpte_n_1([284, 294, 2949], [2845, 2945, 2949]);
$an_dep_n_1 = dep_cpte_n_1([2845, 2945, 2949]);
$ap_dep_n_1 = dep_cpte_n_1([2951, 2952]);

$aj_net_n_1 = $aj_brut_n_1 - $aj_dep_n_1;
$ak_net_n_1 = $ak_brut_n_1 - $ak_dep_n_1;
$al_net_n_1 = $al_brut_n_1 - $al_dep_n_1;
$am_net_n_1 = $am_brut_n_1 - $am_dep_n_1;
$an_net_n_1 = $an_brut_n_1 - $an_dep_n_1;
$ap_net_n_1 = $ap_brut_n_1 - $ap_dep_n_1;



//ai = aj + ak + al + an + am
$ai_brut_n_1 = $aj_brut_n_1 + $ak_brut_n_1 + $al_brut_n_1 + $am_brut_n_1 + $an_brut_n_1;
$ai_dep_n_1 = $aj_dep_n_1 + $ak_dep_n_1 + $al_dep_n_1 + $am_dep_n_1 + $an_dep_n_1;

//Valeur nette de AI
$ai_net_n_1 = $ai_brut_n_1 - $ai_dep_n_1;


//Immo financière AQ

$ar_brut_n_1 = brut_cpte_n_1([26]);
$as_brut_n_1 = brut_cpte_n_1([27]);

$ar_dep_n_1 = dep_cpte_n_1([296]);
$as_dep_n_1 = dep_cpte_n_1([297]);

$ar_net_n_1 = $ar_brut_n_1 - $ar_dep_n_1;
$as_net_n_1 = $as_brut_n_1 - $as_dep_n_1;

//aq = ar + as
$aq_brut_n_1 = $ar_brut_n_1 + $as_brut_n_1;
$aq_dep_n_1 = $ar_dep_n_1 + $as_dep_n_1;

//Valeur nette de AQ
$aq_net_n_1 = $aq_brut_n_1 - $aq_dep_n_1;



//Total actif immo AZ

//az = ad + ai + ap + aq
$az_brut_n_1 = $ad_brut_n_1 + $ai_brut_n_1 + $ap_brut_n_1 + $aq_brut_n_1;
$az_dep_n_1 = $ad_dep_n_1 + $ai_dep_n_1 + $ap_dep_n_1 + $aq_dep_n_1;

//Valeur nette de AZ
$az_net_n_1 = $az_brut_n_1 - $az_dep_n_1;



//Total actif cirlulant BK

$ba_brut_n_1 = brut_cpte_n_1([485, 488]);
$bb_brut_n_1 = brut_cpte_n_1([31, 32, 33, 34, 35, 36, 37, 38]);
$bh_brut_n_1 = brut_cpte_n_1([409]);
$bi_brut_n_1 = brut_cpte_n_1([41], [419]);
$bj_brut_n_1 = brut_cpte_n_1([185, 42, 43, 44, 45, 46, 47], [478]);

$bg_brut_n_1 = $bh_brut_n_1 + $bi_brut_n_1 + $bj_brut_n_1;

$ba_dep_n_1 = dep_cpte_n_1([498]);
$bb_dep_n_1 = dep_cpte_n_1([39]);
$bh_dep_n_1 = dep_cpte_n_1([490]);
$bi_dep_n_1 = dep_cpte_n_1([491]);
$bj_dep_n_1 = dep_cpte_n_1([492, 493, 494, 495, 496, 497]);

$bg_dep_n_1 = $bh_dep_n_1 + $bi_dep_n_1 + $bj_dep_n_1;

$ba_net_n_1 = $ba_brut_n_1 - $ba_dep_n_1;
$bb_net_n_1 = $bb_brut_n_1 - $bb_dep_n_1;
$bh_net_n_1 = $bh_brut_n_1 - $bh_dep_n_1;
$bi_net_n_1 = $bi_brut_n_1 - $bi_dep_n_1;
$bj_net_n_1 = $bj_brut_n_1 - $bj_dep_n_1;

$bg_net_n_1 = $bg_brut_n_1 - $bg_dep_n_1;

//bk = ba + bb + bg, avec bg = bh + bi + bj
$bk_brut_n_1 = $ba_brut_n_1 + $bb_brut_n_1 + $bg_brut_n_1;
$bk_dep_n_1 = $ba_dep_n_1 + $bb_dep_n_1 + $bg_dep_n_1;

//Valeur nette de BK
$bk_net_n_1 = $bk_brut_n_1 - $bk_dep_n_1;


//Total trésorerie actif BT

$bq_brut_n_1 = brut_cpte_n_1([50]);
$br_brut_n_1 = brut_cpte_n_1([51]);
$bs_brut_n_1 = brut_cpte_n_1([52, 53, 54, 55, 57, 581, 582]);

$bq_dep_n_1 = dep_cpte_n_1([590]);
$br_dep_n_1 = dep_cpte_n_1([591]);
$bs_dep_n_1 = dep_cpte_n_1([592, 593, 594]);

$bq_net_n_1 = $bq_brut_n_1 - $bq_dep_n_1;
$br_net_n_1 = $br_brut_n_1 - $br_dep_n_1;
$bs_net_n_1 = $bs_brut_n_1 - $bs_dep_n_1;

//bt = bq + br + bs
$bt_brut_n_1 = $bq_brut_n_1 + $br_brut_n_1 + $bs_brut_n_1;
$bt_dep_n_1 = $bq_dep_n_1 + $br_dep_n_1 + $bs_dep_n_1;

//Valeur nette de BT
$bt_net_n_1 = $bt_brut_n_1 - $bt_dep_n_1;


// Ecart de conversion
$bu_brut_n_1 = brut_cpte_n_1([478]);
$bu_dep_n_1 = 0;

//Valeur nette de BU
$bu_net_n_1 = $bu_brut_n_1 - $bu_dep_n_1;


//Total général
$bz_brut_n_1 = $az_brut_n_1 + $bk_brut_n_1 + $bt_brut_n_1 + $bu_brut_n_1;
$bz_dep_n_1 = $az_dep_n_1 + $bk_dep_n_1 + $bt_dep_n_1 + $bu_dep_n_1;


//Valeur nette de BZ
$bz_net_n_1 = $bz_brut_n_1 - $bz_dep_n_1;



//  PASSIF

//Total capitaux propres et ressources assimilées CP

$ca_n_1 = dep_cpte_n_1([101, 102, 103, 104]);

// $cb, brut_cpte car sa valeur reste dans SFD, et on le rend négatif (voir tableau de correspondance)
$cb_n_1 = 0 - (brut_cpte_n_1([109]));

$cd_n_1 = dep_cpte_n_1([105]);
$ce_n_1 = dep_cpte_n_1([106]);
$cf_n_1 = dep_cpte_n_1([111, 112, 113]);
$cg_n_1 = dep_cpte_n_1([118]);


// Le report à nouveau peut être débiteur ou créditeur
$ch_debiteur_n_1 = brut_cpte_n_1([12]);
$ch_crediteur_n_1 = dep_cpte_n_1([12]);
$ch_n_1 = 0;
if ($ch_debiteur_n_1 > 0 && $ch_crediteur_n_1 == 0) {
    $ch_n_1 = 0 - $ch_debiteur_n_1; //on rend négatif la valeur de ch
}
if ($ch_crediteur_n_1 > 0 && $ch_debiteur_n_1 == 0) {
    $ch_n_1 = $ch_crediteur_n_1;
}
if ($ch_crediteur_n_1 == 0 && $ch_debiteur_n_1 == 0) {
    $ch_n_1 = 0;
}


// Le résultat net peut être débiteur ou créditeur
/*
$cj_debiteur_n_1 = brut_cpte_n_1([13]);
$cj_crediteur_n_1 = dep_cpte_n_1([13]);
$cj_n_1 = 0;
if ($cj_debiteur_n_1 > 0 && $cj_crediteur_n_1 == 0) {
    $cj_n_1 = 0 - $cj_debiteur_n_1; //on rend négatif la valeur de cj
}
if ($cj_crediteur_n_1 > 0 && $cj_debiteur_n_1 == 0) {
    $cj_n_1 = $cj_crediteur_n_1;
}
if ($cj_crediteur_n_1 == 0 && $cj_debiteur_n_1 == 0) {
    $cj_n_1 = 0;
}
*/


// Le résultat net est égal au résultat net du compte de résultat
//$cj_n_1 = $xi_n_1;
$cj_n_1 = 0;


$cl_n_1 = dep_cpte_n_1([14]);
$cm_n_1 = dep_cpte_n_1([15]);

$cp_n_1 = $ca_n_1 + $cb_n_1 + $cd_n_1 + $ce_n_1 + $cf_n_1 + $cg_n_1 + $ch_n_1 + $cj_n_1 + $cl_n_1 + $cm_n_1;



//Total dette financières et ressources assimilées DD

$da_n_1 = dep_cpte_n_1([16, 181, 182, 183, 184]);
$db_n_1 = dep_cpte_n_1([17]);
$dc_n_1 = dep_cpte_n_1([19]);

$dd_n_1 = $da_n_1 + $db_n_1 + $dc_n_1;


// Total Ressources stables DF
$df_n_1 = $cp_n_1 + $dd_n_1;



//Total passif circulant
$dh_n_1 = dep_cpte_n_1([481, 482, 484, 4998]);
$di_n_1 = dep_cpte_n_1([419]);
$dj_n_1 = dep_cpte_n_1([40], [409]);
$dk_n_1 = dep_cpte_n_1([42, 43, 44]);
$dm_n_1 = dep_cpte_n_1([185, 45, 46, 47], [479]);
$dn_n_1 = dep_cpte_n_1([499, 599], [4998]);

$dp_n_1 = $dh_n_1 + $di_n_1 + $dj_n_1 + $dk_n_1 + $dm_n_1 + $dn_n_1;



//Total trésorerie passif
$dq_n_1 = dep_cpte_n_1([564, 565]);
$dr_n_1 = dep_cpte_n_1([52, 53, 561, 566]);

$dt_n_1 = $dq_n_1 + $dr_n_1;



//Ecart de conversion-Passif
$dv_n_1 = dep_cpte_n_1([479]);


//Total général
$dz_n_1 = $cp_n_1 + $df_n_1 + $dp_n_1 + $dt_n_1 + $dv_n_1;




//Update Bilan =============================================================================================================

update('bilan', [
    'id_bilan' => 1,
    'ad_brut' => $ad_brut,
    'ad_dep' => $ad_dep,
    'ad_net' => $ad_net,
    'ad_net_1' => $ad_net_n_1,

    'ae_brut' => $ae_brut,
    'ae_dep' => $ae_dep,
    'ae_net' => $ae_net,
    'ae_net_1' => $ae_net_n_1,

    'af_brut' => $af_brut,
    'af_dep' => $af_dep,
    'af_net' => $af_net,
    'af_net_1' => $af_net_n_1,

    'ag_brut' => $ag_brut,
    'ag_dep' => $ag_dep,
    'ag_net' => $ag_net,
    'ag_net_1' => $ag_net_n_1,

    'ah_brut' => $ah_brut,
    'ah_dep' => $ah_dep,
    'ah_net' => $ah_net,
    'ah_net_1' => $ah_net_n_1,

    'ai_brut' => $ai_brut,
    'ai_dep' => $ai_dep,
    'ai_net' => $ai_net,
    'ai_net_1' => $ai_net_n_1,

    'aj_brut' => $aj_brut,
    'aj_dep' => $aj_dep,
    'aj_net' => $aj_net,
    'aj_net_1' => $aj_net_n_1,

    'ak_brut' => $ak_brut,
    'ak_dep' => $ak_dep,
    'ak_net' => $ak_net,
    'ak_net_1' => $ak_net_n_1,

    'al_brut' => $al_brut,
    'al_dep' => $al_dep,
    'al_net' => $al_net,
    'al_net_1' => $al_net_n_1,

    'am_brut' => $am_brut,
    'am_dep' => $am_dep,
    'am_net' => $am_net,
    'am_net_1' => $am_net_n_1,

    'an_brut' => $an_brut,
    'an_dep' => $an_dep,
    'an_net' => $an_net,
    'an_net_1' => $an_net_n_1,

    'ap_brut' => $ap_brut,
    'ap_dep' => $ap_dep,
    'ap_net' => $ap_net,
    'ap_net_1' => $ap_net_n_1,

    'aq_brut' => $aq_brut,
    'aq_dep' => $aq_dep,
    'aq_net' => $aq_net,
    'aq_net_1' => $aq_net_n_1,

    'ar_brut' => $ar_brut,
    'ar_dep' => $ar_dep,
    'ar_net' => $ar_net,
    'ar_net_1' => $ar_net_n_1,

    'as_brut' => $as_brut,
    'as_dep' => $as_dep,
    'as_net' => $as_net,
    'as_net_1' => $as_net_n_1,

    'az_brut' => $az_brut,
    'az_dep' => $az_dep,
    'az_net' => $az_net,
    'az_net_1' => $az_net_n_1,

    'ba_brut' => $ba_brut,
    'ba_dep' => $ba_dep,
    'ba_net' => $ba_net,
    'ba_net_1' => $ba_net_n_1,

    'bb_brut' => $bb_brut,
    'bb_dep' => $bb_dep,
    'bb_net' => $bb_net,
    'bb_net_1' => $bb_net_n_1,

    'bg_brut' => $bg_brut,
    'bg_dep' => $bg_dep,
    'bg_net' => $bg_net,
    'bg_net_1' => $bg_net_n_1,

    'bh_brut' => $bh_brut,
    'bh_dep' => $bh_dep,
    'bh_net' => $bh_net,
    'bh_net_1' => $bh_net_n_1,

    'bi_brut' => $bi_brut,
    'bi_dep' => $bi_dep,
    'bi_net' => $bi_net,
    'bi_net_1' => $bi_net_n_1,

    'bj_brut' => $bj_brut,
    'bj_dep' => $bj_dep,
    'bj_net' => $bj_net,
    'bj_net_1' => $bj_net_n_1,

    'bk_brut' => $bk_brut,
    'bk_dep' => $bk_dep,
    'bk_net' => $bk_net,
    'bk_net_1' => $bk_net_n_1,

    'bq_brut' => $bq_brut,
    'bq_dep' => $bq_dep,
    'bq_net' => $bq_net,
    'bq_net_1' => $bq_net_n_1,

    'br_brut' => $br_brut,
    'br_dep' => $br_dep,
    'br_net' => $br_net,
    'br_net_1' => $br_net_n_1,

    'bs_brut' => $bs_brut,
    'bs_dep' => $bs_dep,
    'bs_net' => $bs_net,
    'bs_net_1' => $bs_net_n_1,

    'bt_brut' => $bt_brut,
    'bt_dep' => $bt_dep,
    'bt_net' => $bt_net,
    'bt_net_1' => $bt_net_n_1,

    'bu_brut' => $bu_brut,
    'bu_dep' => $bu_dep,
    'bu_net' => $bu_net,
    'bu_net_1' => $bu_net_n_1,

    'bz_brut' => $bz_brut,
    'bz_dep' => $bz_dep,
    'bz_net' => $bz_net,
    'bz_net_1' => $bz_net_n_1,

    'ca' => $ca,
    'ca_1' => $ca_n_1,

    'cb' => $cb,
    'cb_1' => $cb_n_1,

    'cd' => $cd,
    'cd_1' => $cd_n_1,

    'ce' => $ce,
    'ce_1' => $ce_n_1,

    'cf' => $cf,
    'cf_1' => $cf_n_1,

    'cg' => $cg,
    'cg_1' => $cg_n_1,

    'ch' => $ch,
    'ch_1' => $ch_n_1,

    'cj' => $cj,
    'cj_1' => $cj_n_1,

    'cl' => $cl,
    'cl_1' => $cl_n_1,

    'cm' => $cm,
    'cm_1' => $cm_n_1,

    'cp' => $cp,
    'cp_1' => $cp_n_1,

    'da' => $da,
    'da_1' => $da_n_1,

    'db' => $db,
    'db_1' => $db_n_1,

    'dc' => $dc,
    'dc_1' => $dc_n_1,

    'dd' => $dd,
    'dd_1' => $dd_n_1,

    'df' => $df,
    'df_1' => $df_n_1,

    'dh' => $dh,
    'dh_1' => $dh_n_1,

    'di' => $di,
    'di_1' => $di_n_1,

    'dj' => $dj,
    'dj_1' => $dj_n_1,

    'dk' => $dk,
    'dk_1' => $dk_n_1,

    'dm' => $dm,
    'dm_1' => $dm_n_1,

    'dn' => $dn,
    'dn_1' => $dn_n_1,

    'dp' => $dp,
    'dp_1' => $dp_n_1,

    'dq' => $dq,
    'dq_1' => $dq_n_1,

    'dr' => $dr,
    'dr_1' => $dr_n_1,

    'dt' => $dt,
    'dt_1' => $dt_n_1,

    'dv' => $dv,
    'dv_1' => $dv_n_1,

    'dz' => $dz,
    'dz_1' => $dz_n_1

],'id_bilan = 1', $db);